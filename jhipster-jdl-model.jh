entity EnvConfig {
	param String required
	value String required
	description String
}

entity ModelDef {
	name String required
	language Language required
    solver Solver required
    currentVersion Integer
	description String
}

entity ModelConfig {
	param String required
	value String
	description String
}

entity ModelVersion {
	version Integer required
	creationDate LocalDate
	srcPath String
	srcContent TextBlob
	description String
}

entity ModelObjFunction {
	code String required
	name String required
	description String
}

entity ModelRunDef {
	name String required
	language Language required
    solver Solver required
	version Integer required
	objFunction String required
	runDate LocalDate
	startTime String
	endTime String
	status Status
	progress Integer min(0) max(100)
	dataSetInput String
	dataSetResult String
	userResult AnyBlob
	otherResult AnyBlob
	comments String
	user String
}

entity ModelRunConfig {
	param String required
	value String
}

relationship ManyToOne {
	ModelConfig{modelDef(id)} to ModelDef
	ModelVersion{modelDef(id)} to ModelDef
	ModelObjFunction{modelDef(id)} to ModelDef
	ModelRunConfig{modelRunDef(id)} to ModelRunDef
	ModelRunConfig{modelRunDef(id)} to ModelRunDef
}

enum Status {
    READY, RUNNING, FINISHED, ERROR
}

enum Language {
    GAMS, PYOMO, ORTOOLS
}

enum Solver {
    CPLEX, GUROBI, GLPK, CBC
}

dto * with mapstruct

service all with serviceClass

// pagination options
paginate all with infinite-scroll
