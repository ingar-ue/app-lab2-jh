package gov.ingar.applab.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

import gov.ingar.applab.domain.enumeration.Language;

import gov.ingar.applab.domain.enumeration.Solver;

import gov.ingar.applab.domain.enumeration.Status;

/**
 * A ModelRunDef.
 */
@Entity
@Table(name = "model_run_def")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ModelRunDef implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "language", nullable = false)
    private Language language;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "solver", nullable = false)
    private Solver solver;

    @NotNull
    @Column(name = "version", nullable = false)
    private Integer version;

    @NotNull
    @Column(name = "obj_function", nullable = false)
    private String objFunction;

    @Column(name = "run_date")
    private LocalDate runDate;

    @Column(name = "start_time")
    private String startTime;

    @Column(name = "end_time")
    private String endTime;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @Min(value = 0)
    @Max(value = 100)
    @Column(name = "progress")
    private Integer progress;

    @Column(name = "data_set_input")
    private String dataSetInput;

    @Column(name = "data_set_result")
    private String dataSetResult;

    @Lob
    @Column(name = "user_result")
    private byte[] userResult;

    @Column(name = "user_result_content_type")
    private String userResultContentType;

    @Lob
    @Column(name = "other_result")
    private byte[] otherResult;

    @Column(name = "other_result_content_type")
    private String otherResultContentType;

    @Column(name = "comments")
    private String comments;

    @Column(name = "jhi_user")
    private String user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public ModelRunDef name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Language getLanguage() {
        return language;
    }

    public ModelRunDef language(Language language) {
        this.language = language;
        return this;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Solver getSolver() {
        return solver;
    }

    public ModelRunDef solver(Solver solver) {
        this.solver = solver;
        return this;
    }

    public void setSolver(Solver solver) {
        this.solver = solver;
    }

    public Integer getVersion() {
        return version;
    }

    public ModelRunDef version(Integer version) {
        this.version = version;
        return this;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getObjFunction() {
        return objFunction;
    }

    public ModelRunDef objFunction(String objFunction) {
        this.objFunction = objFunction;
        return this;
    }

    public void setObjFunction(String objFunction) {
        this.objFunction = objFunction;
    }

    public LocalDate getRunDate() {
        return runDate;
    }

    public ModelRunDef runDate(LocalDate runDate) {
        this.runDate = runDate;
        return this;
    }

    public void setRunDate(LocalDate runDate) {
        this.runDate = runDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public ModelRunDef startTime(String startTime) {
        this.startTime = startTime;
        return this;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public ModelRunDef endTime(String endTime) {
        this.endTime = endTime;
        return this;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Status getStatus() {
        return status;
    }

    public ModelRunDef status(Status status) {
        this.status = status;
        return this;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Integer getProgress() {
        return progress;
    }

    public ModelRunDef progress(Integer progress) {
        this.progress = progress;
        return this;
    }

    public void setProgress(Integer progress) {
        this.progress = progress;
    }

    public String getDataSetInput() {
        return dataSetInput;
    }

    public ModelRunDef dataSetInput(String dataSetInput) {
        this.dataSetInput = dataSetInput;
        return this;
    }

    public void setDataSetInput(String dataSetInput) {
        this.dataSetInput = dataSetInput;
    }

    public String getDataSetResult() {
        return dataSetResult;
    }

    public ModelRunDef dataSetResult(String dataSetResult) {
        this.dataSetResult = dataSetResult;
        return this;
    }

    public void setDataSetResult(String dataSetResult) {
        this.dataSetResult = dataSetResult;
    }

    public byte[] getUserResult() {
        return userResult;
    }

    public ModelRunDef userResult(byte[] userResult) {
        this.userResult = userResult;
        return this;
    }

    public void setUserResult(byte[] userResult) {
        this.userResult = userResult;
    }

    public String getUserResultContentType() {
        return userResultContentType;
    }

    public ModelRunDef userResultContentType(String userResultContentType) {
        this.userResultContentType = userResultContentType;
        return this;
    }

    public void setUserResultContentType(String userResultContentType) {
        this.userResultContentType = userResultContentType;
    }

    public byte[] getOtherResult() {
        return otherResult;
    }

    public ModelRunDef otherResult(byte[] otherResult) {
        this.otherResult = otherResult;
        return this;
    }

    public void setOtherResult(byte[] otherResult) {
        this.otherResult = otherResult;
    }

    public String getOtherResultContentType() {
        return otherResultContentType;
    }

    public ModelRunDef otherResultContentType(String otherResultContentType) {
        this.otherResultContentType = otherResultContentType;
        return this;
    }

    public void setOtherResultContentType(String otherResultContentType) {
        this.otherResultContentType = otherResultContentType;
    }

    public String getComments() {
        return comments;
    }

    public ModelRunDef comments(String comments) {
        this.comments = comments;
        return this;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getUser() {
        return user;
    }

    public ModelRunDef user(String user) {
        this.user = user;
        return this;
    }

    public void setUser(String user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ModelRunDef modelRunDef = (ModelRunDef) o;
        if (modelRunDef.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), modelRunDef.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ModelRunDef{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", language='" + getLanguage() + "'" +
            ", solver='" + getSolver() + "'" +
            ", version=" + getVersion() +
            ", objFunction='" + getObjFunction() + "'" +
            ", runDate='" + getRunDate() + "'" +
            ", startTime='" + getStartTime() + "'" +
            ", endTime='" + getEndTime() + "'" +
            ", status='" + getStatus() + "'" +
            ", progress=" + getProgress() +
            ", dataSetInput='" + getDataSetInput() + "'" +
            ", dataSetResult='" + getDataSetResult() + "'" +
            ", userResult='" + getUserResult() + "'" +
            ", userResultContentType='" + getUserResultContentType() + "'" +
            ", otherResult='" + getOtherResult() + "'" +
            ", otherResultContentType='" + getOtherResultContentType() + "'" +
            ", comments='" + getComments() + "'" +
            ", user='" + getUser() + "'" +
            "}";
    }

    public String getDuration() {
        String duration = "-";
        if (startTime != null && endTime  != null) {
            DateTimeFormatter fmt = DateTimeFormatter.ofPattern("HH:mm:ss");
            LocalDateTime desde = LocalDateTime.of(runDate, LocalTime.parse(startTime, fmt));
            LocalDateTime hasta = LocalDateTime.of(runDate, LocalTime.parse(endTime, fmt));

            LocalDateTime tempDateTime = LocalDateTime.from( desde );

            long hours = tempDateTime.until( hasta, ChronoUnit.HOURS);
            tempDateTime = tempDateTime.plusHours( hours );

            long minutes = tempDateTime.until( hasta, ChronoUnit.MINUTES);
            tempDateTime = tempDateTime.plusMinutes( minutes );

            long seconds = tempDateTime.until( hasta, ChronoUnit.SECONDS);

            if (hours > 0) {
                duration = hours + " horas " +
                    minutes + " minutos " +
                    seconds + " segundos";
            } else if (minutes > 0) {
                duration = minutes + " minutos " +
                    seconds + " segundos";
            } else {
                duration = seconds + " segundos";
            }
        }
        return duration;
    }

    @JsonIgnore
    public long getDurationInSeconds() {
        long duration = 0;
        if (startTime != null && endTime  != null) {
            DateTimeFormatter fmt = DateTimeFormatter.ofPattern("HH:mm:ss");
            LocalDateTime desde = LocalDateTime.of(runDate, LocalTime.parse(startTime, fmt));
            LocalDateTime hasta = LocalDateTime.of(runDate, LocalTime.parse(endTime, fmt));

            LocalDateTime tempDateTime = LocalDateTime.from( desde );

            long hours = tempDateTime.until( hasta, ChronoUnit.HOURS);
            tempDateTime = tempDateTime.plusHours( hours );

            long minutes = tempDateTime.until( hasta, ChronoUnit.MINUTES);
            tempDateTime = tempDateTime.plusMinutes( minutes );

            long seconds = tempDateTime.until( hasta, ChronoUnit.SECONDS);

            duration = hours * 3600 + minutes * 60 + seconds;
        }
        return duration;
    }
}
