package gov.ingar.applab.domain;

import java.io.*;
import java.util.List;

public class Result implements Serializable {

    private Double cost;
    private List<ResultSolution> solution;

    public Result() {
    }

    public Result(Double cost, List<ResultSolution> solution) {
        this.cost = cost;
        this.solution = solution;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public List<ResultSolution> getSolution() {
        return solution;
    }

    public void setSolutions(List<ResultSolution> solution) {
        this.solution = solution;
    }

    public byte[] toStream() {
        // Reference for stream of bytes
        byte[] stream = null;
        // ObjectOutputStream is used to convert a Java object into OutputStream
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
             ObjectOutputStream oos = new ObjectOutputStream(baos);) {
            oos.writeObject(this);
            stream = baos.toByteArray();
        } catch (IOException e) {
            // Error in serialization
            e.printStackTrace();
        }
        return stream;
    }

    public static Result toResult(byte[] stream) {
        Result result = null;

        try (ByteArrayInputStream bais = new ByteArrayInputStream(stream);
             ObjectInputStream ois = new ObjectInputStream(bais);) {
            result = (Result) ois.readObject();
        } catch (IOException e) {
            // Error in de-serialization
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // You are converting an invalid stream to Student
            e.printStackTrace();
        }
        return result;
    }
}
