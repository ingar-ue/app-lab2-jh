package gov.ingar.applab.domain.enumeration;

/**
 * The Solver enumeration.
 */
public enum Solver {
    CPLEX, GUROBI, GLPK, CBC
}
