package gov.ingar.applab.domain;

import java.io.Serializable;

public class ResultSolution implements Serializable {

    private Market market;
    private Plant plant;
    private int value;

    public ResultSolution() {
    }

    public ResultSolution(Market market, Plant plant, int value) {
        this.market = market;
        this.plant = plant;
        this.value = value;
    }

    public Market getMarket() {
        return market;
    }

    public void setMarket(Market market) {
        this.market = market;
    }

    public Plant getPlant() {
        return plant;
    }

    public void setPlant(Plant plant) {
        this.plant = plant;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
