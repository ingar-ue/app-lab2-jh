package gov.ingar.applab.service;

import gov.ingar.applab.domain.ModelRunConfig;
import gov.ingar.applab.repository.ModelRunConfigRepository;
import gov.ingar.applab.service.dto.ModelRunConfigDTO;
import gov.ingar.applab.service.mapper.ModelRunConfigMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing ModelRunConfig.
 */
@Service
@Transactional
public class ModelRunConfigService {

    private final Logger log = LoggerFactory.getLogger(ModelRunConfigService.class);

    private final ModelRunConfigRepository modelRunConfigRepository;

    private final ModelRunConfigMapper modelRunConfigMapper;

    public ModelRunConfigService(ModelRunConfigRepository modelRunConfigRepository, ModelRunConfigMapper modelRunConfigMapper) {
        this.modelRunConfigRepository = modelRunConfigRepository;
        this.modelRunConfigMapper = modelRunConfigMapper;
    }

    /**
     * Save a modelRunConfig.
     *
     * @param modelRunConfigDTO the entity to save
     * @return the persisted entity
     */
    public ModelRunConfigDTO save(ModelRunConfigDTO modelRunConfigDTO) {
        log.debug("Request to save ModelRunConfig : {}", modelRunConfigDTO);
        ModelRunConfig modelRunConfig = modelRunConfigMapper.toEntity(modelRunConfigDTO);
        modelRunConfig = modelRunConfigRepository.save(modelRunConfig);
        return modelRunConfigMapper.toDto(modelRunConfig);
    }

    /**
     * Get all the modelRunConfigs.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ModelRunConfigDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ModelRunConfigs");
        return modelRunConfigRepository.findAll(pageable)
            .map(modelRunConfigMapper::toDto);
    }


    /**
     * Get one modelRunConfig by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<ModelRunConfigDTO> findOne(Long id) {
        log.debug("Request to get ModelRunConfig : {}", id);
        return modelRunConfigRepository.findById(id)
            .map(modelRunConfigMapper::toDto);
    }

    /**
     * Delete the modelRunConfig by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ModelRunConfig : {}", id);
        modelRunConfigRepository.deleteById(id);
    }
}
