package gov.ingar.applab.service.mapper;

import gov.ingar.applab.domain.*;
import gov.ingar.applab.service.dto.ModelRunDefDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ModelRunDef and its DTO ModelRunDefDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ModelRunDefMapper extends EntityMapper<ModelRunDefDTO, ModelRunDef> {



    default ModelRunDef fromId(Long id) {
        if (id == null) {
            return null;
        }
        ModelRunDef modelRunDef = new ModelRunDef();
        modelRunDef.setId(id);
        return modelRunDef;
    }
}
