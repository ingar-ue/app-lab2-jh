package gov.ingar.applab.service.mapper;

import gov.ingar.applab.domain.Distance;
import gov.ingar.applab.service.dto.DistanceDTO;

import gov.ingar.applab.service.mapper.EntityMapper;
import org.mapstruct.*;

/**
 * Mapper for the entity Distance and its DTO DistanceDTO.
 */
@Mapper(componentModel = "spring", uses = {PlantMapper.class, MarketMapper.class})
public interface DistanceMapper extends EntityMapper<DistanceDTO, Distance> {

    @Mapping(source = "plant.id", target = "plantId")
    @Mapping(source = "market.id", target = "marketId")
    @Mapping(source = "plant.name", target = "plantName")
    @Mapping(source = "market.name", target = "marketName")
    DistanceDTO toDto(Distance distance);

    @Mapping(source = "plantId", target = "plant")
    @Mapping(source = "marketId", target = "market")
    Distance toEntity(DistanceDTO distanceDTO);

    default Distance fromId(Long id) {
        if (id == null) {
            return null;
        }
        Distance distance = new Distance();
        distance.setId(id);
        return distance;
    }
}
