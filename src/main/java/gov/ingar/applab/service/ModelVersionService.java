package gov.ingar.applab.service;

import gov.ingar.applab.domain.ModelVersion;
import gov.ingar.applab.repository.ModelVersionRepository;
import gov.ingar.applab.service.dto.ModelVersionDTO;
import gov.ingar.applab.service.mapper.ModelVersionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing ModelVersion.
 */
@Service
@Transactional
public class ModelVersionService {

    private final Logger log = LoggerFactory.getLogger(ModelVersionService.class);

    private final ModelVersionRepository modelVersionRepository;

    private final ModelVersionMapper modelVersionMapper;

    public ModelVersionService(ModelVersionRepository modelVersionRepository, ModelVersionMapper modelVersionMapper) {
        this.modelVersionRepository = modelVersionRepository;
        this.modelVersionMapper = modelVersionMapper;
    }

    /**
     * Save a modelVersion.
     *
     * @param modelVersionDTO the entity to save
     * @return the persisted entity
     */
    public ModelVersionDTO save(ModelVersionDTO modelVersionDTO) {
        log.debug("Request to save ModelVersion : {}", modelVersionDTO);
        ModelVersion modelVersion = modelVersionMapper.toEntity(modelVersionDTO);
        modelVersion = modelVersionRepository.save(modelVersion);
        return modelVersionMapper.toDto(modelVersion);
    }

    /**
     * Get all the modelVersions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ModelVersionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ModelVersions");
        return modelVersionRepository.findAll(pageable)
            .map(modelVersionMapper::toDto);
    }


    /**
     * Get one modelVersion by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<ModelVersionDTO> findOne(Long id) {
        log.debug("Request to get ModelVersion : {}", id);
        return modelVersionRepository.findById(id)
            .map(modelVersionMapper::toDto);
    }

    /**
     * Delete the modelVersion by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ModelVersion : {}", id);
        modelVersionRepository.deleteById(id);
    }

    public List<ModelVersionDTO> findAllByModelDefId(Long id){
        return modelVersionMapper.toDto(modelVersionRepository.findAllByModelDefId(id));
    }
}
