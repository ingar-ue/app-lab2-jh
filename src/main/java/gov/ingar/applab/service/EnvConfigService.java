package gov.ingar.applab.service;

import gov.ingar.applab.domain.EnvConfig;
import gov.ingar.applab.repository.EnvConfigRepository;
import gov.ingar.applab.service.dto.EnvConfigDTO;
import gov.ingar.applab.service.mapper.EnvConfigMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing EnvConfig.
 */
@Service
@Transactional
public class EnvConfigService {

    private final Logger log = LoggerFactory.getLogger(EnvConfigService.class);

    private final EnvConfigRepository envConfigRepository;

    private final EnvConfigMapper envConfigMapper;

    public EnvConfigService(EnvConfigRepository envConfigRepository, EnvConfigMapper envConfigMapper) {
        this.envConfigRepository = envConfigRepository;
        this.envConfigMapper = envConfigMapper;
    }

    /**
     * Save a envConfig.
     *
     * @param envConfigDTO the entity to save
     * @return the persisted entity
     */
    public EnvConfigDTO save(EnvConfigDTO envConfigDTO) {
        log.debug("Request to save EnvConfig : {}", envConfigDTO);
        EnvConfig envConfig = envConfigMapper.toEntity(envConfigDTO);
        envConfig = envConfigRepository.save(envConfig);
        return envConfigMapper.toDto(envConfig);
    }

    /**
     * Get all the envConfigs.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<EnvConfigDTO> findAll(Pageable pageable) {
        log.debug("Request to get all EnvConfigs");
        return envConfigRepository.findAll(pageable)
            .map(envConfigMapper::toDto);
    }


    /**
     * Get one envConfig by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<EnvConfigDTO> findOne(Long id) {
        log.debug("Request to get EnvConfig : {}", id);
        return envConfigRepository.findById(id)
            .map(envConfigMapper::toDto);
    }

    /**
     * Delete the envConfig by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete EnvConfig : {}", id);
        envConfigRepository.deleteById(id);
    }


    @Transactional(readOnly = true)
    public Optional<EnvConfigDTO> findOneByParam(String param){
        return envConfigRepository.findOneByParam(param)
            .map(envConfigMapper::toDto);
    }
}
