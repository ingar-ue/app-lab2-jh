package gov.ingar.applab.service;

import gov.ingar.applab.domain.ModelObjFunction;
import gov.ingar.applab.repository.ModelObjFunctionRepository;
import gov.ingar.applab.service.dto.ModelObjFunctionDTO;
import gov.ingar.applab.service.mapper.ModelObjFunctionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing ModelObjFunction.
 */
@Service
@Transactional
public class ModelObjFunctionService {

    private final Logger log = LoggerFactory.getLogger(ModelObjFunctionService.class);

    private final ModelObjFunctionRepository modelObjFunctionRepository;

    private final ModelObjFunctionMapper modelObjFunctionMapper;

    public ModelObjFunctionService(ModelObjFunctionRepository modelObjFunctionRepository, ModelObjFunctionMapper modelObjFunctionMapper) {
        this.modelObjFunctionRepository = modelObjFunctionRepository;
        this.modelObjFunctionMapper = modelObjFunctionMapper;
    }

    /**
     * Save a modelObjFunction.
     *
     * @param modelObjFunctionDTO the entity to save
     * @return the persisted entity
     */
    public ModelObjFunctionDTO save(ModelObjFunctionDTO modelObjFunctionDTO) {
        log.debug("Request to save ModelObjFunction : {}", modelObjFunctionDTO);
        ModelObjFunction modelObjFunction = modelObjFunctionMapper.toEntity(modelObjFunctionDTO);
        modelObjFunction = modelObjFunctionRepository.save(modelObjFunction);
        return modelObjFunctionMapper.toDto(modelObjFunction);
    }

    /**
     * Get all the modelObjFunctions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ModelObjFunctionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ModelObjFunctions");
        return modelObjFunctionRepository.findAll(pageable)
            .map(modelObjFunctionMapper::toDto);
    }


    /**
     * Get one modelObjFunction by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<ModelObjFunctionDTO> findOne(Long id) {
        log.debug("Request to get ModelObjFunction : {}", id);
        return modelObjFunctionRepository.findById(id)
            .map(modelObjFunctionMapper::toDto);
    }

    /**
     * Delete the modelObjFunction by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ModelObjFunction : {}", id);
        modelObjFunctionRepository.deleteById(id);
    }

    public List<ModelObjFunctionDTO> findAllByModelDefId(Long id){
        return modelObjFunctionMapper.toDto(modelObjFunctionRepository.findAllByModelDefId(id));
    }
}
