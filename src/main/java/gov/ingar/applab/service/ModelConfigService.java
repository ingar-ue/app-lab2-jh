package gov.ingar.applab.service;

import gov.ingar.applab.domain.ModelConfig;
import gov.ingar.applab.repository.ModelConfigRepository;
import gov.ingar.applab.service.dto.ModelConfigDTO;
import gov.ingar.applab.service.mapper.ModelConfigMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing ModelConfig.
 */
@Service
@Transactional
public class ModelConfigService {

    private final Logger log = LoggerFactory.getLogger(ModelConfigService.class);

    private final ModelConfigRepository modelConfigRepository;

    private final ModelConfigMapper modelConfigMapper;

    public ModelConfigService(ModelConfigRepository modelConfigRepository, ModelConfigMapper modelConfigMapper) {
        this.modelConfigRepository = modelConfigRepository;
        this.modelConfigMapper = modelConfigMapper;
    }

    /**
     * Save a modelConfig.
     *
     * @param modelConfigDTO the entity to save
     * @return the persisted entity
     */
    public ModelConfigDTO save(ModelConfigDTO modelConfigDTO) {
        log.debug("Request to save ModelConfig : {}", modelConfigDTO);
        ModelConfig modelConfig = modelConfigMapper.toEntity(modelConfigDTO);
        modelConfig = modelConfigRepository.save(modelConfig);
        return modelConfigMapper.toDto(modelConfig);
    }

    /**
     * Get all the modelConfigs.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ModelConfigDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ModelConfigs");
        return modelConfigRepository.findAll(pageable)
            .map(modelConfigMapper::toDto);
    }


    /**
     * Get one modelConfig by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<ModelConfigDTO> findOne(Long id) {
        log.debug("Request to get ModelConfig : {}", id);
        return modelConfigRepository.findById(id)
            .map(modelConfigMapper::toDto);
    }

    /**
     * Delete the modelConfig by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ModelConfig : {}", id);
        modelConfigRepository.deleteById(id);
    }

    public List<ModelConfigDTO> findAllByModelDefId(Long id){
        return modelConfigMapper.toDto(modelConfigRepository.findAllByModelDefId(id));
    }
}
