package gov.ingar.applab.service.dto;

import gov.ingar.applab.domain.Result;
import gov.ingar.applab.domain.enumeration.Language;
import gov.ingar.applab.domain.enumeration.Solver;
import gov.ingar.applab.domain.enumeration.Status;

import javax.persistence.Lob;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

public class ModelRunDefResultDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private Language language;

    @NotNull
    private Solver solver;

    @NotNull
    private Integer version;

    @NotNull
    private String objFunction;

    private Integer objFunctionCode;

    private LocalDate runDate;

    private String startTime;

    private String endTime;

    private Status status;

    @Min(value = 0)
    @Max(value = 100)
    private Integer progress;

    private Result userResult;

    private String comments;

    private String user;

    private String duration;

    public ModelRunDefResultDTO(){}

    public ModelRunDefResultDTO(ModelRunDefDTO modelRunDefDTO) {
        this.id = modelRunDefDTO.getId();
        this.name = modelRunDefDTO.getName();
        this.language = modelRunDefDTO.getLanguage();
        this.solver = modelRunDefDTO.getSolver();
        this.version = modelRunDefDTO.getVersion();
        this.objFunction = modelRunDefDTO.getObjFunction();
        this.status = modelRunDefDTO.getStatus();
        this.progress = modelRunDefDTO.getProgress();
        this.user = modelRunDefDTO.getUser();
        this.comments = modelRunDefDTO.getComments();
        this.runDate = modelRunDefDTO.getRunDate();
        this.userResult = Result.toResult(modelRunDefDTO.getUserResult());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Solver getSolver() {
        return solver;
    }

    public void setSolver(Solver solver) {
        this.solver = solver;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getObjFunction() {
        return objFunction;
    }

    public void setObjFunction(String objFunction) {
        this.objFunction = objFunction;
    }

    public Integer getObjFunctionCode() {
        return objFunctionCode;
    }

    public void setObjFunctionCode(Integer objFunctionCode) {
        this.objFunctionCode = objFunctionCode;
    }

    public LocalDate getRunDate() {
        return runDate;
    }

    public void setRunDate(LocalDate runDate) {
        this.runDate = runDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Integer getProgress() {
        return progress;
    }

    public void setProgress(Integer progress) {
        this.progress = progress;
    }

    public Result getUserResult() {
        return userResult;
    }

    public void setUserResult(Result userResult) {
        this.userResult = userResult;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
