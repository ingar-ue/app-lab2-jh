package gov.ingar.applab.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the ModelObjFunction entity.
 */
public class ModelObjFunctionDTO implements Serializable {

    private Long id;

    @NotNull
    private String code;

    @NotNull
    private String name;

    private String description;

    private Long modelDefId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getModelDefId() {
        return modelDefId;
    }

    public void setModelDefId(Long modelDefId) {
        this.modelDefId = modelDefId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ModelObjFunctionDTO modelObjFunctionDTO = (ModelObjFunctionDTO) o;
        if (modelObjFunctionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), modelObjFunctionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ModelObjFunctionDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", modelDef=" + getModelDefId() +
            "}";
    }
}
