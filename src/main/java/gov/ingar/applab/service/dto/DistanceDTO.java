package gov.ingar.applab.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Distance entity.
 */
public class DistanceDTO implements Serializable {

    private Long id;

    @NotNull
    private Double distance;

    private Long plantId;

    private Long marketId;

    private String plantName;

    private String marketName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Long getPlantId() {
        return plantId;
    }

    public void setPlantId(Long plantId) {
        this.plantId = plantId;
    }

    public Long getMarketId() {
        return marketId;
    }

    public void setMarketId(Long marketId) {
        this.marketId = marketId;
    }

    public String getPlantName() {
        return plantName;
    }

    public void setPlantName(String plantName) {
        this.plantName = plantName;
    }

    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DistanceDTO distanceDTO = (DistanceDTO) o;
        if (distanceDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), distanceDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DistanceDTO{" +
            "id=" + getId() +
            ", distance=" + getDistance() +
            ", plant=" + getPlantId() +
            ", market=" + getMarketId() +
            "}";
    }
}
