package gov.ingar.applab.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Objects;
import javax.persistence.Lob;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gov.ingar.applab.domain.enumeration.Language;
import gov.ingar.applab.domain.enumeration.Solver;
import gov.ingar.applab.domain.enumeration.Status;

/**
 * A DTO for the ModelRunDef entity.
 */
public class ModelRunDefDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private Language language;

    @NotNull
    private Solver solver;

    @NotNull
    private Integer version;

    @NotNull
    private String objFunction;

    private Integer objFunctionCode;

    private LocalDate runDate;

    private String startTime;

    private String endTime;

    private Status status;

    @Min(value = 0)
    @Max(value = 100)
    private Integer progress;

    private String dataSetInput;

    private String dataSetResult;

    @Lob
    private byte[] userResult;
    private String userResultContentType;

    @Lob
    private byte[] otherResult;
    private String otherResultContentType;

    private String comments;

    private String user;

    private String duration;

    public ModelRunDefDTO() {}

    public ModelRunDefDTO(ModelRunDefResultDTO modelRunDefResultDTO) {
        this.name = modelRunDefResultDTO.getName();
        this.language = modelRunDefResultDTO.getLanguage();
        this.solver = modelRunDefResultDTO.getSolver();
        this.version = modelRunDefResultDTO.getVersion();
        this.objFunction = modelRunDefResultDTO.getObjFunction();
        this.status = modelRunDefResultDTO.getStatus();
        this.progress = modelRunDefResultDTO.getProgress();
        this.user = modelRunDefResultDTO.getUser();
        this.comments = modelRunDefResultDTO.getComments();
        this.runDate = modelRunDefResultDTO.getRunDate();
        this.userResult = modelRunDefResultDTO.getUserResult().toStream();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Solver getSolver() {
        return solver;
    }

    public void setSolver(Solver solver) {
        this.solver = solver;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getObjFunction() {
        return objFunction;
    }

    public void setObjFunction(String objFunction) {
        this.objFunction = objFunction;
    }

    public LocalDate getRunDate() {
        return runDate;
    }

    public void setRunDate(LocalDate runDate) {
        this.runDate = runDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Integer getProgress() {
        return progress;
    }

    public void setProgress(Integer progress) {
        this.progress = progress;
    }

    public String getDataSetInput() {
        return dataSetInput;
    }

    public void setDataSetInput(String dataSetInput) {
        this.dataSetInput = dataSetInput;
    }

    public String getDataSetResult() {
        return dataSetResult;
    }

    public void setDataSetResult(String dataSetResult) {
        this.dataSetResult = dataSetResult;
    }

    public byte[] getUserResult() {
        return userResult;
    }

    public void setUserResult(byte[] userResult) {
        this.userResult = userResult;
    }

    public String getUserResultContentType() {
        return userResultContentType;
    }

    public void setUserResultContentType(String userResultContentType) {
        this.userResultContentType = userResultContentType;
    }

    public byte[] getOtherResult() {
        return otherResult;
    }

    public void setOtherResult(byte[] otherResult) {
        this.otherResult = otherResult;
    }

    public String getOtherResultContentType() {
        return otherResultContentType;
    }

    public void setOtherResultContentType(String otherResultContentType) {
        this.otherResultContentType = otherResultContentType;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Integer getObjFunctionCode() {
        return objFunctionCode;
    }

    public void setObjFunctionCode(Integer objFunctionCode) {
        this.objFunctionCode = objFunctionCode;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ModelRunDefDTO modelRunDefDTO = (ModelRunDefDTO) o;
        if (modelRunDefDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), modelRunDefDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ModelRunDefDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", language='" + getLanguage() + "'" +
            ", solver='" + getSolver() + "'" +
            ", version=" + getVersion() +
            ", objFunction='" + getObjFunction() + "'" +
            ", runDate='" + getRunDate() + "'" +
            ", startTime='" + getStartTime() + "'" +
            ", endTime='" + getEndTime() + "'" +
            ", status='" + getStatus() + "'" +
            ", progress=" + getProgress() +
            ", dataSetInput='" + getDataSetInput() + "'" +
            ", dataSetResult='" + getDataSetResult() + "'" +
            ", userResult='" + getUserResult() + "'" +
            ", otherResult='" + getOtherResult() + "'" +
            ", comments='" + getComments() + "'" +
            ", user='" + getUser() + "'" +
            "}";
    }
}
