package gov.ingar.applab.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Plant entity.
 */
public class PlantDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private Integer productiveCapacity;

    private String address;

    private Double latitude;

    private Double longitude;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getProductiveCapacity() {
        return productiveCapacity;
    }

    public void setProductiveCapacity(Integer productiveCapacity) {
        this.productiveCapacity = productiveCapacity;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PlantDTO plantDTO = (PlantDTO) o;
        if (plantDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), plantDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PlantDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", productiveCapacity=" + getProductiveCapacity() +
            ", address='" + getAddress() + "'" +
            ", latitude=" + getLatitude() +
            ", longitude=" + getLongitude() +
            "}";
    }
}
