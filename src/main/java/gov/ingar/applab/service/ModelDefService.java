package gov.ingar.applab.service;

import gov.ingar.applab.domain.ModelDef;
import gov.ingar.applab.repository.ModelDefRepository;
import gov.ingar.applab.service.dto.ModelDefDTO;
import gov.ingar.applab.service.mapper.ModelDefMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing ModelDef.
 */
@Service
@Transactional
public class ModelDefService {

    private final Logger log = LoggerFactory.getLogger(ModelDefService.class);

    private final ModelDefRepository modelDefRepository;

    private final ModelDefMapper modelDefMapper;

    public ModelDefService(ModelDefRepository modelDefRepository, ModelDefMapper modelDefMapper) {
        this.modelDefRepository = modelDefRepository;
        this.modelDefMapper = modelDefMapper;
    }

    /**
     * Save a modelDef.
     *
     * @param modelDefDTO the entity to save
     * @return the persisted entity
     */
    public ModelDefDTO save(ModelDefDTO modelDefDTO) {
        log.debug("Request to save ModelDef : {}", modelDefDTO);
        ModelDef modelDef = modelDefMapper.toEntity(modelDefDTO);
        modelDef = modelDefRepository.save(modelDef);
        return modelDefMapper.toDto(modelDef);
    }

    /**
     * Get all the modelDefs.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ModelDefDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ModelDefs");
        return modelDefRepository.findAll(pageable)
            .map(modelDefMapper::toDto);
    }


    /**
     * Get one modelDef by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ModelDef findOneByName(String name) {
        log.debug("Request to get all ModelDefs");
        return modelDefRepository.findAllByName(name).get(0);
    }


    /**
     * Get one modelDef by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<ModelDefDTO> findOne(Long id) {
        log.debug("Request to get ModelDef : {}", id);
        return modelDefRepository.findById(id)
            .map(modelDefMapper::toDto);
    }

    /**
     * Delete the modelDef by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ModelDef : {}", id);
        modelDefRepository.deleteById(id);
    }
}
