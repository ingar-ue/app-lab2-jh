package gov.ingar.applab.service;

import gov.ingar.applab.domain.ModelRunDef;
import gov.ingar.applab.domain.enumeration.Status;
import gov.ingar.applab.repository.ModelRunDefRepository;
import gov.ingar.applab.service.dto.ModelRunDefDTO;
import gov.ingar.applab.service.mapper.ModelRunDefMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing ModelRunDef.
 */
@Service
@Transactional
public class ModelRunDefService {

    private final Logger log = LoggerFactory.getLogger(ModelRunDefService.class);

    private final ModelRunDefRepository modelRunDefRepository;

    private final ModelRunDefMapper modelRunDefMapper;

    public ModelRunDefService(ModelRunDefRepository modelRunDefRepository, ModelRunDefMapper modelRunDefMapper) {
        this.modelRunDefRepository = modelRunDefRepository;
        this.modelRunDefMapper = modelRunDefMapper;
    }

    /**
     * Save a modelRunDef.
     *
     * @param modelRunDefDTO the entity to save
     * @return the persisted entity
     */
    public ModelRunDefDTO save(ModelRunDefDTO modelRunDefDTO) {
        log.debug("Request to save ModelRunDef : {}", modelRunDefDTO);
        ModelRunDef modelRunDef = modelRunDefMapper.toEntity(modelRunDefDTO);
        modelRunDef = modelRunDefRepository.save(modelRunDef);
        return modelRunDefMapper.toDto(modelRunDef);
    }

    /**
     * Get all the modelRunDefs.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ModelRunDefDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ModelRunDefs");
        return modelRunDefRepository.findAllByOrderByIdDesc(pageable)
            .map(modelRunDefMapper::toDto);
    }


    /**
     * Get one modelRunDef by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<ModelRunDefDTO> findOne(Long id) {
        log.debug("Request to get ModelRunDef : {}", id);
        return modelRunDefRepository.findById(id)
            .map(modelRunDefMapper::toDto);
    }

    @Transactional(readOnly = true)
    public ModelRunDefDTO findRunningModel() {
        Optional<ModelRunDef> o  = modelRunDefRepository.findOneByStatus(Status.RUNNING);
        if(o.isPresent()) return modelRunDefMapper.toDto(o.get());
        else return null;
    }

    /**
     * Delete the modelRunDef by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ModelRunDef : {}", id);
        modelRunDefRepository.deleteById(id);
    }

    public ModelRunDefDTO findLastFinishedOP1(){
        return modelRunDefMapper.toDto(modelRunDefRepository.findFirstByNameAndStatusOrderByIdDesc("Pattern Generator", Status.FINISHED));
    }

    public List<ModelRunDef> findLast10(String name, Status status){
        return modelRunDefRepository.findFirst10ByNameAndStatusOrderByIdDesc(name, status);
    }
}
