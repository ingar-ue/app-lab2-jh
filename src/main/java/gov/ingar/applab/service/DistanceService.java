package gov.ingar.applab.service;

import gov.ingar.applab.domain.Distance;
import gov.ingar.applab.repository.DistanceRepository;
import gov.ingar.applab.service.dto.DistanceDTO;
import gov.ingar.applab.service.mapper.DistanceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Distance.
 */
@Service
@Transactional
public class DistanceService {

    private final Logger log = LoggerFactory.getLogger(DistanceService.class);

    private final DistanceRepository distanceRepository;

    private final DistanceMapper distanceMapper;

    public DistanceService(DistanceRepository distanceRepository, DistanceMapper distanceMapper) {
        this.distanceRepository = distanceRepository;
        this.distanceMapper = distanceMapper;
    }

    /**
     * Save a distance.
     *
     * @param distanceDTO the entity to save
     * @return the persisted entity
     */
    public DistanceDTO save(DistanceDTO distanceDTO) {
        log.debug("Request to save Distance : {}", distanceDTO);
        Distance distance = distanceMapper.toEntity(distanceDTO);
        distance = distanceRepository.save(distance);
        return distanceMapper.toDto(distance);
    }

    /**
     * Get all the distances.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DistanceDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Distances");
        return distanceRepository.findAll(pageable)
            .map(distanceMapper::toDto);
    }


    /**
     * Get one distance by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<DistanceDTO> findOne(Long id) {
        log.debug("Request to get Distance : {}", id);
        return distanceRepository.findById(id)
            .map(distanceMapper::toDto);
    }

    /**
     * Delete the distance by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Distance : {}", id);
        distanceRepository.deleteById(id);
    }
}
