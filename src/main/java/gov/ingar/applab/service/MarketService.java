package gov.ingar.applab.service;

import gov.ingar.applab.domain.Market;
import gov.ingar.applab.repository.MarketRepository;
import gov.ingar.applab.service.dto.MarketDTO;
import gov.ingar.applab.service.mapper.MarketMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Market.
 */
@Service
@Transactional
public class MarketService {

    private final Logger log = LoggerFactory.getLogger(MarketService.class);

    private final MarketRepository marketRepository;

    private final MarketMapper marketMapper;

    public MarketService(MarketRepository marketRepository, MarketMapper marketMapper) {
        this.marketRepository = marketRepository;
        this.marketMapper = marketMapper;
    }

    /**
     * Save a market.
     *
     * @param marketDTO the entity to save
     * @return the persisted entity
     */
    public MarketDTO save(MarketDTO marketDTO) {
        log.debug("Request to save Market : {}", marketDTO);
        Market market = marketMapper.toEntity(marketDTO);
        market = marketRepository.save(market);
        return marketMapper.toDto(market);
    }

    /**
     * Get all the markets.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MarketDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Markets");
        return marketRepository.findAll(pageable)
            .map(marketMapper::toDto);
    }


    /**
     * Get one market by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<MarketDTO> findOne(Long id) {
        log.debug("Request to get Market : {}", id);
        return marketRepository.findById(id)
            .map(marketMapper::toDto);
    }

    /**
     * Delete the market by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Market : {}", id);
        marketRepository.deleteById(id);
    }
}
