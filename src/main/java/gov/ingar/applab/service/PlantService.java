package gov.ingar.applab.service;

import gov.ingar.applab.domain.Plant;
import gov.ingar.applab.repository.PlantRepository;
import gov.ingar.applab.service.mapper.PlantMapper;
import gov.ingar.applab.repository.PlantRepository;
import gov.ingar.applab.service.dto.PlantDTO;
import gov.ingar.applab.service.mapper.PlantMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Plant.
 */
@Service
@Transactional
public class PlantService {

    private final Logger log = LoggerFactory.getLogger(PlantService.class);

    private final PlantRepository plantRepository;

    private final PlantMapper plantMapper;

    public PlantService(PlantRepository plantRepository, PlantMapper plantMapper) {
        this.plantRepository = plantRepository;
        this.plantMapper = plantMapper;
    }

    /**
     * Save a plant.
     *
     * @param plantDTO the entity to save
     * @return the persisted entity
     */
    public PlantDTO save(PlantDTO plantDTO) {
        log.debug("Request to save Plant : {}", plantDTO);
        Plant plant = plantMapper.toEntity(plantDTO);
        plant = plantRepository.save(plant);
        return plantMapper.toDto(plant);
    }

    /**
     * Get all the plants.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PlantDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Plants");
        return plantRepository.findAll(pageable)
            .map(plantMapper::toDto);
    }


    /**
     * Get one plant by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<PlantDTO> findOne(Long id) {
        log.debug("Request to get Plant : {}", id);
        return plantRepository.findById(id)
            .map(plantMapper::toDto);
    }

    /**
     * Delete the plant by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Plant : {}", id);
        plantRepository.deleteById(id);
    }
}
