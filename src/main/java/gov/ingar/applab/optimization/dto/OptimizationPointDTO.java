package gov.ingar.applab.optimization.dto;

import gov.ingar.applab.domain.ModelRunDef;
import gov.ingar.applab.service.dto.DistanceDTO;
import gov.ingar.applab.service.dto.MarketDTO;
import gov.ingar.applab.service.dto.PlantDTO;

import java.io.Serializable;
import java.util.List;

public class OptimizationPointDTO implements Serializable {
    List<MarketDTO> markets;
    List<PlantDTO> plants;
    List<DistanceDTO> distances;
    private ModelRunDef modelRunDef;
    private Long modelDefId;

    public List<MarketDTO> getMarkets() {
        return markets;
    }

    public void setMarkets(List<MarketDTO> markets) {
        this.markets = markets;
    }

    public List<PlantDTO> getPlants() {
        return plants;
    }

    public void setPlants(List<PlantDTO> plants) {
        this.plants = plants;
    }

    public List<DistanceDTO> getDistances() {
        return distances;
    }

    public void setDistances(List<DistanceDTO> distances) {
        this.distances = distances;
    }

    public ModelRunDef getModelRunDef() {
        return modelRunDef;
    }

    public void setModelRunDef(ModelRunDef modelRunDef) {
        this.modelRunDef = modelRunDef;
    }

    public Long getModelDefId() {
        return modelDefId;
    }

    public void setModelDefId(Long modelDefId) {
        this.modelDefId = modelDefId;
    }
}
