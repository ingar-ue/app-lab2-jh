package gov.ingar.applab.optimization;

import gov.ingar.applab.domain.ModelDef;
import gov.ingar.applab.domain.ModelRunDef;
import gov.ingar.applab.optimization.dto.OptimizationPointDTO;
import gov.ingar.applab.repository.DistanceRepository;
import gov.ingar.applab.repository.MarketRepository;
import gov.ingar.applab.repository.PlantRepository;
import gov.ingar.applab.security.SecurityUtils;
import gov.ingar.applab.service.ModelConfigService;
import gov.ingar.applab.service.ModelDefService;
import gov.ingar.applab.service.dto.DistanceDTO;
import gov.ingar.applab.service.dto.MarketDTO;
import gov.ingar.applab.service.dto.PlantDTO;
import gov.ingar.applab.service.mapper.DistanceMapper;
import gov.ingar.applab.service.mapper.MarketMapper;
import gov.ingar.applab.service.mapper.PlantMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class OptimizationPointService {

    private final Logger log = LoggerFactory.getLogger(OptimizationPointService.class);

    private final ModelDefService modelDefService;
    private final ModelConfigService modelConfigService;

    private final MarketRepository marketRepository;
    private final PlantRepository plantRepository;
    private final DistanceRepository distanceRepository;

    private final MarketMapper marketMapper;
    private final PlantMapper plantMapper;
    private final DistanceMapper distanceMapper;


    public OptimizationPointService(MarketRepository marketRepository, PlantRepository plantRepository, DistanceRepository distanceRepository,
                                    MarketMapper marketMapper, PlantMapper plantMapper, DistanceMapper distanceMapper,
                                    ModelDefService modelDefService, ModelConfigService modelConfigService) {
        this.modelDefService = modelDefService;
        this.modelConfigService = modelConfigService;

        this.marketRepository = marketRepository;
        this.plantRepository = plantRepository;
        this.distanceRepository = distanceRepository;

        this.marketMapper = marketMapper;
        this.plantMapper = plantMapper;
        this.distanceMapper = distanceMapper;

    }

    public OptimizationPointDTO loadCase(){
        ModelRunDef runDef = new ModelRunDef();
        ModelDef def = modelDefService.findOneByName("Transport Planner");
        OptimizationPointDTO op1 = loadCaseInput();

        runDef.setName(def.getName());
        runDef.setLanguage(def.getLanguage());
        runDef.setSolver(def.getSolver());
        runDef.setVersion(def.getCurrentVersion());
        runDef.setProgress(0);
        runDef.setUser(SecurityUtils.getCurrentUsername());

        op1.setModelRunDef(runDef);
        op1.setModelDefId(def.getId());

        return op1;
    }

    private OptimizationPointDTO loadCaseInput(){
        OptimizationPointDTO dto = new OptimizationPointDTO();

        dto.setMarkets(this.marketList());
        dto.setPlants(this.plantList());
        dto.setDistances(this.distanceList());

        return dto;
    }

    private List<MarketDTO> marketList() {
        return marketMapper.toDto(marketRepository.findAll());
    }

    private List<PlantDTO> plantList() {
        return plantMapper.toDto(plantRepository.findAll());
    }

    private List<DistanceDTO> distanceList() {
        return distanceMapper.toDto(distanceRepository.findAll());
    }

//    @Override
//    public void runOptimizationPoint(OptimizationPointDTO optimizationPointDTO) {
//        gamsOptimizationPoint1Runner.runOptimizationPoint1((OptimizationPoint1DTO) optimizationPointDTO);
//    }

}
