package gov.ingar.applab.repository;

import gov.ingar.applab.domain.ModelConfig;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the ModelConfig entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ModelConfigRepository extends JpaRepository<ModelConfig, Long> {

    List<ModelConfig> findAllByModelDefId(Long modelDefId);

}
