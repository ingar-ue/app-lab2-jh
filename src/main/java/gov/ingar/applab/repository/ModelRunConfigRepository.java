package gov.ingar.applab.repository;

import gov.ingar.applab.domain.ModelRunConfig;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ModelRunConfig entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ModelRunConfigRepository extends JpaRepository<ModelRunConfig, Long> {

}
