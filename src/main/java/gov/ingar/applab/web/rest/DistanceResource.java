package gov.ingar.applab.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.ingar.applab.service.DistanceService;
import gov.ingar.applab.web.rest.errors.BadRequestAlertException;
import gov.ingar.applab.web.rest.util.HeaderUtil;
import gov.ingar.applab.web.rest.util.PaginationUtil;
import gov.ingar.applab.service.dto.DistanceDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Distance.
 */
@RestController
@RequestMapping("/api")
public class DistanceResource {

    private final Logger log = LoggerFactory.getLogger(DistanceResource.class);

    private static final String ENTITY_NAME = "distance";

    private final DistanceService distanceService;

    public DistanceResource(DistanceService distanceService) {
        this.distanceService = distanceService;
    }

    /**
     * POST  /distances : Create a new distance.
     *
     * @param distanceDTO the distanceDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new distanceDTO, or with status 400 (Bad Request) if the distance has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/distances")
    @Timed
    public ResponseEntity<DistanceDTO> createDistance(@Valid @RequestBody DistanceDTO distanceDTO) throws URISyntaxException {
        log.debug("REST request to save Distance : {}", distanceDTO);
        if (distanceDTO.getId() != null) {
            throw new BadRequestAlertException("A new distance cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DistanceDTO result = distanceService.save(distanceDTO);
        return ResponseEntity.created(new URI("/api/distances/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /distances : Updates an existing distance.
     *
     * @param distanceDTO the distanceDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated distanceDTO,
     * or with status 400 (Bad Request) if the distanceDTO is not valid,
     * or with status 500 (Internal Server Error) if the distanceDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/distances")
    @Timed
    public ResponseEntity<DistanceDTO> updateDistance(@Valid @RequestBody DistanceDTO distanceDTO) throws URISyntaxException {
        log.debug("REST request to update Distance : {}", distanceDTO);
        if (distanceDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DistanceDTO result = distanceService.save(distanceDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, distanceDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /distances : get all the distances.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of distances in body
     */
    @GetMapping("/distances")
    @Timed
    public ResponseEntity<List<DistanceDTO>> getAllDistances(Pageable pageable) {
        log.debug("REST request to get a page of Distances");
        Page<DistanceDTO> page = distanceService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/distances");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /distances/:id : get the "id" distance.
     *
     * @param id the id of the distanceDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the distanceDTO, or with status 404 (Not Found)
     */
    @GetMapping("/distances/{id}")
    @Timed
    public ResponseEntity<DistanceDTO> getDistance(@PathVariable Long id) {
        log.debug("REST request to get Distance : {}", id);
        Optional<DistanceDTO> distanceDTO = distanceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(distanceDTO);
    }

    /**
     * DELETE  /distances/:id : delete the "id" distance.
     *
     * @param id the id of the distanceDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/distances/{id}")
    @Timed
    public ResponseEntity<Void> deleteDistance(@PathVariable Long id) {
        log.debug("REST request to delete Distance : {}", id);
        distanceService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
