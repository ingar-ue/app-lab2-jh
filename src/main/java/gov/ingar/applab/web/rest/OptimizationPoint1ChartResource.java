package gov.ingar.applab.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

@RestController
@RequestMapping("/api")
public class OptimizationPoint1ChartResource {

    private final Logger log = LoggerFactory.getLogger(AccountResource.class);

    @GetMapping("/mapData")
    @Timed
    public ResponseEntity<String> getMapData() throws Exception{
        InputStream is = this.getClass().getResourceAsStream("/argentina.json");
        log.debug("trayendo mapa del resource");
        String jsonTxt = IOUtils.toString(is, "UTF-8");
        return new ResponseEntity<>(jsonTxt, HttpStatus.OK);
    }

//    @GetMapping("/pointsData")
//    @Timed
//    public ResponseEntity<String> getPointsData() throws Exception{
//        File f = new File("src/main/resources/data/puntos.json");
//        InputStream is = new FileInputStream("src/main/resources/data/puntos.json");
//        String jsonTxt = IOUtils.toString(is, "UTF-8");
//        return new ResponseEntity<>(jsonTxt, HttpStatus.OK);
//    }
}
