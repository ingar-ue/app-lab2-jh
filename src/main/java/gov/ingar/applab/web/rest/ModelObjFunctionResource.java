package gov.ingar.applab.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.ingar.applab.service.ModelObjFunctionService;
import gov.ingar.applab.web.rest.errors.BadRequestAlertException;
import gov.ingar.applab.web.rest.util.HeaderUtil;
import gov.ingar.applab.web.rest.util.PaginationUtil;
import gov.ingar.applab.service.dto.ModelObjFunctionDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ModelObjFunction.
 */
@RestController
@RequestMapping("/api")
public class ModelObjFunctionResource {

    private final Logger log = LoggerFactory.getLogger(ModelObjFunctionResource.class);

    private static final String ENTITY_NAME = "modelObjFunction";

    private final ModelObjFunctionService modelObjFunctionService;

    public ModelObjFunctionResource(ModelObjFunctionService modelObjFunctionService) {
        this.modelObjFunctionService = modelObjFunctionService;
    }

    /**
     * POST  /model-obj-functions : Create a new modelObjFunction.
     *
     * @param modelObjFunctionDTO the modelObjFunctionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new modelObjFunctionDTO, or with status 400 (Bad Request) if the modelObjFunction has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/model-obj-functions")
    @Timed
    public ResponseEntity<ModelObjFunctionDTO> createModelObjFunction(@Valid @RequestBody ModelObjFunctionDTO modelObjFunctionDTO) throws URISyntaxException {
        log.debug("REST request to save ModelObjFunction : {}", modelObjFunctionDTO);
        if (modelObjFunctionDTO.getId() != null) {
            throw new BadRequestAlertException("A new modelObjFunction cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ModelObjFunctionDTO result = modelObjFunctionService.save(modelObjFunctionDTO);
        return ResponseEntity.created(new URI("/api/model-obj-functions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /model-obj-functions : Updates an existing modelObjFunction.
     *
     * @param modelObjFunctionDTO the modelObjFunctionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated modelObjFunctionDTO,
     * or with status 400 (Bad Request) if the modelObjFunctionDTO is not valid,
     * or with status 500 (Internal Server Error) if the modelObjFunctionDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/model-obj-functions")
    @Timed
    public ResponseEntity<ModelObjFunctionDTO> updateModelObjFunction(@Valid @RequestBody ModelObjFunctionDTO modelObjFunctionDTO) throws URISyntaxException {
        log.debug("REST request to update ModelObjFunction : {}", modelObjFunctionDTO);
        if (modelObjFunctionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ModelObjFunctionDTO result = modelObjFunctionService.save(modelObjFunctionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, modelObjFunctionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /model-obj-functions : get all the modelObjFunctions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of modelObjFunctions in body
     */
    @GetMapping("/model-obj-functions")
    @Timed
    public ResponseEntity<List<ModelObjFunctionDTO>> getAllModelObjFunctions(Pageable pageable) {
        log.debug("REST request to get a page of ModelObjFunctions");
        Page<ModelObjFunctionDTO> page = modelObjFunctionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/model-obj-functions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /model-obj-functions/:id : get the "id" modelObjFunction.
     *
     * @param id the id of the modelObjFunctionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the modelObjFunctionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/model-obj-functions/{id}")
    @Timed
    public ResponseEntity<ModelObjFunctionDTO> getModelObjFunction(@PathVariable Long id) {
        log.debug("REST request to get ModelObjFunction : {}", id);
        Optional<ModelObjFunctionDTO> modelObjFunctionDTO = modelObjFunctionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(modelObjFunctionDTO);
    }

    /**
     * DELETE  /model-obj-functions/:id : delete the "id" modelObjFunction.
     *
     * @param id the id of the modelObjFunctionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/model-obj-functions/{id}")
    @Timed
    public ResponseEntity<Void> deleteModelObjFunction(@PathVariable Long id) {
        log.debug("REST request to delete ModelObjFunction : {}", id);
        modelObjFunctionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
