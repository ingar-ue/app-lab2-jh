package gov.ingar.applab.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.ingar.applab.service.EnvConfigService;
import gov.ingar.applab.web.rest.errors.BadRequestAlertException;
import gov.ingar.applab.web.rest.util.HeaderUtil;
import gov.ingar.applab.web.rest.util.PaginationUtil;
import gov.ingar.applab.service.dto.EnvConfigDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing EnvConfig.
 */
@RestController
@RequestMapping("/api")
public class EnvConfigResource {

    private final Logger log = LoggerFactory.getLogger(EnvConfigResource.class);

    private static final String ENTITY_NAME = "envConfig";

    private final EnvConfigService envConfigService;

    public EnvConfigResource(EnvConfigService envConfigService) {
        this.envConfigService = envConfigService;
    }

    /**
     * POST  /env-configs : Create a new envConfig.
     *
     * @param envConfigDTO the envConfigDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new envConfigDTO, or with status 400 (Bad Request) if the envConfig has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/env-configs")
    @Timed
    public ResponseEntity<EnvConfigDTO> createEnvConfig(@Valid @RequestBody EnvConfigDTO envConfigDTO) throws URISyntaxException {
        log.debug("REST request to save EnvConfig : {}", envConfigDTO);
        if (envConfigDTO.getId() != null) {
            throw new BadRequestAlertException("A new envConfig cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EnvConfigDTO result = envConfigService.save(envConfigDTO);
        return ResponseEntity.created(new URI("/api/env-configs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /env-configs : Updates an existing envConfig.
     *
     * @param envConfigDTO the envConfigDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated envConfigDTO,
     * or with status 400 (Bad Request) if the envConfigDTO is not valid,
     * or with status 500 (Internal Server Error) if the envConfigDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/env-configs")
    @Timed
    public ResponseEntity<EnvConfigDTO> updateEnvConfig(@Valid @RequestBody EnvConfigDTO envConfigDTO) throws URISyntaxException {
        log.debug("REST request to update EnvConfig : {}", envConfigDTO);
        if (envConfigDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EnvConfigDTO result = envConfigService.save(envConfigDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, envConfigDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /env-configs : get all the envConfigs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of envConfigs in body
     */
    @GetMapping("/env-configs")
    @Timed
    public ResponseEntity<List<EnvConfigDTO>> getAllEnvConfigs(Pageable pageable) {
        log.debug("REST request to get a page of EnvConfigs");
        Page<EnvConfigDTO> page = envConfigService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/env-configs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /env-configs/:id : get the "id" envConfig.
     *
     * @param id the id of the envConfigDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the envConfigDTO, or with status 404 (Not Found)
     */
    @GetMapping("/env-configs/{id}")
    @Timed
    public ResponseEntity<EnvConfigDTO> getEnvConfig(@PathVariable Long id) {
        log.debug("REST request to get EnvConfig : {}", id);
        Optional<EnvConfigDTO> envConfigDTO = envConfigService.findOne(id);
        return ResponseUtil.wrapOrNotFound(envConfigDTO);
    }

    /**
     * DELETE  /env-configs/:id : delete the "id" envConfig.
     *
     * @param id the id of the envConfigDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/env-configs/{id}")
    @Timed
    public ResponseEntity<Void> deleteEnvConfig(@PathVariable Long id) {
        log.debug("REST request to delete EnvConfig : {}", id);
        envConfigService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/get-solver-server-url")
    @Timed
    public ResponseEntity<EnvConfigDTO> getSolverServerURL() {
        Optional<EnvConfigDTO> envConfigDTO = envConfigService.findOneByParam("Solver Server URL");
        return ResponseUtil.wrapOrNotFound(envConfigDTO);
    }
}
