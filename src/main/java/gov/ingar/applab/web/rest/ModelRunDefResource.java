package gov.ingar.applab.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.ingar.applab.service.ModelRunDefService;
import gov.ingar.applab.service.dto.ModelRunDefResultDTO;
import gov.ingar.applab.web.rest.errors.BadRequestAlertException;
import gov.ingar.applab.web.rest.util.HeaderUtil;
import gov.ingar.applab.web.rest.util.PaginationUtil;
import gov.ingar.applab.service.dto.ModelRunDefDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ModelRunDef.
 */
@RestController
@RequestMapping("/api")
public class ModelRunDefResource {

    private final Logger log = LoggerFactory.getLogger(ModelRunDefResource.class);

    private static final String ENTITY_NAME = "modelRunDef";

    private final ModelRunDefService modelRunDefService;

    public ModelRunDefResource(ModelRunDefService modelRunDefService) {
        this.modelRunDefService = modelRunDefService;
    }

    /**
     * POST  /model-run-defs : Create a new modelRunDef.
     *
     * @param modelRunDefResultDTO the modelRunDefDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new modelRunDefDTO, or with status 400 (Bad Request) if the modelRunDef has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/model-run-defs")
    @Timed
    public ResponseEntity<ModelRunDefDTO> createModelRunDef(@Valid @RequestBody ModelRunDefResultDTO modelRunDefResultDTO) throws URISyntaxException {
        log.debug("REST request to save ModelRunDef : {}", modelRunDefResultDTO);
        if (modelRunDefResultDTO.getId() != null) {
            throw new BadRequestAlertException("A new modelRunDef cannot already have an ID", ENTITY_NAME, "idexists");
        }
        modelRunDefResultDTO.setRunDate(LocalDate.now());
//        log.debug("result: {}", modelRunDefResultDTO.getUserResult());
//        log.debug("result solutions: {}", modelRunDefResultDTO.getUserResult().getSolution());

        ModelRunDefDTO modelRunDefDTO = new ModelRunDefDTO(modelRunDefResultDTO);
        ModelRunDefDTO result = modelRunDefService.save(modelRunDefDTO);
        return ResponseEntity.created(new URI("/api/model-run-defs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /model-run-defs : Updates an existing modelRunDef.
     *
     * @param modelRunDefDTO the modelRunDefDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated modelRunDefDTO,
     * or with status 400 (Bad Request) if the modelRunDefDTO is not valid,
     * or with status 500 (Internal Server Error) if the modelRunDefDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/model-run-defs")
    @Timed
    public ResponseEntity<ModelRunDefDTO> updateModelRunDef(@Valid @RequestBody ModelRunDefDTO modelRunDefDTO) throws URISyntaxException {
        log.debug("REST request to update ModelRunDef : {}", modelRunDefDTO);
        if (modelRunDefDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ModelRunDefDTO result = modelRunDefService.save(modelRunDefDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, modelRunDefDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /model-run-defs : get all the modelRunDefs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of modelRunDefs in body
     */
    @GetMapping("/model-run-defs")
    @Timed
    public ResponseEntity<List<ModelRunDefResultDTO>> getAllModelRunDefs(Pageable pageable) {
        log.debug("REST request to get a page of ModelRunDefs");
        Page<ModelRunDefResultDTO> page = modelRunDefService.findAll(pageable)
            .map(modelRunDefDTO -> {
                ModelRunDefResultDTO ret = new ModelRunDefResultDTO(modelRunDefDTO);
                return ret;
            });
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/model-run-defs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /model-run-defs/:id : get the "id" modelRunDef.
     *
     * @param id the id of the modelRunDefDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the modelRunDefDTO, or with status 404 (Not Found)
     */
    @GetMapping("/model-run-defs/{id}")
    @Timed
    public ResponseEntity<ModelRunDefResultDTO> getModelRunDef(@PathVariable Long id) {
        log.debug("REST request to get ModelRunDef : {}", id);
        Optional<ModelRunDefResultDTO> modelRunDefDTO = modelRunDefService.findOne(id)
            .map(DTO -> {
            ModelRunDefResultDTO ret = new ModelRunDefResultDTO(DTO);
            return ret;
        });
        return ResponseUtil.wrapOrNotFound(modelRunDefDTO);
    }

    /**
     * DELETE  /model-run-defs/:id : delete the "id" modelRunDef.
     *
     * @param id the id of the modelRunDefDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/model-run-defs/{id}")
    @Timed
    public ResponseEntity<Void> deleteModelRunDef(@PathVariable Long id) {
        log.debug("REST request to delete ModelRunDef : {}", id);
        modelRunDefService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
