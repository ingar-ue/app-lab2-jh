package gov.ingar.applab.web.rest;

import com.codahale.metrics.annotation.Timed;
import gov.ingar.applab.domain.ModelDef;
import gov.ingar.applab.service.ModelConfigService;
import gov.ingar.applab.service.ModelDefService;
import gov.ingar.applab.service.ModelObjFunctionService;
import gov.ingar.applab.service.ModelVersionService;
import gov.ingar.applab.web.rest.errors.BadRequestAlertException;
import gov.ingar.applab.web.rest.util.HeaderUtil;
import gov.ingar.applab.web.rest.util.PaginationUtil;
import gov.ingar.applab.service.dto.ModelDefDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ModelDef.
 */
@RestController
@RequestMapping("/api")
public class ModelDefResource {

    private final Logger log = LoggerFactory.getLogger(ModelDefResource.class);

    private static final String ENTITY_NAME = "modelDef";

    private final ModelDefService modelDefService;
    private final ModelConfigService modelConfigService;
    private final ModelVersionService modelVersionService;
    private final ModelObjFunctionService modelObjFunctionService;

    public ModelDefResource(ModelDefService modelDefService, ModelConfigService modelConfigService,
                            ModelVersionService modelVersionService,ModelObjFunctionService modelObjFunctionService) {
        this.modelDefService = modelDefService;
        this.modelConfigService = modelConfigService;
        this.modelVersionService = modelVersionService;
        this.modelObjFunctionService = modelObjFunctionService;
    }

    /**
     * POST  /model-defs : Create a new modelDef.
     *
     * @param modelDefDTO the modelDefDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new modelDefDTO, or with status 400 (Bad Request) if the modelDef has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/model-defs")
    @Timed
    public ResponseEntity<ModelDefDTO> createModelDef(@Valid @RequestBody ModelDefDTO modelDefDTO) throws URISyntaxException {
        log.debug("REST request to save ModelDef : {}", modelDefDTO);
        if (modelDefDTO.getId() != null) {
            throw new BadRequestAlertException("A new modelDef cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ModelDefDTO result = modelDefService.save(modelDefDTO);
        return ResponseEntity.created(new URI("/api/model-defs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /model-defs : Updates an existing modelDef.
     *
     * @param modelDefDTO the modelDefDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated modelDefDTO,
     * or with status 400 (Bad Request) if the modelDefDTO is not valid,
     * or with status 500 (Internal Server Error) if the modelDefDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/model-defs")
    @Timed
    public ResponseEntity<ModelDefDTO> updateModelDef(@Valid @RequestBody ModelDefDTO modelDefDTO) throws URISyntaxException {
        log.debug("REST request to update ModelDef : {}", modelDefDTO);
        if (modelDefDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ModelDefDTO result = modelDefService.save(modelDefDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, modelDefDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /model-defs : get all the modelDefs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of modelDefs in body
     */
    @GetMapping("/model-defs")
    @Timed
    public ResponseEntity<List<ModelDefDTO>> getAllModelDefs(Pageable pageable) {
        log.debug("REST request to get a page of ModelDefs");
        Page<ModelDefDTO> page = modelDefService.findAll(pageable);
        List<ModelDefDTO> modelDefs = page.getContent();
        for (ModelDefDTO modelDef : modelDefs){
            modelDef.setModelConfigs(modelConfigService.findAllByModelDefId(modelDef.getId()));
            modelDef.setModelVersions(modelVersionService.findAllByModelDefId(modelDef.getId()));
            modelDef.setModelObjFunctions(modelObjFunctionService.findAllByModelDefId(modelDef.getId()));
        }

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/model-defs");
        return new ResponseEntity<>(modelDefs, headers, HttpStatus.OK);
    }

    /**
     * GET  /model-defs/:id : get the "id" modelDef.
     *
     * @param id the id of the modelDefDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the modelDefDTO, or with status 404 (Not Found)
     */
    @GetMapping("/model-defs/{id}")
    @Timed
    public ResponseEntity<ModelDefDTO> getModelDef(@PathVariable Long id) {
        log.debug("REST request to get ModelDef : {}", id);
        Optional<ModelDefDTO> modelDefDTO = modelDefService.findOne(id);
        return ResponseUtil.wrapOrNotFound(modelDefDTO);
    }

    /**
     * DELETE  /model-defs/:id : delete the "id" modelDef.
     *
     * @param id the id of the modelDefDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/model-defs/{id}")
    @Timed
    public ResponseEntity<Void> deleteModelDef(@PathVariable Long id) {
        log.debug("REST request to delete ModelDef : {}", id);
        modelDefService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
