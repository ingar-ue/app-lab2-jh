import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppLab2SharedModule } from 'app/shared';
import { SummaryService } from './';

const ENTITY_STATES = [];

@NgModule({
    imports: [AppLab2SharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [],
    entryComponents: [],
    providers: [SummaryService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLabSummaryModule {}
