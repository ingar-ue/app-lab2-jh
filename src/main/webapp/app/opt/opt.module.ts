import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppLabAboutModule } from './about/about.module';
import { AppLabSummaryModule } from './summary/summary.module';

@NgModule({
    imports: [AppLabAboutModule, AppLabSummaryModule],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLab2OptModule {}
