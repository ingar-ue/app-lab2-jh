import './vendor.ts';

import { NgModule, Injector } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { Ng2Webstorage, LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { JhiEventManager } from 'ng-jhipster';

import { AuthInterceptor } from './blocks/interceptor/auth.interceptor';
import { AuthExpiredInterceptor } from './blocks/interceptor/auth-expired.interceptor';
import { ErrorHandlerInterceptor } from './blocks/interceptor/errorhandler.interceptor';
import { NotificationInterceptor } from './blocks/interceptor/notification.interceptor';
import { AppLab2SharedModule } from 'app/shared';
import { AppLab2CoreModule } from 'app/core';
import { AppLab2AppRoutingModule } from './app-routing.module';
import { AppLab2HomeModule } from './home/home.module';
import { AppLab2AccountModule } from './account/account.module';
import { AppLab2EntityModule } from './entities/entity.module';
import * as moment from 'moment';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { JhiMainComponent, NavbarComponent, FooterComponent, PageRibbonComponent, ActiveMenuDirective, ErrorComponent } from './layouts';
import { StompRService } from '@stomp/ng2-stompjs';
import { AgGridModule } from 'ag-grid-angular';
import { ChartModule } from 'angular2-chartjs';
import { AppLab2OptModule } from 'app/opt/opt.module';

@NgModule({
    imports: [
        BrowserModule,
        AppLab2AppRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-' }),
        AppLab2SharedModule,
        AppLab2CoreModule,
        AppLab2HomeModule,
        AppLab2AccountModule,
        AppLab2EntityModule,
        AppLab2OptModule,
        AgGridModule.withComponents([]),
        ChartModule
        // jhipster-needle-angular-add-module JHipster will add new module here
    ],
    declarations: [JhiMainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true,
            deps: [LocalStorageService, SessionStorageService]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthExpiredInterceptor,
            multi: true,
            deps: [Injector]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorHandlerInterceptor,
            multi: true,
            deps: [JhiEventManager]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: NotificationInterceptor,
            multi: true,
            deps: [Injector]
        },
        StompRService
    ],
    bootstrap: [JhiMainComponent]
})
export class AppLab2AppModule {}
