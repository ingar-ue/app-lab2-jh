import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IDistance } from 'app/shared/model/distance.model';
import { DistanceService } from './distance.service';
import { IPlant } from 'app/shared/model/plant.model';
import { PlantService } from 'app/entities/plant';
import { IMarket } from 'app/shared/model/market.model';
import { MarketService } from 'app/entities/market';

@Component({
    selector: 'jhi-distance-update',
    templateUrl: './distance-update.component.html'
})
export class DistanceUpdateComponent implements OnInit {
    private _distance: IDistance;
    isSaving: boolean;

    plants: IPlant[];

    markets: IMarket[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private distanceService: DistanceService,
        private plantService: PlantService,
        private marketService: MarketService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ distance }) => {
            this.distance = distance;
        });
        this.plantService.query().subscribe(
            (res: HttpResponse<IPlant[]>) => {
                this.plants = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.marketService.query().subscribe(
            (res: HttpResponse<IMarket[]>) => {
                this.markets = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.distance.id !== undefined) {
            this.subscribeToSaveResponse(this.distanceService.update(this.distance));
        } else {
            this.subscribeToSaveResponse(this.distanceService.create(this.distance));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IDistance>>) {
        result.subscribe((res: HttpResponse<IDistance>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackPlantById(index: number, item: IPlant) {
        return item.id;
    }

    trackMarketById(index: number, item: IMarket) {
        return item.id;
    }
    get distance() {
        return this._distance;
    }

    set distance(distance: IDistance) {
        this._distance = distance;
    }
}
