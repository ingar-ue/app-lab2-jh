import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ModelDef } from 'app/shared/model/model-def.model';
import { ModelDefService } from './model-def.service';
import { ModelDefComponent } from './model-def.component';
import { ModelDefDetailComponent } from './model-def-detail.component';
import { ModelDefUpdatePopupComponent } from './model-def-update.component';
import { ModelDefDeletePopupComponent } from './model-def-delete-dialog.component';
import { IModelDef } from 'app/shared/model/model-def.model';

@Injectable({ providedIn: 'root' })
export class ModelDefResolve implements Resolve<IModelDef> {
    constructor(private service: ModelDefService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((modelDef: HttpResponse<ModelDef>) => modelDef.body));
        }
        return of(new ModelDef());
    }
}

export const modelDefRoute: Routes = [
    {
        path: 'model-def',
        component: ModelDefComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLab2App.modelDef.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'model-def/:id/view',
        component: ModelDefDetailComponent,
        resolve: {
            modelDef: ModelDefResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLab2App.modelDef.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const modelDefPopupRoute: Routes = [
    {
        path: 'model-def/:id/delete',
        component: ModelDefDeletePopupComponent,
        resolve: {
            modelDef: ModelDefResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLab2App.modelDef.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'model-def/:id/edit',
        component: ModelDefUpdatePopupComponent,
        resolve: {
            modelDef: ModelDefResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLab2App.modelDef.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'model-def/new',
        component: ModelDefUpdatePopupComponent,
        resolve: {
            modelDef: ModelDefResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLab2App.modelDef.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
