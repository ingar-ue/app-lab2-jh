import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IEnvConfig } from 'app/shared/model/env-config.model';

type EntityResponseType = HttpResponse<IEnvConfig>;
type EntityArrayResponseType = HttpResponse<IEnvConfig[]>;

@Injectable({ providedIn: 'root' })
export class EnvConfigService {
    private resourceUrl = SERVER_API_URL + 'api/env-configs';

    constructor(private http: HttpClient) {}

    create(envConfig: IEnvConfig): Observable<EntityResponseType> {
        return this.http.post<IEnvConfig>(this.resourceUrl, envConfig, { observe: 'response' });
    }

    update(envConfig: IEnvConfig): Observable<EntityResponseType> {
        return this.http.put<IEnvConfig>(this.resourceUrl, envConfig, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IEnvConfig>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IEnvConfig[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
