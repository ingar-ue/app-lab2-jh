import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppLab2SharedModule } from 'app/shared';
import {
    EnvConfigComponent,
    EnvConfigDetailComponent,
    EnvConfigUpdateComponent,
    EnvConfigDeletePopupComponent,
    EnvConfigDeleteDialogComponent,
    envConfigRoute,
    envConfigPopupRoute
} from './';

const ENTITY_STATES = [...envConfigRoute, ...envConfigPopupRoute];

@NgModule({
    imports: [AppLab2SharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        EnvConfigComponent,
        EnvConfigDetailComponent,
        EnvConfigUpdateComponent,
        EnvConfigDeleteDialogComponent,
        EnvConfigDeletePopupComponent
    ],
    entryComponents: [EnvConfigComponent, EnvConfigUpdateComponent, EnvConfigDeleteDialogComponent, EnvConfigDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLab2EnvConfigModule {}
