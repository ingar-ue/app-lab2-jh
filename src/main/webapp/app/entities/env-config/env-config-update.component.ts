import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IEnvConfig } from 'app/shared/model/env-config.model';
import { EnvConfigService } from './env-config.service';

@Component({
    selector: 'jhi-env-config-update',
    templateUrl: './env-config-update.component.html'
})
export class EnvConfigUpdateComponent implements OnInit {
    private _envConfig: IEnvConfig;
    isSaving: boolean;

    constructor(private envConfigService: EnvConfigService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ envConfig }) => {
            this.envConfig = envConfig;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.envConfig.id !== undefined) {
            this.subscribeToSaveResponse(this.envConfigService.update(this.envConfig));
        } else {
            this.subscribeToSaveResponse(this.envConfigService.create(this.envConfig));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IEnvConfig>>) {
        result.subscribe((res: HttpResponse<IEnvConfig>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get envConfig() {
        return this._envConfig;
    }

    set envConfig(envConfig: IEnvConfig) {
        this._envConfig = envConfig;
    }
}
