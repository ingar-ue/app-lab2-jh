import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppLab2EnvConfigModule } from './env-config/env-config.module';
import { AppLab2ModelDefModule } from './model-def/model-def.module';
import { AppLab2ModelConfigModule } from './model-config/model-config.module';
import { AppLab2ModelObjFunctionModule } from './model-obj-function/model-obj-function.module';
import { AppLab2ModelRunDefModule } from './model-run-def/model-run-def.module';
import { AppLab2ModelRunConfigModule } from './model-run-config/model-run-config.module';
import { AppLab2MarketModule } from './market/market.module';
import { AppLab2PlantModule } from './plant/plant.module';
import { AppLab2DistanceModule } from './distance/distance.module';
import { AppLab2ModelVersionModule } from 'app/entities/model-version/model-version.module';
import { AppLab2OptimizationPoint1Module } from 'app/entities/optimization-point-1/optimization-point-1.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        AppLab2EnvConfigModule,
        AppLab2ModelDefModule,
        AppLab2ModelConfigModule,
        AppLab2ModelVersionModule,
        AppLab2ModelObjFunctionModule,
        AppLab2ModelRunDefModule,
        AppLab2ModelRunConfigModule,
        AppLab2MarketModule,
        AppLab2PlantModule,
        AppLab2DistanceModule,
        AppLab2OptimizationPoint1Module
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLab2EntityModule {}
