import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ModelObjFunction } from 'app/shared/model/model-obj-function.model';
import { ModelObjFunctionService } from './model-obj-function.service';
import { ModelObjFunctionUpdatePopupComponent } from './model-obj-function-update.component';
import { ModelObjFunctionDeletePopupComponent } from './model-obj-function-delete-dialog.component';
import { IModelObjFunction } from 'app/shared/model/model-obj-function.model';

@Injectable({ providedIn: 'root' })
export class ModelObjFunctionResolve implements Resolve<IModelObjFunction> {
    constructor(private service: ModelObjFunctionService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((modelObjFunction: HttpResponse<ModelObjFunction>) => modelObjFunction.body));
        }
        return of(new ModelObjFunction());
    }
}

export const modelObjFunctionRoute: Routes = [];

export const modelObjFunctionPopupRoute: Routes = [
    {
        path: 'model-obj-function/:id/delete',
        component: ModelObjFunctionDeletePopupComponent,
        resolve: {
            modelObjFunction: ModelObjFunctionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLab2App.modelObjFunction.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'model-obj-function/:id/edit/:modelDefId',
        component: ModelObjFunctionUpdatePopupComponent,
        resolve: {
            modelObjFunction: ModelObjFunctionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLab2App.modelObjFunction.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'model-obj-function/new/:modelDefId',
        component: ModelObjFunctionUpdatePopupComponent,
        resolve: {
            modelObjFunction: ModelObjFunctionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLab2App.modelObjFunction.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
