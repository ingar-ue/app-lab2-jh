import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IOptimizationPoint1 } from 'app/shared/model/optimization-point-1.model';
import { OptimizationPoint1Service } from './optimization-point-1.service';

@Component({
    selector: 'jhi-optimization-point-1-execute-dialog',
    templateUrl: './optimization-point-1-execute-dialog.component.html'
})
export class OptimizationPoint1ExecuteDialogComponent {
    optimizationPoint1: IOptimizationPoint1;

    constructor(
        private optimizationPoint1Service: OptimizationPoint1Service,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    optimizationPoint1Execute() {
        console.log(this.optimizationPoint1);
        this.optimizationPoint1Service.run(this.optimizationPoint1).subscribe(response => {
            this.eventManager.broadcast({
                name: 'optimizationPoint1ListModification',
                content: 'OptimizationPoint1 generated'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-optimization-point-1-execute-popup',
    template: ''
})
export class OptimizationPoint1ExecutePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ optimizationPoint1 }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(OptimizationPoint1ExecuteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.optimizationPoint1 = optimizationPoint1;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
