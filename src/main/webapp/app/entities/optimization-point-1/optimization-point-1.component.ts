import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import {
    IOptimizationPoint1,
    IPyomoInput,
    IPyomoInputDistance,
    IPyomoInputMarket,
    IPyomoInputPlant,
    IResult,
    ResultSolution
} from 'app/shared/model/optimization-point-1.model';
import { Principal } from 'app/core';

import { OptimizationPoint1Service } from './optimization-point-1.service';

import 'ag-grid/dist/styles/ag-grid.css';
import 'ag-grid/dist/styles/ag-theme-balham.css';
import { IModelRunDef } from 'app/shared/model/model-run-def.model';
import { IModelObjFunction } from 'app/shared/model/model-obj-function.model';
import { IModelRunConfig } from 'app/shared/model/model-run-config.model';
import { IEnvConfig } from 'app/shared/model/env-config.model';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';

@Component({
    selector: 'jhi-optimization-point-1',
    templateUrl: './optimization-point-1.component.html'
})
export class OptimizationPoint1Component implements OnInit, OnDestroy {
    currentAccount: any;
    eventSubscriber: Subscription;
    optimizationPoint1: IOptimizationPoint1;
    modelRunDef: IModelRunDef;
    result: IResult;
    modelRunDefChange = 0;
    objFunctions: IModelObjFunction[];
    configs: IModelRunConfig[];
    configValuesLoaded = true;
    executingOP2: boolean;
    activeTabId: string;
    alertClosed: boolean;
    alertMessage = '';
    sub;

    /******************* TABLE DATA **************************/
    // 1 - markets
    columnDefs1 = [
        { headerName: 'Nombre', field: 'name', editable: false },
        { headerName: 'Domicilio', field: 'address', editable: false },
        { headerName: 'Demanda (unidades)', field: 'demand', editable: false }
    ];
    // 2 - plants
    columnDefs2 = [
        { headerName: 'Nombre', field: 'name', editable: false },
        { headerName: 'Domicilio', field: 'address', editable: false },
        { headerName: 'Capacidad Productiva (unidades)', field: 'productiveCapacity', editable: false }
    ];
    // 3 - distances
    columnDefs3 = [
        { headerName: 'Planta de producción', field: 'plantName', editable: false },
        { headerName: 'Mercado', field: 'marketName', editable: true },
        { headerName: 'Distancia (km)', field: 'distance', editable: true }
    ];

    rowData1: any;
    rowData2: any;
    rowData3: any;

    grid1IsValid = false;
    grid2IsValid = false;
    grid3IsValid = false;

    grid1ValidMsg: String;
    grid2ValidMsg: String;
    grid3ValidMsg: String;

    gridApi: any;
    gridColumnApi: any;

    /************************ CLASS **************************/
    constructor(
        private optimizationPoint1Service: OptimizationPoint1Service,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private parseLinks: JhiParseLinks,
        private principal: Principal
    ) {
        this.modelRunDef = {};
        this.executingOP2 = false;
        this.activeTabId = 'dataTab';
        this.alertClosed = true;
    }

    ngOnInit() {
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.loadCase();
        this.registerChangeInOptimizationPoint1s();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInOptimizationPoint1s() {
        this.eventSubscriber = this.eventManager.subscribe('optimizationPoint1ListModification', response => this.reset());
    }

    reset() {
        this.loadCase();
        this.registerChangeInOptimizationPoint1s();
        // this.chartInit();
    }

    loadCase() {
        this.optimizationPoint1Service.loadCase().subscribe((res: HttpResponse<IOptimizationPoint1>) => {
            this.optimizationPoint1 = res.body;
            this.optimizationPoint1Service.checkRunning().subscribe((res1: HttpResponse<IModelRunDef>) => {
                if (res1.body) {
                    this.modelRunDef = res1.body;
                    this.modelRunDefChange++;
                    this.activeTabId = 'processTab';
                } else {
                    this.modelRunDef = this.optimizationPoint1.modelRunDef;
                    this.modelRunDefChange++;
                }
            });

            // Populate tables
            this.rowData1 = this.optimizationPoint1.markets;
            this.rowData2 = this.optimizationPoint1.plants;
            this.rowData3 = this.optimizationPoint1.distances;

            // Validate data grids and set message
            this.validateCaseData();

            this.optimizationPoint1Service
                .loadObjFunctions(this.optimizationPoint1.modelDefId)
                .subscribe((res1: HttpResponse<IModelObjFunction[]>) => {
                    this.objFunctions = res1.body;
                });
            this.optimizationPoint1Service
                .loadConfigs(this.optimizationPoint1.modelDefId)
                .subscribe((res2: HttpResponse<IModelRunConfig[]>) => {
                    this.configs = res2.body;
                });
        });
    }

    run() {
        let url = '';
        this.optimizationPoint1Service.getSolverServerURL().subscribe((res1: HttpResponse<IEnvConfig>) => {
            url = res1.body.value;
            this.modelRunDef.progress = 5;
            this.sub = Observable.interval(5000).subscribe(val => {
                if (this.modelRunDef.progress < 95) {
                    this.modelRunDef.progress += 5;
                }
            });
            this.optimizationPoint1Service.postJSON(url, JSON.parse(JSON.stringify(this.getInput()))).subscribe((res: JSON) => {
                this.sub.unsubscribe();
                this.modelRunDef.progress = 100;
                this.modelRunDef.status = 'FINISHED';

                const solution = res[0];
                const cost = res[1][1];
                const result: IResult = {};
                result.cost = cost;
                result.solution = [];

                for (let i = 0; i < solution.length; i++) {
                    const sol = solution[i];
                    const resultSolution: ResultSolution = new ResultSolution();
                    resultSolution.setMarket(sol['mercado'].replace(/-/g, ' '), this.optimizationPoint1.markets);
                    resultSolution.setPlant(sol['planta'].replace(/-/g, ' '), this.optimizationPoint1.plants);
                    resultSolution.value = sol['valor'];
                    result.solution.push(resultSolution);
                }

                this.modelRunDef.userResult = result;
                this.optimizationPoint1Service.saveModelRunDef(this.modelRunDef).subscribe();
                this.fireSuccessAlert('La ejecución ha finalizado, puede visualizar los resultados en la pestaña correspondiente');
                this.activeTabId = 'resultsTab';
            });
        });
    }

    getInput(): IPyomoInput {
        const input: IPyomoInput = {};
        input.markets = [];
        input.plants = [];
        input.distances = [];
        for (const market of this.optimizationPoint1.markets) {
            const marketInput: IPyomoInputMarket = {};
            marketInput.name = market.name.toLowerCase().replace(/ /g, '-');
            marketInput.demand = market.demand;
            input.markets.push(marketInput);
        }
        for (const plant of this.optimizationPoint1.plants) {
            const plantInput: IPyomoInputPlant = {};
            plantInput.name = plant.name.toLowerCase().replace(/ /g, '-');
            plantInput.productiveCapacity = plant.productiveCapacity;
            input.plants.push(plantInput);
        }
        for (const distance of this.optimizationPoint1.distances) {
            const distanceInput: IPyomoInputDistance = {};
            distanceInput.marketName = distance.marketName.toLowerCase().replace(/ /g, '-');
            distanceInput.plantName = distance.plantName.toLowerCase().replace(/ /g, '-');
            distanceInput.distance = distance.distance;
            input.distances.push(distanceInput);
        }
        input.freight = +this.configs[0].value;
        return input;
    }

    fireSuccessAlert(msg: string) {
        this.alertMessage = msg;
        this.alertClosed = false;
        setTimeout(() => (this.alertClosed = true), 10000);
    }

    configChange() {
        this.configValuesLoaded = true;
        for (const config of this.configs) {
            if (!config.value) {
                this.configValuesLoaded = false;
            }
        }
    }

    /******************* TABLE FUNCTIONS **************************/
    validateCaseData() {
        // grid 1
        if (this.rowData1.length > 0) {
            this.grid1IsValid = true;
            this.grid1ValidMsg = 'Datos correctos';
        } else {
            this.grid1IsValid = false;
            this.grid1ValidMsg = 'Error de validación: Debe haber al menos un registro cargado';
        }
        // grid 2
        if (this.rowData2.length > 0) {
            this.grid2IsValid = true;
            this.grid2ValidMsg = 'Datos correctos';
        } else {
            this.grid2IsValid = false;
            this.grid2ValidMsg = 'Error de validación: Debe haber al menos un registro cargado';
        }
        // grid 3
        if (this.rowData3.length > 0) {
            this.grid3IsValid = true;
            this.grid3ValidMsg = 'Datos correctos';
        } else {
            this.grid3IsValid = false;
            this.grid3ValidMsg = 'Error de validación: Debe haber al menos un registro cargado';
        }
    }

    onGridReady(params, id) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        // Setea ancho de las columnas para llenar ancho de la tabla
        params.api.sizeColumnsToFit();
        // Si la cantidad de filas a mostrar es mayor que 25 recorta el largo de la tabla
        console.log('params.api.getDisplayedRowCount() ' + params.api.getDisplayedRowCount());
        if (params.api.getDisplayedRowCount() > 10) {
            params.api.setGridAutoHeight(false);
            const theGrid: HTMLElement = <HTMLElement>document.querySelector('#' + id);
            theGrid.style.height = '300px';
        } else {
            params.api.setGridAutoHeight(true);
            const theGrid: HTMLElement = <HTMLElement>document.querySelector('#' + id);
            theGrid.style.height = '';
        }
    }
}
