import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { OptimizationPoint1 } from 'app/shared/model/optimization-point-1.model';
import { OptimizationPoint1Service } from './optimization-point-1.service';
import { OptimizationPoint1Component } from './optimization-point-1.component';
import { OptimizationPoint1ExecutePopupComponent } from './optimization-point-1-execute-dialog.component';
import { IOptimizationPoint1 } from 'app/shared/model/optimization-point-1.model';

@Injectable({ providedIn: 'root' })
export class OptimizationPoint1Resolve implements Resolve<IOptimizationPoint1> {
    constructor(private service: OptimizationPoint1Service) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        // if (id) {
        //     return this.service.find(id).pipe(map((optimizationPoint1: HttpResponse<OptimizationPoint1>) => optimizationPoint1.body));
        // }
        return of(new OptimizationPoint1());
    }
}

export const optimizationPoint1Route: Routes = [
    {
        path: 'optimization-point-1',
        component: OptimizationPoint1Component,
        data: {
            authorities: ['ROLE_DEMO', 'ROLE_USER'],
            pageTitle: 'appLab2App.optimization.op1.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const optimizationPoint1PopupRoute: Routes = [
    {
        path: 'optimization-point-1/execute/:op1',
        component: OptimizationPoint1ExecutePopupComponent,
        resolve: {
            optimizationPoint1: OptimizationPoint1Resolve
        },
        data: {
            authorities: ['ROLE_DEMO', 'ROLE_USER'],
            pageTitle: 'appLab2App.optimization.op1.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
