import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { IOptimizationPoint1 } from 'app/shared/model/optimization-point-1.model';
import { IModelObjFunction } from 'app/shared/model/model-obj-function.model';
import { IModelRunConfig } from 'app/shared/model/model-run-config.model';
import { IModelRunDef } from 'app/shared/model/model-run-def.model';
import { IEnvConfig } from 'app/shared/model/env-config.model';

type EntityResponseType = HttpResponse<IOptimizationPoint1>;
type EntityArrayResponseType = HttpResponse<IOptimizationPoint1[]>;

@Injectable({ providedIn: 'root' })
export class OptimizationPoint1Service {
    private resourceUrl = SERVER_API_URL + 'api/';

    constructor(private http: HttpClient) {}

    postJSON(url: string, json: JSON): Observable<any> {
        return this.http.post(url, json).pipe(map((res: any) => res));
    }

    getSolverServerURL(): Observable<HttpResponse<IEnvConfig>> {
        return this.http
            .get(this.resourceUrl + 'get-solver-server-url', { observe: 'response' })
            .pipe(map((res: HttpResponse<IEnvConfig>) => res));
    }

    saveModelRunDef(runDef: IModelRunDef): Observable<HttpResponse<IModelRunDef>> {
        return this.http.post<IModelRunDef>(this.resourceUrl + 'model-run-defs', runDef, { observe: 'response' });
    }

    loadCase(): Observable<HttpResponse<IOptimizationPoint1>> {
        return this.http
            .get(this.resourceUrl + 'optimization-point-1-load', { observe: 'response' })
            .pipe(map((res: HttpResponse<IOptimizationPoint1>) => res));
    }

    run(optimizationPoint1: IOptimizationPoint1): Observable<HttpResponse<any>> {
        return this.http
            .put(this.resourceUrl + 'optimization-point-1-execute', optimizationPoint1, { observe: 'response' })
            .pipe(map((res: any) => res));
    }

    loadObjFunctions(modelDefId: number): Observable<HttpResponse<IModelObjFunction[]>> {
        return this.http
            .get(this.resourceUrl + 'optimization-point-1-load-obj-functions/' + modelDefId, { observe: 'response' })
            .pipe(map((res: HttpResponse<IModelObjFunction[]>) => res));
    }

    loadConfigs(modelDefId: number): Observable<HttpResponse<IModelRunConfig[]>> {
        return this.http
            .get(this.resourceUrl + 'optimization-point-1-load-configs/' + modelDefId, { observe: 'response' })
            .pipe(map((res: HttpResponse<IModelRunConfig[]>) => res));
    }

    checkRunning(): Observable<HttpResponse<IModelRunDef>> {
        return this.http
            .get(this.resourceUrl + 'optimization-point-1-check-running', { observe: 'response' })
            .pipe(map((res: HttpResponse<IModelRunDef>) => res));
    }

    private convertDateFromClient(optimizationPoint1: IOptimizationPoint1): IOptimizationPoint1 {
        const copy: IOptimizationPoint1 = Object.assign({}, optimizationPoint1, {
            // runDate:
            //     optimizationPoint1.runDate != null && optimizationPoint1.runDate.isValid()
            //         ? optimizationPoint1.runDate.format(DATE_FORMAT)
            //         : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        // res.body.runDate = res.body.runDate != null ? moment(res.body.runDate) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        // res.body.forEach((optimizationPoint1: IOptimizationPoint1) => {
        //     optimizationPoint1.runDate = optimizationPoint1.runDate != null ? moment(optimizationPoint1.runDate) : null;
        // });
        return res;
    }

    // getChartData(modelRunDefId, chart): Observable<HttpResponse<any>> {
    //     return this.http
    //         .get(`api/optimization-point-1-results-${chart}/${modelRunDefId}`, { observe: 'response' })
    //         .pipe(map((res: HttpResponse<any>) => res.body));
    // }
}
