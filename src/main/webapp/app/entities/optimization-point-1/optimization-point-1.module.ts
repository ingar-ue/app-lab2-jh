import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppLab2SharedModule } from 'app/shared';
import {
    OptimizationPoint1Component,
    OptimizationPoint1ExecutePopupComponent,
    OptimizationPoint1ExecuteDialogComponent,
    optimizationPoint1Route,
    optimizationPoint1PopupRoute
} from './';

import { AgGridModule } from 'ag-grid-angular';

import 'ag-grid/dist/styles/ag-grid.css';
import 'ag-grid/dist/styles/ag-theme-balham.css';

import { ChartModule } from 'angular2-chartjs';

const ENTITY_STATES = [...optimizationPoint1Route, ...optimizationPoint1PopupRoute];

@NgModule({
    imports: [AppLab2SharedModule, RouterModule.forChild(ENTITY_STATES), AgGridModule.withComponents([]), ChartModule],
    declarations: [OptimizationPoint1Component, OptimizationPoint1ExecutePopupComponent, OptimizationPoint1ExecuteDialogComponent],
    entryComponents: [OptimizationPoint1Component, OptimizationPoint1ExecutePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLab2OptimizationPoint1Module {}
