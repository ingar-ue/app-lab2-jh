import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IModelRunDef } from 'app/shared/model/model-run-def.model';
import { IOptimizationPoint1 } from 'app/shared/model/optimization-point-1.model';
import { IPlant } from 'app/shared/model/plant.model';
import { IMarket } from 'app/shared/model/market.model';

@Component({
    selector: 'jhi-model-run-def-detail',
    templateUrl: './model-run-def-detail.component.html'
})
export class ModelRunDefDetailComponent implements OnInit {
    modelRunDef: IModelRunDef;
    modelRunDefChange = 0;
    optimizationPoint1Input: IOptimizationPoint1 = {};

    constructor(private dataUtils: JhiDataUtils, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ modelRunDef }) => {
            this.modelRunDef = modelRunDef;

            const plantas: IPlant[] = [];
            const mercados: IMarket[] = [];
            for (const sol of this.modelRunDef.userResult.solution) {
                if (mercados.filter(mercado => mercado.name === sol.market.name).length === 0) {
                    mercados.push(sol.market);
                }
                if (plantas.filter(planta => planta.name === sol.plant.name).length === 0) {
                    plantas.push(sol.plant);
                }
            }
            this.optimizationPoint1Input.markets = mercados;
            this.optimizationPoint1Input.plants = plantas;

            this.modelRunDefChange++;
        });
    }

    previousState() {
        window.history.back();
    }
}
