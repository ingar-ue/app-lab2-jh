import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppLab2SharedModule } from 'app/shared';
import {
    ModelRunDefComponent,
    ModelRunDefDetailComponent,
    ModelRunDefDeletePopupComponent,
    ModelRunDefDeleteDialogComponent,
    modelRunDefRoute,
    modelRunDefPopupRoute
} from './';

import { ChartModule } from 'angular2-chartjs';
import { ModelRunDefCompareComponent } from 'app/entities/model-run-def/model-run-def-compare.component';

const ENTITY_STATES = [...modelRunDefRoute, ...modelRunDefPopupRoute];

@NgModule({
    imports: [AppLab2SharedModule, RouterModule.forChild(ENTITY_STATES), ChartModule],
    declarations: [
        ModelRunDefComponent,
        ModelRunDefDetailComponent,
        ModelRunDefDeleteDialogComponent,
        ModelRunDefDeletePopupComponent,
        ModelRunDefCompareComponent
    ],
    entryComponents: [ModelRunDefComponent, ModelRunDefDeleteDialogComponent, ModelRunDefDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLab2ModelRunDefModule {}
