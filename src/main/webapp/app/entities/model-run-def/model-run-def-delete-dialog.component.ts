import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IModelRunDef } from 'app/shared/model/model-run-def.model';
import { ModelRunDefService } from './model-run-def.service';

@Component({
    selector: 'jhi-model-run-def-delete-dialog',
    templateUrl: './model-run-def-delete-dialog.component.html'
})
export class ModelRunDefDeleteDialogComponent {
    modelRunDef: IModelRunDef;

    constructor(
        private modelRunDefService: ModelRunDefService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.modelRunDefService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'modelRunDefListModification',
                content: 'Deleted an modelRunDef'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-model-run-def-delete-popup',
    template: ''
})
export class ModelRunDefDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ modelRunDef }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ModelRunDefDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.modelRunDef = modelRunDef;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
