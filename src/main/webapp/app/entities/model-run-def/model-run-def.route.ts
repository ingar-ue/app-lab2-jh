import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ModelRunDef } from 'app/shared/model/model-run-def.model';
import { ModelRunDefService } from './model-run-def.service';
import { ModelRunDefComponent } from './model-run-def.component';
import { ModelRunDefDetailComponent } from './model-run-def-detail.component';
import { ModelRunDefDeletePopupComponent } from './model-run-def-delete-dialog.component';
import { IModelRunDef } from 'app/shared/model/model-run-def.model';
import { Observable } from 'rxjs/Observable';
import { ModelRunDefCompareComponent } from 'app/entities/model-run-def/model-run-def-compare.component';

@Injectable({ providedIn: 'root' })
export class ModelRunDefResolve implements Resolve<IModelRunDef> {
    constructor(private service: ModelRunDefService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((modelRunDef: HttpResponse<ModelRunDef>) => modelRunDef.body));
        }
        return of(new ModelRunDef());
    }
}

@Injectable({ providedIn: 'root' })
export class ModelRunDefCompareResolve implements Resolve<IModelRunDef[]> {
    constructor(private service: ModelRunDefService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const modelRunDefs: IModelRunDef[] = [];
        const id1 = route.params['id1'] ? route.params['id1'] : 0;
        if (id1 > 0) {
            this.service.find(id1).subscribe((res: HttpResponse<IModelRunDef>) => modelRunDefs.push(res.body));
        }
        const id2 = route.params['id2'] ? route.params['id2'] : null;
        if (id2 > 0) {
            this.service.find(id2).subscribe((res: HttpResponse<IModelRunDef>) => modelRunDefs.push(res.body));
        }
        const id3 = route.params['id3'] ? route.params['id3'] : null;
        if (id3 > 0) {
            this.service.find(id3).subscribe((res: HttpResponse<IModelRunDef>) => modelRunDefs.push(res.body));
        }

        return modelRunDefs;
    }
}

export const modelRunDefRoute: Routes = [
    {
        path: 'model-run-def',
        component: ModelRunDefComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLab2App.modelRunDef.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'model-run-def/:id/view',
        component: ModelRunDefDetailComponent,
        resolve: {
            modelRunDef: ModelRunDefResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLab2App.modelRunDef.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'model-run-def/compare/:id1/:id2/:id3',
        component: ModelRunDefCompareComponent,
        resolve: {
            modelRunDef: ModelRunDefCompareResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLab2App.modelRunDef.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const modelRunDefPopupRoute: Routes = [
    {
        path: 'model-run-def/:id/delete',
        component: ModelRunDefDeletePopupComponent,
        resolve: {
            modelRunDef: ModelRunDefResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLab2App.modelRunDef.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
