import { Component, OnInit } from '@angular/core';
import { IModelRunDef } from 'app/shared/model/model-run-def.model';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'jhi-model-run-def-compare',
    templateUrl: './model-run-def-compare.component.html'
})
export class ModelRunDefCompareComponent implements OnInit {
    modelRunDefs: IModelRunDef[];
    currentChart = 1;
    chartTitles: string[] = ['A', 'B', 'C'];

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ modelRunDef }) => {
            this.modelRunDefs = modelRunDef;
        });
    }
}
