import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { IModelRunDef } from 'app/shared/model/model-run-def.model';
import { Principal } from 'app/core';

import { ITEMS_PER_PAGE } from 'app/shared';
import { ModelRunDefService } from './model-run-def.service';
import { NgbCalendar, NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { Moment } from 'moment';

@Component({
    selector: 'jhi-model-run-def',
    templateUrl: './model-run-def.component.html',
    styleUrls: ['model-run-def.css']
})
export class ModelRunDefComponent implements OnInit, OnDestroy {
    modelRunDefs: IModelRunDef[];
    filteredModelRunDefs: IModelRunDef[];
    currentAccount: any;
    eventSubscriber: Subscription;
    itemsPerPage: number;
    links: any;
    page: any;
    predicate: any;
    queryCount: any;
    reverse: any;
    totalItems: number;
    searchNameString: string;

    modelRunDefCompare: IModelRunDef[] = [];
    compare: boolean[];
    compareId = [0, 0, 0];

    /******DATE PICKER VARIABLES*******/
    hoveredDate: NgbDate;
    fromDate: NgbDate;
    toDate: NgbDate;
    selectedDate: Moment;
    range_date = '';

    constructor(
        private modelRunDefService: ModelRunDefService,
        private jhiAlertService: JhiAlertService,
        private dataUtils: JhiDataUtils,
        private eventManager: JhiEventManager,
        private parseLinks: JhiParseLinks,
        private principal: Principal,
        private calendar: NgbCalendar
    ) {
        this.modelRunDefs = [];
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.links = {
            last: 0
        };
        this.predicate = 'id';
        this.reverse = true;

        /******DATE PICKER DECLARATIONS*******/
        this.toDate = calendar.getToday();
        this.fromDate = calendar.getPrev(calendar.getToday(), 'd', 28);
        this.range_date = this.formatDate(this.fromDate) + ' - ' + this.formatDate(this.toDate);
    }

    loadAll() {
        this.modelRunDefService
            .query({
                page: this.page,
                size: this.itemsPerPage,
                sort: this.sort()
            })
            .subscribe(
                (res: HttpResponse<IModelRunDef[]>) => this.paginateModelRunDefs(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    reset() {
        this.page = 0;
        this.modelRunDefs = [];
        this.loadAll();
    }

    loadPage(page) {
        this.page = page;
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInModelRunDefs();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IModelRunDef) {
        return item.id;
    }

    registerChangeInModelRunDefs() {
        this.eventSubscriber = this.eventManager.subscribe('modelRunDefListModification', response => this.reset());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private paginateModelRunDefs(data: IModelRunDef[], headers: HttpHeaders) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
        for (let i = 0; i < data.length; i++) {
            this.modelRunDefs.push(data[i]);
        }
        this.filterModelRunDefs();
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    filterModelRunDefs() {
        this.filteredModelRunDefs = [];
        this.compare = [];
        this.modelRunDefs.forEach(i => {
            console.log(i.userResult.solution);
            const date = new NgbDate(i.runDate.year(), i.runDate.month() + 1, i.runDate.date());
            if (
                (this.dateIsAfterDate(date, this.fromDate) && this.dateIsBeforeDate(date, this.toDate)) ||
                (this.dateIsAfterDate(date, this.fromDate) && this.dateEqualsDate(date, this.toDate)) ||
                (this.dateEqualsDate(date, this.fromDate) && this.dateIsBeforeDate(date, this.toDate)) ||
                (this.dateEqualsDate(date, this.fromDate) && this.dateEqualsDate(date, this.toDate))
            ) {
                this.filteredModelRunDefs.push(i);
                this.compare.push(false);
            }
        });
    }

    /******COMPARE FUNCTIONS***********/
    changeCompareList(isChecked: boolean, runDef: IModelRunDef) {
        if (isChecked) {
            this.modelRunDefCompare.push(runDef);
        } else {
            this.modelRunDefCompare.forEach((item, index) => {
                if (item === runDef) {
                    this.modelRunDefCompare.splice(index, 1);
                }
            });
        }
        this.compareId = [0, 0, 0];
        this.modelRunDefCompare.forEach((item, index) => {
            this.compareId[index] = item.id;
        });
    }

    /******DATE PICKER FUNCTIONS*******/
    onDateSelection(d) {
        const date = new NgbDate(this.selectedDate.year(), this.selectedDate.month() + 1, this.selectedDate.date());
        if (!this.fromDate && !this.toDate) {
            this.fromDate = date;
        } else if (
            this.fromDate &&
            this.toDate &&
            this.dateEqualsDate(this.fromDate, this.toDate) &&
            this.dateIsAfterDate(date, this.fromDate)
        ) {
            this.toDate = date;
            this.range_date = this.formatDate(this.fromDate) + ' - ' + this.formatDate(this.toDate);
            d.close();
            this.filterModelRunDefs();
        } else {
            this.toDate = date;
            this.fromDate = date;
        }
    }

    dateIsAfterDate(date1: NgbDate, date2: NgbDate): boolean {
        if (date2 === null || date1 === null) {
            return false;
        } else if (date1.year === date2.year) {
            if (date1.month === date2.month) {
                return date1.day === date2.day ? false : date1.day > date2.day;
            } else {
                return date1.month > date2.month;
            }
        } else {
            return date1.year > date2.year;
        }
    }

    dateIsBeforeDate(date1: NgbDate, date2: NgbDate): boolean {
        if (date2 === null || date1 === null) {
            return false;
        } else if (date1.year === date2.year) {
            if (date1.month === date2.month) {
                return date1.day === date2.day ? false : date1.day < date2.day;
            } else {
                return date1.month < date2.month;
            }
        } else {
            return date1.year < date2.year;
        }
    }

    dateEqualsDate(date1: NgbDate, date2: NgbDate): boolean {
        if (date2 === null || date1 === null) {
            return false;
        } else {
            return date1.year === date2.year && date1.month === date2.month && date1.day === date2.day;
        }
    }

    isHovered(date: NgbDate) {
        return (
            this.fromDate &&
            this.dateEqualsDate(this.fromDate, this.toDate) &&
            this.hoveredDate &&
            this.dateIsAfterDate(date, this.fromDate) &&
            this.dateIsBeforeDate(date, this.hoveredDate)
        );
    }

    isInside(date: NgbDate) {
        return this.dateIsAfterDate(date, this.fromDate) && this.dateIsBeforeDate(date, this.toDate);
    }

    isRange(date: NgbDate) {
        return (
            this.dateEqualsDate(date, this.fromDate) ||
            this.dateEqualsDate(date, this.toDate) ||
            this.isInside(date) ||
            this.isHovered(date)
        );
    }

    formatDate(date: NgbDate): string {
        return date.day + '/' + date.month + '/' + date.year;
    }
}
