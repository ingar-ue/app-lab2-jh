import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ModelRunConfig } from 'app/shared/model/model-run-config.model';
import { ModelRunConfigService } from './model-run-config.service';
import { ModelRunConfigComponent } from './model-run-config.component';
import { ModelRunConfigDetailComponent } from './model-run-config-detail.component';
import { ModelRunConfigUpdateComponent } from './model-run-config-update.component';
import { ModelRunConfigDeletePopupComponent } from './model-run-config-delete-dialog.component';
import { IModelRunConfig } from 'app/shared/model/model-run-config.model';

@Injectable({ providedIn: 'root' })
export class ModelRunConfigResolve implements Resolve<IModelRunConfig> {
    constructor(private service: ModelRunConfigService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((modelRunConfig: HttpResponse<ModelRunConfig>) => modelRunConfig.body));
        }
        return of(new ModelRunConfig());
    }
}

export const modelRunConfigRoute: Routes = [
    {
        path: 'model-run-config',
        component: ModelRunConfigComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLab2App.modelRunConfig.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'model-run-config/:id/view',
        component: ModelRunConfigDetailComponent,
        resolve: {
            modelRunConfig: ModelRunConfigResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLab2App.modelRunConfig.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'model-run-config/new',
        component: ModelRunConfigUpdateComponent,
        resolve: {
            modelRunConfig: ModelRunConfigResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLab2App.modelRunConfig.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'model-run-config/:id/edit',
        component: ModelRunConfigUpdateComponent,
        resolve: {
            modelRunConfig: ModelRunConfigResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLab2App.modelRunConfig.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const modelRunConfigPopupRoute: Routes = [
    {
        path: 'model-run-config/:id/delete',
        component: ModelRunConfigDeletePopupComponent,
        resolve: {
            modelRunConfig: ModelRunConfigResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLab2App.modelRunConfig.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
