import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppLab2SharedModule } from 'app/shared';
import {
    ModelConfigUpdateDialogComponent,
    ModelConfigUpdatePopupComponent,
    ModelConfigDeletePopupComponent,
    ModelConfigDeleteDialogComponent,
    modelConfigRoute,
    modelConfigPopupRoute
} from './';

const ENTITY_STATES = [...modelConfigRoute, ...modelConfigPopupRoute];

@NgModule({
    imports: [AppLab2SharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ModelConfigUpdateDialogComponent,
        ModelConfigUpdatePopupComponent,
        ModelConfigDeleteDialogComponent,
        ModelConfigDeletePopupComponent
    ],
    entryComponents: [
        ModelConfigUpdateDialogComponent,
        ModelConfigUpdatePopupComponent,
        ModelConfigDeleteDialogComponent,
        ModelConfigDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLab2ModelConfigModule {}
