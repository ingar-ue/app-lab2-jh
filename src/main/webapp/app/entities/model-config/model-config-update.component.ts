import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { IModelConfig } from 'app/shared/model/model-config.model';
import { ModelConfigService } from './model-config.service';
import { IModelDef } from 'app/shared/model/model-def.model';
import { ModelDefService } from 'app/entities/model-def';
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'jhi-model-config-update',
    templateUrl: './model-config-update.component.html'
})
export class ModelConfigUpdateDialogComponent implements OnInit {
    modelConfig: IModelConfig;
    isSaving: boolean;

    modeldefs: IModelDef[];
    modelDefDefined: boolean;

    constructor(
        private jhiAlertService: JhiAlertService,
        private modelConfigService: ModelConfigService,
        private modelDefService: ModelDefService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    ngOnInit() {
        this.isSaving = false;
        //
        // this.modelDefService.query().subscribe(
        //     (res: HttpResponse<IModelDef[]>) => {
        //         this.modeldefs = res.body;
        //     },
        //     (res: HttpErrorResponse) => this.onError(res.message)
        // );
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.modelConfig.id !== undefined) {
            this.subscribeToSaveResponse(this.modelConfigService.update(this.modelConfig));
        } else {
            this.subscribeToSaveResponse(this.modelConfigService.create(this.modelConfig));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IModelConfig>>) {
        result.subscribe((res: HttpResponse<IModelConfig>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.eventManager.broadcast({
            name: 'modelConfigListModification',
            content: 'Edited a modelConfig'
        });
        this.activeModal.dismiss(true);
    }

    private onSaveError() {
        this.isSaving = false;
        this.activeModal.dismiss(true);
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackModelDefById(index: number, item: IModelDef) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-model-config-update-popup',
    template: ''
})
export class ModelConfigUpdatePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ modelConfig }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ModelConfigUpdateDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.modelConfig = modelConfig;
                this.activatedRoute.params.subscribe(params => {
                    this.ngbModalRef.componentInstance.modelConfig.modelDefId = params['modelDefId'];
                });
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
