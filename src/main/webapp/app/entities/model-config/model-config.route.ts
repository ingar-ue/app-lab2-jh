import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ModelConfig } from 'app/shared/model/model-config.model';
import { ModelConfigService } from './model-config.service';
import { ModelConfigUpdatePopupComponent } from './model-config-update.component';
import { ModelConfigDeletePopupComponent } from './model-config-delete-dialog.component';
import { IModelConfig } from 'app/shared/model/model-config.model';

@Injectable({ providedIn: 'root' })
export class ModelConfigResolve implements Resolve<IModelConfig> {
    constructor(private service: ModelConfigService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((modelConfig: HttpResponse<ModelConfig>) => modelConfig.body));
        }
        return of(new ModelConfig());
    }
}

export const modelConfigRoute: Routes = [];

export const modelConfigPopupRoute: Routes = [
    {
        path: 'model-config/:id/delete',
        component: ModelConfigDeletePopupComponent,
        resolve: {
            modelConfig: ModelConfigResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLab2App.modelConfig.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'model-config/:id/edit/:modelDefId',
        component: ModelConfigUpdatePopupComponent,
        resolve: {
            modelConfig: ModelConfigResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLab2App.modelConfig.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'model-config/new/:modelDefId',
        component: ModelConfigUpdatePopupComponent,
        resolve: {
            modelConfig: ModelConfigResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLab2App.modelConfig.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
