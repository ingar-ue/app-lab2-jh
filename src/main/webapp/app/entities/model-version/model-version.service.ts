import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IModelVersion } from 'app/shared/model/model-version.model';

type EntityResponseType = HttpResponse<IModelVersion>;
type EntityArrayResponseType = HttpResponse<IModelVersion[]>;

@Injectable({ providedIn: 'root' })
export class ModelVersionService {
    private resourceUrl = SERVER_API_URL + 'api/model-versions';

    constructor(private http: HttpClient) {}

    create(modelVersion: IModelVersion): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(modelVersion);
        return this.http
            .post<IModelVersion>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(modelVersion: IModelVersion): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(modelVersion);
        return this.http
            .put<IModelVersion>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IModelVersion>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IModelVersion[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(modelVersion: IModelVersion): IModelVersion {
        const copy: IModelVersion = Object.assign({}, modelVersion, {
            creationDate:
                modelVersion.creationDate != null && modelVersion.creationDate.isValid()
                    ? modelVersion.creationDate.format(DATE_FORMAT)
                    : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.creationDate = res.body.creationDate != null ? moment(res.body.creationDate) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((modelVersion: IModelVersion) => {
            modelVersion.creationDate = modelVersion.creationDate != null ? moment(modelVersion.creationDate) : null;
        });
        return res;
    }
}
