import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ModelVersion } from 'app/shared/model/model-version.model';
import { ModelVersionService } from './model-version.service';
import { ModelVersionUpdatePopupComponent } from './model-version-update.component';
import { ModelVersionDeletePopupComponent } from './model-version-delete-dialog.component';
import { IModelVersion } from 'app/shared/model/model-version.model';

@Injectable({ providedIn: 'root' })
export class ModelVersionResolve implements Resolve<IModelVersion> {
    constructor(private service: ModelVersionService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((modelVersion: HttpResponse<ModelVersion>) => modelVersion.body));
        }
        return of(new ModelVersion());
    }
}

export const modelVersionRoute: Routes = [];

export const modelVersionPopupRoute: Routes = [
    {
        path: 'model-version/:id/delete',
        component: ModelVersionDeletePopupComponent,
        resolve: {
            modelVersion: ModelVersionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLab2App.modelVersion.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'model-version/:id/edit/:modelDefId',
        component: ModelVersionUpdatePopupComponent,
        resolve: {
            modelVersion: ModelVersionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLab2App.modelVersion.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'model-version/new/:modelDefId',
        component: ModelVersionUpdatePopupComponent,
        resolve: {
            modelVersion: ModelVersionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appLab2App.modelVersion.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
