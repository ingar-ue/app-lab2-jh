import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppLab2SharedModule } from 'app/shared';
import {
    ModelVersionUpdateDialogComponent,
    ModelVersionUpdatePopupComponent,
    ModelVersionDeletePopupComponent,
    ModelVersionDeleteDialogComponent,
    modelVersionRoute,
    modelVersionPopupRoute
} from './';

const ENTITY_STATES = [...modelVersionRoute, ...modelVersionPopupRoute];

@NgModule({
    imports: [AppLab2SharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ModelVersionUpdateDialogComponent,
        ModelVersionUpdatePopupComponent,
        ModelVersionDeleteDialogComponent,
        ModelVersionDeletePopupComponent
    ],
    entryComponents: [
        ModelVersionUpdateDialogComponent,
        ModelVersionUpdatePopupComponent,
        ModelVersionDeleteDialogComponent,
        ModelVersionDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLab2ModelVersionModule {}
