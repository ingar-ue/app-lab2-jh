import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService, JhiDataUtils, JhiEventManager } from 'ng-jhipster';

import { IModelVersion } from 'app/shared/model/model-version.model';
import { ModelVersionService } from './model-version.service';
import { IModelDef } from 'app/shared/model/model-def.model';
import { ModelDefService } from 'app/entities/model-def';
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'jhi-model-version-update',
    templateUrl: './model-version-update.component.html'
})
export class ModelVersionUpdateDialogComponent implements OnInit {
    modelVersion: IModelVersion;
    isSaving: boolean;

    modeldefs: IModelDef[];
    creationDateDp: any;

    constructor(
        private dataUtils: JhiDataUtils,
        private jhiAlertService: JhiAlertService,
        private modelVersionService: ModelVersionService,
        private modelDefService: ModelDefService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    ngOnInit() {
        this.isSaving = false;
        // this.modelDefService.query().subscribe(
        //     (res: HttpResponse<IModelDef[]>) => {
        //         this.modeldefs = res.body;
        //     },
        //     (res: HttpErrorResponse) => this.onError(res.message)
        // );
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.modelVersion.id !== undefined) {
            this.subscribeToSaveResponse(this.modelVersionService.update(this.modelVersion));
        } else {
            this.subscribeToSaveResponse(this.modelVersionService.create(this.modelVersion));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IModelVersion>>) {
        result.subscribe((res: HttpResponse<IModelVersion>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.eventManager.broadcast({
            name: 'modelVersionListModification',
            content: 'Edited a modelConfig'
        });
        this.activeModal.dismiss(true);
    }

    private onSaveError() {
        this.isSaving = false;
        this.activeModal.dismiss(true);
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackModelDefById(index: number, item: IModelDef) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-model-version-update-popup',
    template: ''
})
export class ModelVersionUpdatePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ modelVersion }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ModelVersionUpdateDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.modelVersion = modelVersion;
                this.activatedRoute.params.subscribe(params => {
                    this.ngbModalRef.componentInstance.modelVersion.modelDefId = params['modelDefId'];
                });
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
