import { IModelRunDef } from 'app/shared/model/model-run-def.model';
import { Component, Input, OnChanges } from '@angular/core';
import { IOptimizationPoint1 } from 'app/shared/model/optimization-point-1.model';
import { ChartService } from 'app/shared/chart/chart.service';
import * as d3 from 'D3';
import * as topojson from 'topojson';
import { HttpResponse } from '@angular/common/http';
import * as path from 'path';

@Component({
    selector: 'jhi-map',
    templateUrl: './map.component.html'
})
export class MapComponent implements OnChanges {
    chartsLoaded = false;
    @Input() modelRunDef: IModelRunDef;
    @Input() optimizationPoint1Input: IOptimizationPoint1;

    mapData;
    svg;
    width = 700;
    height = 580;
    geoPath;
    projection;
    puntos;
    lines;
    info;
    centered = null;
    map;

    constructor(private chartService: ChartService) {}

    ngOnChanges() {
        if (!this.chartsLoaded && this.modelRunDef.status === 'FINISHED') {
            this.makeMap();
            this.chartsLoaded = true;
        }
    }

    makeMap() {
        this.svg = d3
            .select('jhi-map')
            .append('svg')
            .attr('width', this.width)
            .attr('height', this.height);

        this.makeReferences();

        this.projection = d3
            .geoMercator()
            .scale(700)
            // .rotate( [71.057,0] )
            .center([-60.3616, -40.6037])
            .translate([this.width / 2, this.height / 2]);

        this.geoPath = d3.geoPath().projection(this.projection);

        this.getData();
    }

    getData() {
        this.chartService.getMapData().subscribe((res: HttpResponse<string>) => {
            this.mapData = res.body;
            this.map = this.svg.append('g');

            this.map
                .selectAll('path')
                .data(topojson.feature(this.mapData, this.mapData.objects.provincias).features)
                .enter()
                .append('path')
                .attr('fill', '#ccc')
                .attr('d', this.geoPath)
                .style('stroke', 'white')
                .style('stroke-width', '0.5 px');

            this.getPointsData();
        });
    }

    getPointsData() {
        const _this = this;
        const pointsJson = this.makePointsJson();

        this.lines = this.svg.append('g');
        this.puntos = this.svg.append('g');

        this.puntos
            .selectAll('circle')
            .data(pointsJson.features)
            .enter()
            .append('circle')
            .attr('transform', function(d) {
                return 'translate(' + _this.projection(d.geometry.coordinates) + ')';
            })
            .attr('r', 6)
            .attr('fill', function(d) {
                return d.properties.color;
            })
            .attr('cursor', 'pointer')
            .attr('d', this.geoPath)
            .on('mouseover', function(d) {
                if (d.properties.entity === 'plant') {
                    _this.handleMouseOverPlant(d.properties);
                } else {
                    _this.handleMouseOverMarket(d.properties);
                }
            })
            .on('mouseout', function(d) {
                if (d.properties.entity === 'plant') {
                    _this.handleMouseOutPlant();
                } else {
                    _this.handleMouseOutMarket();
                }
            })
            .on('click', function(d) {
                _this.handleClick(d);
            });
    }

    handleMouseOverPlant(plantProperties) {
        const features = [];
        let i = 0;
        for (const sol of this.modelRunDef.userResult.solution) {
            if (sol.plant.id === plantProperties.id && sol.value > 0) {
                features.push({
                    id: i,
                    type: 'Feature',
                    properties: {
                        sendTo: sol.market.name,
                        quantity: sol.value
                    },
                    geometry: {
                        coordinates: [[sol.plant.longitude, sol.plant.latitude], [sol.market.longitude, sol.market.latitude]],
                        type: 'LineString'
                    }
                });
                i++;
            }
        }

        const linesJson = { type: 'FeatureCollection', features: features };

        this.lines
            .selectAll('path')
            .data(linesJson.features)
            .enter()
            .append('path')
            .attr('stroke', '#000')
            .attr('d', this.geoPath)
            .style('stroke-width', '0.25 px');

        this.makePlantReferences(plantProperties, linesJson);
    }

    handleMouseOutPlant() {
        this.lines.selectAll('path').remove();
        this.svg.selectAll('#plant-references').remove();
    }

    handleMouseOverMarket(marketProperties) {
        this.makeMarketReferences(marketProperties);
    }

    handleMouseOutMarket() {
        this.svg.selectAll('#market-references').remove();
    }

    handleClick(d) {
        let x;
        let y;
        let zoomLevel;
        let radius;
        let stroke;

        if (d && this.centered !== d) {
            const centroid = this.geoPath.centroid(d);
            x = centroid[0];
            y = centroid[1];
            zoomLevel = 5;
            radius = 1.5;
            stroke = '0.02 px';
            this.centered = d;
            this.deleteReferences();
        } else {
            const centroid = this.geoPath.centroid(this.map);
            console.log(centroid[0]);
            console.log(centroid[1]);
            x = centroid[0];
            y = centroid[1];
            zoomLevel = 1;
            this.centered = null;
            radius = 6;
            stroke = '0.25 px';
            this.makeReferences();
        }

        this.map
            .transition()
            .duration(1000)
            .ease(d3.easeCubicOut)
            .attr(
                'transform',
                'translate(' + this.width / 2 + ',' + this.height / 2 + ')scale(' + zoomLevel + ')translate(' + -x + ',' + -y + ')'
            );
        this.puntos
            .transition()
            .duration(1000)
            .ease(d3.easeCubicOut)
            .attr(
                'transform',
                'translate(' + this.width / 2 + ',' + this.height / 2 + ')scale(' + zoomLevel + ')translate(' + -x + ',' + -y + ')'
            )
            .selectAll('circle')
            .attr('r', radius);
        this.lines
            .transition()
            .duration(1000)
            .ease(d3.easeCubicOut)
            .attr(
                'transform',
                'translate(' + this.width / 2 + ',' + this.height / 2 + ')scale(' + zoomLevel + ')translate(' + -x + ',' + -y + ')'
            )
            .selectAll('path')
            .attr('stroke-width', stroke);
    }

    makePointsJson(): any {
        const features = [];
        let i = 0;
        for (const market of this.optimizationPoint1Input.markets) {
            features.push({
                id: i,
                type: 'Feature',
                properties: {
                    color: '#900',
                    id: market.id,
                    entity: 'market',
                    name: market.name,
                    demand: market.demand
                },
                geometry: {
                    coordinates: [market.longitude, market.latitude],
                    type: 'Point'
                }
            });
            i++;
        }
        for (const plant of this.optimizationPoint1Input.plants) {
            features.push({
                id: i,
                type: 'Feature',
                properties: {
                    color: '#080',
                    id: plant.id,
                    entity: 'plant',
                    name: plant.name,
                    production: plant.productiveCapacity
                },
                geometry: {
                    coordinates: [plant.longitude, plant.latitude],
                    type: 'Point'
                }
            });
            i++;
        }

        return { type: 'FeatureCollection', features: features };
    }

    makeReferences() {
        const references = this.svg.append('g').attr('id', 'references');
        references
            .append('circle')
            .attr('cx', 10)
            .attr('cy', 10)
            .attr('r', 6)
            .style('fill', '#900');
        references
            .append('circle')
            .attr('cx', 10)
            .attr('cy', 30)
            .attr('r', 6)
            .style('fill', '#080');
        references
            .append('text')
            .text('Mercados')
            .attr('x', 22)
            .attr('y', 15);
        references
            .append('text')
            .text('Plantas de producción')
            .attr('x', 22)
            .attr('y', 35);
    }

    deleteReferences() {
        this.svg.selectAll('#references').remove();
    }

    makePlantReferences(plantProperties, linesJson) {
        let x = 400,
            y = 400;

        const references = this.svg.append('g').attr('id', 'plant-references');

        const rect = references
            .append('rect')
            .attr('x', x - 5)
            .attr('y', y - 20)
            .attr('rx', 6)
            .attr('ry', 6)
            .attr('width', 400)
            .attr('height', 0)
            .attr('fill', 'white');

        references
            .append('text')
            .text(plantProperties.name)
            .attr('x', x)
            .attr('y', y)
            .attr('font-weight', 'bold');
        y = y + 20;
        references
            .append('text')
            .text('Envía a:')
            .attr('x', x)
            .attr('y', y);

        x = x + 50;
        for (const feature of linesJson.features) {
            y = y + 20;
            references
                .append('text')
                .text(feature.properties.sendTo)
                .attr('x', x)
                .attr('y', y);
        }
        rect.attr('height', y + 25 - 400);
    }

    makeMarketReferences(marketProperties) {
        const x = 400,
            y = 400;

        const references = this.svg.append('g').attr('id', 'market-references');

        const rect = references
            .append('rect')
            .attr('x', x - 5)
            .attr('y', y - 20)
            .attr('rx', 6)
            .attr('ry', 6)
            .attr('width', 400)
            .attr('height', 0)
            .attr('fill', 'white');

        references
            .append('text')
            .text(marketProperties.name)
            .attr('x', x)
            .attr('y', y)
            .attr('font-weight', 'bold');
        references
            .append('text')
            .text('Demanda: ' + marketProperties.demand)
            .attr('x', x)
            .attr('y', y + 20);
        rect.attr('height', 45);
    }
}
