import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { SERVER_API_URL } from 'app/app.constants';

@Injectable({ providedIn: 'root' })
export class ChartService {
    opt2DemandFilter: any;
    private resourceUrl = SERVER_API_URL + 'api/';

    constructor(private http: HttpClient) {}

    getMapData(): Observable<HttpResponse<string>> {
        return this.http.get(this.resourceUrl + 'mapData', { observe: 'response' }).pipe(map((res: HttpResponse<string>) => res));
    }

    setFilters(filters) {
        this.opt2DemandFilter = filters;
    }

    objectToArray(obj) {
        const array = [];
        for (const key of Object.keys(obj)) {
            array.push({ product: key, demand: obj[key] });
        }
        return array;
    }

    filterByProduction = item => {
        switch (this.opt2DemandFilter.production) {
            case 'null':
                return true;
            case 'positive':
                return item.demand < 0;
            case 'negative':
                return item.demand > 0;
            case 'satisfied':
                return item.demand === 0;
        }
    };

    filterByLarge = item => {
        switch (this.opt2DemandFilter.large) {
            case 'null':
                return true;
            case 'l10':
                return item.product.slice(-2) === '10';
            case 'l12':
                return item.product.slice(-2) === '12';
            case 'l14':
                return item.product.slice(-2) === '14';
            case 'l16':
                return item.product.slice(-2) === '16';
        }
    };

    filterByType = item => {
        switch (this.opt2DemandFilter.type) {
            case 'null':
                return true;
            case 'r0':
                return item.product.indexOf('R0') !== -1;
            case 'r1':
                return item.product.indexOf('R1') !== -1;
            case 'r2':
                return item.product.indexOf('R2') !== -1;
            case 'r3':
                return item.product.indexOf('R3') !== -1;
            case 'r4':
                return item.product.indexOf('R4') !== -1;
        }
    };

    sortByKey = (a, b) => {
        return a.product < b.product ? -1 : a.product > b.product ? 1 : 0;
    };

    applyFilters(data, filters) {
        this.setFilters(filters);
        return data
            .filter(this.filterByProduction)
            .filter(this.filterByType)
            .filter(this.filterByLarge)
            .sort(this.sortByKey);
    }

    getArrayByKey(array, key) {
        const result = [];
        array.map(obj => {
            result.push(obj[key]);
        });
        return result;
    }

    setChartConfig(dataset: object, title, labelX, labelY, aspectRatio = true, showLegend = false, stack = false) {
        const chartConfig = {
            data: dataset,
            options: {
                legend: {},
                responsive: true,
                maintainAspectRatio: aspectRatio,
                scales: {
                    xAxes: [{ scaleLabel: {}, stacked: {} }],
                    yAxes: [{ scaleLabel: {}, stacked: {} }]
                },
                title: {}
            }
        };

        if (title) {
            chartConfig.options.title = {
                display: true,
                position: 'bottom',
                fontSize: 14,
                text: title
            };
        }

        if (showLegend) {
            chartConfig.options.legend = {
                display: true
            };
        } else {
            chartConfig.options.legend = false;
        }

        if (labelX) {
            chartConfig.options.scales.xAxes[0].scaleLabel = {
                display: true,
                labelString: labelX
            };
        }

        if (labelY) {
            chartConfig.options.scales.yAxes[0].scaleLabel = {
                display: true,
                labelString: labelY
            };
        }

        if (stack) {
            chartConfig.options.scales.xAxes[0].stacked = true;
            chartConfig.options.scales.yAxes[0].stacked = true;
        }

        return chartConfig;
    }
}
