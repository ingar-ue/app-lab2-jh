import { Component, Input, OnChanges } from '@angular/core';
import { OptimizationPoint1Service } from 'app/entities/optimization-point-1/optimization-point-1.service';
import { IModelRunDef } from 'app/shared/model/model-run-def.model';
import { ChartService } from 'app/shared/chart/chart.service';
import { IOptimizationPoint1, IResult, IResultSolution } from 'app/shared/model/optimization-point-1.model';

@Component({
    selector: 'jhi-optimization-point-1-charts',
    templateUrl: './optimization-point-1-charts.component.html'
})
export class OptimizationPoint1ChartsComponent implements OnChanges {
    charts: any;
    chartsLoaded = false;
    @Input() currentChart: number;
    @Input() change: number;
    @Input() showSidebar: boolean;
    @Input() chartColumnSize: number;
    @Input() modelRunDef: IModelRunDef;
    @Input() optimizationPoint1Input: IOptimizationPoint1;

    constructor(private optimizationPoint1Service: OptimizationPoint1Service, private chartService: ChartService) {
        this.chartInit();
        this.showSidebar = true;
        this.chartColumnSize = 8;
    }

    ngOnChanges() {
        if (!this.chartsLoaded && this.modelRunDef.status === 'FINISHED') {
            this.makeChart('chart1');
            this.chartsLoaded = true;
        }
    }

    chartInit() {
        this.charts = {};
        this.currentChart = 1;
        if (!this.chartsLoaded && this.modelRunDef && this.modelRunDef.status === 'FINISHED') {
            this.makeChart('chart1');
            this.chartsLoaded = true;
        }
    }

    makeChart(chart) {
        const data = this.getChartData();
        this.setData(chart, data);
    }

    getChartData(): any {
        const data: any = {};
        const result: IResult = this.modelRunDef.userResult;
        data.valuesY = [];

        const plantas: string[] = [];
        const mercados: string[] = [];
        const info: number[][] = [];

        for (const sol of result.solution) {
            if (mercados.indexOf(sol.market.name) === -1) {
                mercados.push(sol.market.name);
                info[sol.market.name] = [];
            }
            if (plantas.indexOf(sol.plant.name) === -1) {
                plantas.push(sol.plant.name);
            }
            info[sol.market.name][sol.plant.name] = sol.value;
        }

        data.valuesX = plantas;
        data.labelX = 'Plantas de producción';

        data.labelY = 'Unidades a enviar';
        for (const mercado of mercados) {
            const dataset: any = {};
            dataset.label = mercado;
            dataset.data = [];
            dataset.backgroundColor =
                'rgb(' +
                Math.floor(Math.random() * 255) +
                ',' +
                Math.floor(Math.random() * 255) +
                ',' +
                Math.floor(Math.random() * 255) +
                ')';
            for (const planta of plantas) {
                dataset.data.push(info[mercado][planta]);
            }
            data.valuesY.push(dataset);
        }

        return data;
    }

    setData(chart, data) {
        this.charts[chart] = this.chartService.setChartConfig(
            {
                labels: data.valuesX,
                datasets: data.valuesY
            },
            null,
            data.labelX,
            data.labelY,
            true,
            true,
            true
        );
    }
}
