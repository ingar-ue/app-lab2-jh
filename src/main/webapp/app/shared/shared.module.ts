import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';

import { NgbDateMomentAdapter } from './util/datepicker-adapter';
import { AppLab2SharedLibsModule, AppLab2SharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';
import { StringFilterPipe } from 'app/shared/pipes/string-filter.pipe';
import { OptimizationPoint1ChartsComponent } from 'app/shared/chart/optimization-point-1-charts.component';
import { ChartModule } from 'angular2-chartjs';
import { SortPipe } from 'app/shared/pipes/sort.pipe';
import { CustomFilterPipe } from 'app/shared/pipes/custom-filter.pipe';
import { MapComponent } from 'app/shared/chart/map.component';

@NgModule({
    imports: [AppLab2SharedLibsModule, AppLab2SharedCommonModule, ChartModule],
    declarations: [
        JhiLoginModalComponent,
        HasAnyAuthorityDirective,
        StringFilterPipe,
        OptimizationPoint1ChartsComponent,
        MapComponent,
        SortPipe,
        CustomFilterPipe
    ],
    providers: [{ provide: NgbDateAdapter, useClass: NgbDateMomentAdapter }, MapComponent, OptimizationPoint1ChartsComponent],
    entryComponents: [JhiLoginModalComponent, MapComponent, OptimizationPoint1ChartsComponent],
    exports: [
        AppLab2SharedCommonModule,
        JhiLoginModalComponent,
        HasAnyAuthorityDirective,
        StringFilterPipe,
        OptimizationPoint1ChartsComponent,
        MapComponent,
        SortPipe,
        CustomFilterPipe
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppLab2SharedModule {}
