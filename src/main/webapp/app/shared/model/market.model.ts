export interface IMarket {
    id?: number;
    name?: string;
    demand?: number;
    address?: string;
    latitude?: number;
    longitude?: number;
}

export class Market implements IMarket {
    constructor(
        public id?: number,
        public name?: string,
        public demand?: number,
        public address?: string,
        public latitude?: number,
        public longitude?: number
    ) {}
}
