import { IModelRunDef, ModelRunDef } from 'app/shared/model/model-run-def.model';
import { IMarket } from 'app/shared/model/market.model';
import { IPlant } from 'app/shared/model/plant.model';
import { IDistance } from 'app/shared/model/distance.model';
import { IModelRunConfig } from 'app/shared/model/model-run-config.model';

export interface IOptimizationPoint1 {
    markets?: IMarket[];
    plants?: IPlant[];
    distances?: IDistance[];
    modelRunDef?: IModelRunDef;
    modelDefId?: number;
    modelRunConfigs?: IModelRunConfig[];
}

export class OptimizationPoint1 implements IOptimizationPoint1 {
    constructor(
        public markets?: IMarket[],
        public plants?: IPlant[],
        public distances?: IDistance[],
        public modelRunDef?: ModelRunDef,
        public modelDefId?: number,
        public modelRunConfigs?: IModelRunConfig[]
    ) {}
}

export interface IPyomoInput {
    markets?: IPyomoInputMarket[];
    plants?: IPyomoInputPlant[];
    distances?: IPyomoInputDistance[];
    freight?: number;
}

export interface IPyomoInputMarket {
    name?: string;
    demand?: number;
}
export interface IPyomoInputPlant {
    name?: string;
    productiveCapacity?: number;
}
export interface IPyomoInputDistance {
    marketName?: string;
    plantName?: string;
    distance?: number;
}

export interface IResult {
    solution?: IResultSolution[];
    cost?: number;
}

export interface IResultSolution {
    market?: IMarket;
    plant?: IPlant;
    value?: number;
}

export class ResultSolution implements IResultSolution {
    constructor(public market?: IMarket, public plant?: IPlant, public value?: number) {}

    setMarket(marketName: string, markets: IMarket[]) {
        for (const market of markets) {
            if (market.name.toLowerCase() === marketName) {
                this.market = market;
                break;
            }
        }
    }

    setPlant(plantName: string, plants: IPlant[]) {
        for (const plant of plants) {
            if (plant.name.toLowerCase() === plantName) {
                this.plant = plant;
                break;
            }
        }
    }
}
