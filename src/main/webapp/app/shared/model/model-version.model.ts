import { Moment } from 'moment';

export interface IModelVersion {
    id?: number;
    version?: number;
    creationDate?: Moment;
    srcPath?: string;
    srcContent?: any;
    description?: string;
    modelDefId?: number;
}

export class ModelVersion implements IModelVersion {
    constructor(
        public id?: number,
        public version?: number,
        public creationDate?: Moment,
        public srcPath?: string,
        public srcContent?: any,
        public description?: string,
        public modelDefId?: number
    ) {}
}
