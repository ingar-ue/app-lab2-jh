export interface IDistance {
    id?: number;
    distance?: number;
    plantId?: number;
    marketId?: number;
    plantName?: string;
    marketName?: string;
}

export class Distance implements IDistance {
    constructor(
        public id?: number,
        public distance?: number,
        public plantId?: number,
        public marketId?: number,
        public plantName?: string,
        public marketName?: string
    ) {}
}
