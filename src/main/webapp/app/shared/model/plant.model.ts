export interface IPlant {
    id?: number;
    name?: string;
    productiveCapacity?: number;
    address?: string;
    latitude?: number;
    longitude?: number;
}

export class Plant implements IPlant {
    constructor(
        public id?: number,
        public name?: string,
        public productiveCapacity?: number,
        public address?: string,
        public latitude?: number,
        public longitude?: number
    ) {}
}
