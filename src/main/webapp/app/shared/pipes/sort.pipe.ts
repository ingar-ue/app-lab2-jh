import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'sort'
})
@Injectable()
export class SortPipe implements PipeTransform {
    transform(items: any[], field: string, desc: boolean) {
        if (items && items.length && field) {
            return items.sort((a, b) => {
                const aLC: string = String(a[field]).toLowerCase();
                const bLC: string = String(b[field]).toLowerCase();
                if (aLC === bLC) {
                    return a.id < b.id ? -1 : a.id > b.id ? 1 : 0;
                } else {
                    if (desc) {
                        return aLC < bLC ? -1 : aLC > bLC ? 1 : 0;
                    } else {
                        return aLC < bLC ? 1 : aLC > bLC ? -1 : 0;
                    }
                }
            });
        } else {
            return items;
        }
    }
}
