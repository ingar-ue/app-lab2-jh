/* after changing this file run 'npm run webpack:build' */
/* tslint:disable */
import '../content/css/vendor.css';

// Imports all fontawesome core and solid icons

import { library } from '@fortawesome/fontawesome-svg-core';
import {
    faUser,
    faSort,
    faSortUp,
    faSortDown,
    faSync,
    faEye,
    faBan,
    faTimes,
    faArrowLeft,
    faArrowCircleRight,
    faSave,
    faPlus,
    faPencilAlt,
    faBars,
    faThList,
    faUserPlus,
    faRoad,
    faTachometerAlt,
    faHeart,
    faList,
    faBell,
    faBook,
    faHdd,
    faFlag,
    faWrench,
    faClock,
    faCloud,
    faSignOutAlt,
    faSignInAlt,
    faCalendarAlt,
    faSearch,
    faTrashAlt,
    faAsterisk,
    faTasks,
    faTree,
    faSignal,
    faBullseye,
    faThLarge,
    faTh,
    faListOl,
    faBolt,
    faObjectGroup,
    faDollarSign,
    faSpinner,
    faHome,
    faCheckCircle,
    faDna,
    faDownload,
    faInfoCircle,
    faExclamationCircle,
    faExclamationTriangle,
    faWindowClose,
    faCheckSquare,
    faArrowsAltH,
    faDotCircle,
    faTags,
    faTag
} from '@fortawesome/free-solid-svg-icons';

// Adds the SVG icon to the library so you can use it in your page
library.add(faUser);
library.add(faSort);
library.add(faSortUp);
library.add(faSortDown);
library.add(faSync);
library.add(faEye);
library.add(faBan);
library.add(faTimes);
library.add(faArrowLeft);
library.add(faArrowCircleRight);
library.add(faSave);
library.add(faPlus);
library.add(faPencilAlt);
library.add(faBars);
library.add(faHome);
library.add(faThList);
library.add(faUserPlus);
library.add(faRoad);
library.add(faTachometerAlt);
library.add(faHeart);
library.add(faList);
library.add(faBell);
library.add(faTasks);
library.add(faBook);
library.add(faHdd);
library.add(faFlag);
library.add(faWrench);
library.add(faClock);
library.add(faCloud);
library.add(faSignOutAlt);
library.add(faSignInAlt);
library.add(faCalendarAlt);
library.add(faSearch);
library.add(faTrashAlt);
library.add(faAsterisk);
library.add(faTree);
library.add(faSignal);
library.add(faBullseye);
library.add(faThLarge);
library.add(faTh);
library.add(faListOl);
library.add(faBolt);
library.add(faObjectGroup);
library.add(faDollarSign);
library.add(faSpinner);
library.add(faCheckCircle);
library.add(faDna);
library.add(faDownload);
library.add(faInfoCircle);
library.add(faExclamationCircle);
library.add(faExclamationTriangle);
library.add(faWindowClose);
library.add(faCheckSquare);
library.add(faArrowsAltH);
library.add(faDotCircle);
library.add(faTags);
library.add(faTag);

// jhipster-needle-add-element-to-vendor - JHipster will add new menu items here
