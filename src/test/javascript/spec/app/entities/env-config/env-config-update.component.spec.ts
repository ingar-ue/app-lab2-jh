/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { AppLab2TestModule } from '../../../test.module';
import { EnvConfigUpdateComponent } from 'app/entities/env-config/env-config-update.component';
import { EnvConfigService } from 'app/entities/env-config/env-config.service';
import { EnvConfig } from 'app/shared/model/env-config.model';

describe('Component Tests', () => {
    describe('EnvConfig Management Update Component', () => {
        let comp: EnvConfigUpdateComponent;
        let fixture: ComponentFixture<EnvConfigUpdateComponent>;
        let service: EnvConfigService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLab2TestModule],
                declarations: [EnvConfigUpdateComponent]
            })
                .overrideTemplate(EnvConfigUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(EnvConfigUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EnvConfigService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new EnvConfig(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.envConfig = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new EnvConfig();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.envConfig = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
