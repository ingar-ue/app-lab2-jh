/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { AppLab2TestModule } from '../../../test.module';
import { EnvConfigDeleteDialogComponent } from 'app/entities/env-config/env-config-delete-dialog.component';
import { EnvConfigService } from 'app/entities/env-config/env-config.service';

describe('Component Tests', () => {
    describe('EnvConfig Management Delete Component', () => {
        let comp: EnvConfigDeleteDialogComponent;
        let fixture: ComponentFixture<EnvConfigDeleteDialogComponent>;
        let service: EnvConfigService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLab2TestModule],
                declarations: [EnvConfigDeleteDialogComponent]
            })
                .overrideTemplate(EnvConfigDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(EnvConfigDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EnvConfigService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
