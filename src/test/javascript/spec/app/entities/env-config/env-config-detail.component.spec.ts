/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppLab2TestModule } from '../../../test.module';
import { EnvConfigDetailComponent } from 'app/entities/env-config/env-config-detail.component';
import { EnvConfig } from 'app/shared/model/env-config.model';

describe('Component Tests', () => {
    describe('EnvConfig Management Detail Component', () => {
        let comp: EnvConfigDetailComponent;
        let fixture: ComponentFixture<EnvConfigDetailComponent>;
        const route = ({ data: of({ envConfig: new EnvConfig(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLab2TestModule],
                declarations: [EnvConfigDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(EnvConfigDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(EnvConfigDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.envConfig).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
