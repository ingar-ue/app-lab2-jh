/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { AppLab2TestModule } from '../../../test.module';
import { ModelRunDefDeleteDialogComponent } from 'app/entities/model-run-def/model-run-def-delete-dialog.component';
import { ModelRunDefService } from 'app/entities/model-run-def/model-run-def.service';

describe('Component Tests', () => {
    describe('ModelRunDef Management Delete Component', () => {
        let comp: ModelRunDefDeleteDialogComponent;
        let fixture: ComponentFixture<ModelRunDefDeleteDialogComponent>;
        let service: ModelRunDefService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLab2TestModule],
                declarations: [ModelRunDefDeleteDialogComponent]
            })
                .overrideTemplate(ModelRunDefDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ModelRunDefDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ModelRunDefService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
