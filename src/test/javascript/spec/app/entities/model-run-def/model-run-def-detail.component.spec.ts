/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppLab2TestModule } from '../../../test.module';
import { ModelRunDefDetailComponent } from 'app/entities/model-run-def/model-run-def-detail.component';
import { ModelRunDef } from 'app/shared/model/model-run-def.model';

describe('Component Tests', () => {
    describe('ModelRunDef Management Detail Component', () => {
        let comp: ModelRunDefDetailComponent;
        let fixture: ComponentFixture<ModelRunDefDetailComponent>;
        const route = ({ data: of({ modelRunDef: new ModelRunDef(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLab2TestModule],
                declarations: [ModelRunDefDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ModelRunDefDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ModelRunDefDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.modelRunDef).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
