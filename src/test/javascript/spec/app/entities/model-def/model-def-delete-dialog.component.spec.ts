/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { AppLab2TestModule } from '../../../test.module';
import { ModelDefDeleteDialogComponent } from 'app/entities/model-def/model-def-delete-dialog.component';
import { ModelDefService } from 'app/entities/model-def/model-def.service';

describe('Component Tests', () => {
    describe('ModelDef Management Delete Component', () => {
        let comp: ModelDefDeleteDialogComponent;
        let fixture: ComponentFixture<ModelDefDeleteDialogComponent>;
        let service: ModelDefService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLab2TestModule],
                declarations: [ModelDefDeleteDialogComponent]
            })
                .overrideTemplate(ModelDefDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ModelDefDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ModelDefService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
