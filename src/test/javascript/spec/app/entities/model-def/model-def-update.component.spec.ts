/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { AppLab2TestModule } from '../../../test.module';
import { ModelDefUpdateDialogComponent } from 'app/entities/model-def/model-def-update.component';
import { ModelDefService } from 'app/entities/model-def/model-def.service';
import { ModelDef } from 'app/shared/model/model-def.model';

describe('Component Tests', () => {
    describe('ModelDef Management Update Component', () => {
        let comp: ModelDefUpdateDialogComponent;
        let fixture: ComponentFixture<ModelDefUpdateDialogComponent>;
        let service: ModelDefService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLab2TestModule],
                declarations: [ModelDefUpdateDialogComponent]
            })
                .overrideTemplate(ModelDefUpdateDialogComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ModelDefUpdateDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ModelDefService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ModelDef(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.modelDef = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ModelDef();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.modelDef = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
