/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppLab2TestModule } from '../../../test.module';
import { ModelDefDetailComponent } from 'app/entities/model-def/model-def-detail.component';
import { ModelDef } from 'app/shared/model/model-def.model';

describe('Component Tests', () => {
    describe('ModelDef Management Detail Component', () => {
        let comp: ModelDefDetailComponent;
        let fixture: ComponentFixture<ModelDefDetailComponent>;
        const route = ({ data: of({ modelDef: new ModelDef(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLab2TestModule],
                declarations: [ModelDefDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ModelDefDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ModelDefDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.modelDef).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
