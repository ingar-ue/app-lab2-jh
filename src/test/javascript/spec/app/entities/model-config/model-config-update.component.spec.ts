/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { AppLab2TestModule } from '../../../test.module';
import { ModelConfigUpdateDialogComponent } from 'app/entities/model-config/model-config-update.component';
import { ModelConfigService } from 'app/entities/model-config/model-config.service';
import { ModelConfig } from 'app/shared/model/model-config.model';

describe('Component Tests', () => {
    describe('ModelConfig Management Update Component', () => {
        let comp: ModelConfigUpdateDialogComponent;
        let fixture: ComponentFixture<ModelConfigUpdateDialogComponent>;
        let service: ModelConfigService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLab2TestModule],
                declarations: [ModelConfigUpdateDialogComponent]
            })
                .overrideTemplate(ModelConfigUpdateDialogComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ModelConfigUpdateDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ModelConfigService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ModelConfig(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.modelConfig = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ModelConfig();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.modelConfig = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
