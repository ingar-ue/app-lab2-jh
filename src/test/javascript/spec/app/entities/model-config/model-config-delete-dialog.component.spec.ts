/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { AppLab2TestModule } from '../../../test.module';
import { ModelConfigDeleteDialogComponent } from 'app/entities/model-config/model-config-delete-dialog.component';
import { ModelConfigService } from 'app/entities/model-config/model-config.service';

describe('Component Tests', () => {
    describe('ModelConfig Management Delete Component', () => {
        let comp: ModelConfigDeleteDialogComponent;
        let fixture: ComponentFixture<ModelConfigDeleteDialogComponent>;
        let service: ModelConfigService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLab2TestModule],
                declarations: [ModelConfigDeleteDialogComponent]
            })
                .overrideTemplate(ModelConfigDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ModelConfigDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ModelConfigService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
