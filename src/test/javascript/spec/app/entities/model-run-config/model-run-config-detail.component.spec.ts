/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AppLab2TestModule } from '../../../test.module';
import { ModelRunConfigDetailComponent } from 'app/entities/model-run-config/model-run-config-detail.component';
import { ModelRunConfig } from 'app/shared/model/model-run-config.model';

describe('Component Tests', () => {
    describe('ModelRunConfig Management Detail Component', () => {
        let comp: ModelRunConfigDetailComponent;
        let fixture: ComponentFixture<ModelRunConfigDetailComponent>;
        const route = ({ data: of({ modelRunConfig: new ModelRunConfig(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLab2TestModule],
                declarations: [ModelRunConfigDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ModelRunConfigDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ModelRunConfigDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.modelRunConfig).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
