/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { AppLab2TestModule } from '../../../test.module';
import { ModelRunConfigUpdateComponent } from 'app/entities/model-run-config/model-run-config-update.component';
import { ModelRunConfigService } from 'app/entities/model-run-config/model-run-config.service';
import { ModelRunConfig } from 'app/shared/model/model-run-config.model';

describe('Component Tests', () => {
    describe('ModelRunConfig Management Update Component', () => {
        let comp: ModelRunConfigUpdateComponent;
        let fixture: ComponentFixture<ModelRunConfigUpdateComponent>;
        let service: ModelRunConfigService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLab2TestModule],
                declarations: [ModelRunConfigUpdateComponent]
            })
                .overrideTemplate(ModelRunConfigUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ModelRunConfigUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ModelRunConfigService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ModelRunConfig(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.modelRunConfig = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ModelRunConfig();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.modelRunConfig = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
