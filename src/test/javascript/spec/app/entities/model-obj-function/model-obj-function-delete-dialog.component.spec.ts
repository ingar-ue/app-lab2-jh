/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { AppLab2TestModule } from '../../../test.module';
import { ModelObjFunctionDeleteDialogComponent } from 'app/entities/model-obj-function/model-obj-function-delete-dialog.component';
import { ModelObjFunctionService } from 'app/entities/model-obj-function/model-obj-function.service';

describe('Component Tests', () => {
    describe('ModelObjFunction Management Delete Component', () => {
        let comp: ModelObjFunctionDeleteDialogComponent;
        let fixture: ComponentFixture<ModelObjFunctionDeleteDialogComponent>;
        let service: ModelObjFunctionService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLab2TestModule],
                declarations: [ModelObjFunctionDeleteDialogComponent]
            })
                .overrideTemplate(ModelObjFunctionDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ModelObjFunctionDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ModelObjFunctionService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
