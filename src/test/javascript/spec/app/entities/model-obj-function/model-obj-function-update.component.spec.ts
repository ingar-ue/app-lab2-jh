/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { AppLab2TestModule } from '../../../test.module';
import { ModelObjFunctionUpdateDialogComponent } from 'app/entities/model-obj-function/model-obj-function-update.component';
import { ModelObjFunctionService } from 'app/entities/model-obj-function/model-obj-function.service';
import { ModelObjFunction } from 'app/shared/model/model-obj-function.model';

describe('Component Tests', () => {
    describe('ModelObjFunction Management Update Component', () => {
        let comp: ModelObjFunctionUpdateDialogComponent;
        let fixture: ComponentFixture<ModelObjFunctionUpdateDialogComponent>;
        let service: ModelObjFunctionService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [AppLab2TestModule],
                declarations: [ModelObjFunctionUpdateDialogComponent]
            })
                .overrideTemplate(ModelObjFunctionUpdateDialogComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ModelObjFunctionUpdateDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ModelObjFunctionService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ModelObjFunction(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.modelObjFunction = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ModelObjFunction();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.modelObjFunction = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
