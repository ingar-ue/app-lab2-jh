package gov.ingar.applab.web.rest;

import gov.ingar.applab.AppLab2App;

import gov.ingar.applab.domain.ModelRunDef;
import gov.ingar.applab.repository.ModelRunDefRepository;
import gov.ingar.applab.service.ModelRunDefService;
import gov.ingar.applab.service.dto.ModelRunDefDTO;
import gov.ingar.applab.service.mapper.ModelRunDefMapper;
import gov.ingar.applab.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static gov.ingar.applab.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import gov.ingar.applab.domain.enumeration.Language;
import gov.ingar.applab.domain.enumeration.Solver;
import gov.ingar.applab.domain.enumeration.Status;
/**
 * Test class for the ModelRunDefResource REST controller.
 *
 * @see ModelRunDefResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppLab2App.class)
public class ModelRunDefResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Language DEFAULT_LANGUAGE = Language.GAMS;
    private static final Language UPDATED_LANGUAGE = Language.PYOMO;

    private static final Solver DEFAULT_SOLVER = Solver.CPLEX;
    private static final Solver UPDATED_SOLVER = Solver.GUROBI;

    private static final Integer DEFAULT_VERSION = 1;
    private static final Integer UPDATED_VERSION = 2;

    private static final String DEFAULT_OBJ_FUNCTION = "AAAAAAAAAA";
    private static final String UPDATED_OBJ_FUNCTION = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_RUN_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_RUN_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_START_TIME = "AAAAAAAAAA";
    private static final String UPDATED_START_TIME = "BBBBBBBBBB";

    private static final String DEFAULT_END_TIME = "AAAAAAAAAA";
    private static final String UPDATED_END_TIME = "BBBBBBBBBB";

    private static final Status DEFAULT_STATUS = Status.READY;
    private static final Status UPDATED_STATUS = Status.RUNNING;

    private static final Integer DEFAULT_PROGRESS = 0;
    private static final Integer UPDATED_PROGRESS = 1;

    private static final String DEFAULT_DATA_SET_INPUT = "AAAAAAAAAA";
    private static final String UPDATED_DATA_SET_INPUT = "BBBBBBBBBB";

    private static final String DEFAULT_DATA_SET_RESULT = "AAAAAAAAAA";
    private static final String UPDATED_DATA_SET_RESULT = "BBBBBBBBBB";

    private static final byte[] DEFAULT_USER_RESULT = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_USER_RESULT = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_USER_RESULT_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_USER_RESULT_CONTENT_TYPE = "image/png";

    private static final byte[] DEFAULT_OTHER_RESULT = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_OTHER_RESULT = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_OTHER_RESULT_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_OTHER_RESULT_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_COMMENTS = "AAAAAAAAAA";
    private static final String UPDATED_COMMENTS = "BBBBBBBBBB";

    private static final String DEFAULT_USER = "AAAAAAAAAA";
    private static final String UPDATED_USER = "BBBBBBBBBB";

    @Autowired
    private ModelRunDefRepository modelRunDefRepository;

    @Autowired
    private ModelRunDefMapper modelRunDefMapper;

    @Autowired
    private ModelRunDefService modelRunDefService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restModelRunDefMockMvc;

    private ModelRunDef modelRunDef;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ModelRunDefResource modelRunDefResource = new ModelRunDefResource(modelRunDefService);
        this.restModelRunDefMockMvc = MockMvcBuilders.standaloneSetup(modelRunDefResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ModelRunDef createEntity(EntityManager em) {
        ModelRunDef modelRunDef = new ModelRunDef()
            .name(DEFAULT_NAME)
            .language(DEFAULT_LANGUAGE)
            .solver(DEFAULT_SOLVER)
            .version(DEFAULT_VERSION)
            .objFunction(DEFAULT_OBJ_FUNCTION)
            .runDate(DEFAULT_RUN_DATE)
            .startTime(DEFAULT_START_TIME)
            .endTime(DEFAULT_END_TIME)
            .status(DEFAULT_STATUS)
            .progress(DEFAULT_PROGRESS)
            .dataSetInput(DEFAULT_DATA_SET_INPUT)
            .dataSetResult(DEFAULT_DATA_SET_RESULT)
            .userResult(DEFAULT_USER_RESULT)
            .userResultContentType(DEFAULT_USER_RESULT_CONTENT_TYPE)
            .otherResult(DEFAULT_OTHER_RESULT)
            .otherResultContentType(DEFAULT_OTHER_RESULT_CONTENT_TYPE)
            .comments(DEFAULT_COMMENTS)
            .user(DEFAULT_USER);
        return modelRunDef;
    }

    @Before
    public void initTest() {
        modelRunDef = createEntity(em);
    }

    @Test
    @Transactional
    public void createModelRunDef() throws Exception {
        int databaseSizeBeforeCreate = modelRunDefRepository.findAll().size();

        // Create the ModelRunDef
        ModelRunDefDTO modelRunDefDTO = modelRunDefMapper.toDto(modelRunDef);
        restModelRunDefMockMvc.perform(post("/api/model-run-defs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelRunDefDTO)))
            .andExpect(status().isCreated());

        // Validate the ModelRunDef in the database
        List<ModelRunDef> modelRunDefList = modelRunDefRepository.findAll();
        assertThat(modelRunDefList).hasSize(databaseSizeBeforeCreate + 1);
        ModelRunDef testModelRunDef = modelRunDefList.get(modelRunDefList.size() - 1);
        assertThat(testModelRunDef.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testModelRunDef.getLanguage()).isEqualTo(DEFAULT_LANGUAGE);
        assertThat(testModelRunDef.getSolver()).isEqualTo(DEFAULT_SOLVER);
        assertThat(testModelRunDef.getVersion()).isEqualTo(DEFAULT_VERSION);
        assertThat(testModelRunDef.getObjFunction()).isEqualTo(DEFAULT_OBJ_FUNCTION);
        assertThat(testModelRunDef.getRunDate()).isEqualTo(DEFAULT_RUN_DATE);
        assertThat(testModelRunDef.getStartTime()).isEqualTo(DEFAULT_START_TIME);
        assertThat(testModelRunDef.getEndTime()).isEqualTo(DEFAULT_END_TIME);
        assertThat(testModelRunDef.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testModelRunDef.getProgress()).isEqualTo(DEFAULT_PROGRESS);
        assertThat(testModelRunDef.getDataSetInput()).isEqualTo(DEFAULT_DATA_SET_INPUT);
        assertThat(testModelRunDef.getDataSetResult()).isEqualTo(DEFAULT_DATA_SET_RESULT);
        assertThat(testModelRunDef.getUserResult()).isEqualTo(DEFAULT_USER_RESULT);
        assertThat(testModelRunDef.getUserResultContentType()).isEqualTo(DEFAULT_USER_RESULT_CONTENT_TYPE);
        assertThat(testModelRunDef.getOtherResult()).isEqualTo(DEFAULT_OTHER_RESULT);
        assertThat(testModelRunDef.getOtherResultContentType()).isEqualTo(DEFAULT_OTHER_RESULT_CONTENT_TYPE);
        assertThat(testModelRunDef.getComments()).isEqualTo(DEFAULT_COMMENTS);
        assertThat(testModelRunDef.getUser()).isEqualTo(DEFAULT_USER);
    }

    @Test
    @Transactional
    public void createModelRunDefWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = modelRunDefRepository.findAll().size();

        // Create the ModelRunDef with an existing ID
        modelRunDef.setId(1L);
        ModelRunDefDTO modelRunDefDTO = modelRunDefMapper.toDto(modelRunDef);

        // An entity with an existing ID cannot be created, so this API call must fail
        restModelRunDefMockMvc.perform(post("/api/model-run-defs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelRunDefDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ModelRunDef in the database
        List<ModelRunDef> modelRunDefList = modelRunDefRepository.findAll();
        assertThat(modelRunDefList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = modelRunDefRepository.findAll().size();
        // set the field null
        modelRunDef.setName(null);

        // Create the ModelRunDef, which fails.
        ModelRunDefDTO modelRunDefDTO = modelRunDefMapper.toDto(modelRunDef);

        restModelRunDefMockMvc.perform(post("/api/model-run-defs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelRunDefDTO)))
            .andExpect(status().isBadRequest());

        List<ModelRunDef> modelRunDefList = modelRunDefRepository.findAll();
        assertThat(modelRunDefList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLanguageIsRequired() throws Exception {
        int databaseSizeBeforeTest = modelRunDefRepository.findAll().size();
        // set the field null
        modelRunDef.setLanguage(null);

        // Create the ModelRunDef, which fails.
        ModelRunDefDTO modelRunDefDTO = modelRunDefMapper.toDto(modelRunDef);

        restModelRunDefMockMvc.perform(post("/api/model-run-defs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelRunDefDTO)))
            .andExpect(status().isBadRequest());

        List<ModelRunDef> modelRunDefList = modelRunDefRepository.findAll();
        assertThat(modelRunDefList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSolverIsRequired() throws Exception {
        int databaseSizeBeforeTest = modelRunDefRepository.findAll().size();
        // set the field null
        modelRunDef.setSolver(null);

        // Create the ModelRunDef, which fails.
        ModelRunDefDTO modelRunDefDTO = modelRunDefMapper.toDto(modelRunDef);

        restModelRunDefMockMvc.perform(post("/api/model-run-defs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelRunDefDTO)))
            .andExpect(status().isBadRequest());

        List<ModelRunDef> modelRunDefList = modelRunDefRepository.findAll();
        assertThat(modelRunDefList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkVersionIsRequired() throws Exception {
        int databaseSizeBeforeTest = modelRunDefRepository.findAll().size();
        // set the field null
        modelRunDef.setVersion(null);

        // Create the ModelRunDef, which fails.
        ModelRunDefDTO modelRunDefDTO = modelRunDefMapper.toDto(modelRunDef);

        restModelRunDefMockMvc.perform(post("/api/model-run-defs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelRunDefDTO)))
            .andExpect(status().isBadRequest());

        List<ModelRunDef> modelRunDefList = modelRunDefRepository.findAll();
        assertThat(modelRunDefList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkObjFunctionIsRequired() throws Exception {
        int databaseSizeBeforeTest = modelRunDefRepository.findAll().size();
        // set the field null
        modelRunDef.setObjFunction(null);

        // Create the ModelRunDef, which fails.
        ModelRunDefDTO modelRunDefDTO = modelRunDefMapper.toDto(modelRunDef);

        restModelRunDefMockMvc.perform(post("/api/model-run-defs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelRunDefDTO)))
            .andExpect(status().isBadRequest());

        List<ModelRunDef> modelRunDefList = modelRunDefRepository.findAll();
        assertThat(modelRunDefList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllModelRunDefs() throws Exception {
        // Initialize the database
        modelRunDefRepository.saveAndFlush(modelRunDef);

        // Get all the modelRunDefList
        restModelRunDefMockMvc.perform(get("/api/model-run-defs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(modelRunDef.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].language").value(hasItem(DEFAULT_LANGUAGE.toString())))
            .andExpect(jsonPath("$.[*].solver").value(hasItem(DEFAULT_SOLVER.toString())))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION)))
            .andExpect(jsonPath("$.[*].objFunction").value(hasItem(DEFAULT_OBJ_FUNCTION.toString())))
            .andExpect(jsonPath("$.[*].runDate").value(hasItem(DEFAULT_RUN_DATE.toString())))
            .andExpect(jsonPath("$.[*].startTime").value(hasItem(DEFAULT_START_TIME.toString())))
            .andExpect(jsonPath("$.[*].endTime").value(hasItem(DEFAULT_END_TIME.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].progress").value(hasItem(DEFAULT_PROGRESS)))
            .andExpect(jsonPath("$.[*].dataSetInput").value(hasItem(DEFAULT_DATA_SET_INPUT.toString())))
            .andExpect(jsonPath("$.[*].dataSetResult").value(hasItem(DEFAULT_DATA_SET_RESULT.toString())))
            .andExpect(jsonPath("$.[*].userResultContentType").value(hasItem(DEFAULT_USER_RESULT_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].userResult").value(hasItem(Base64Utils.encodeToString(DEFAULT_USER_RESULT))))
            .andExpect(jsonPath("$.[*].otherResultContentType").value(hasItem(DEFAULT_OTHER_RESULT_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].otherResult").value(hasItem(Base64Utils.encodeToString(DEFAULT_OTHER_RESULT))))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS.toString())))
            .andExpect(jsonPath("$.[*].user").value(hasItem(DEFAULT_USER.toString())));
    }

    @Test
    @Transactional
    public void getModelRunDef() throws Exception {
        // Initialize the database
        modelRunDefRepository.saveAndFlush(modelRunDef);

        // Get the modelRunDef
        restModelRunDefMockMvc.perform(get("/api/model-run-defs/{id}", modelRunDef.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(modelRunDef.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.language").value(DEFAULT_LANGUAGE.toString()))
            .andExpect(jsonPath("$.solver").value(DEFAULT_SOLVER.toString()))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION))
            .andExpect(jsonPath("$.objFunction").value(DEFAULT_OBJ_FUNCTION.toString()))
            .andExpect(jsonPath("$.runDate").value(DEFAULT_RUN_DATE.toString()))
            .andExpect(jsonPath("$.startTime").value(DEFAULT_START_TIME.toString()))
            .andExpect(jsonPath("$.endTime").value(DEFAULT_END_TIME.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.progress").value(DEFAULT_PROGRESS))
            .andExpect(jsonPath("$.dataSetInput").value(DEFAULT_DATA_SET_INPUT.toString()))
            .andExpect(jsonPath("$.dataSetResult").value(DEFAULT_DATA_SET_RESULT.toString()))
            .andExpect(jsonPath("$.userResultContentType").value(DEFAULT_USER_RESULT_CONTENT_TYPE))
            .andExpect(jsonPath("$.userResult").value(Base64Utils.encodeToString(DEFAULT_USER_RESULT)))
            .andExpect(jsonPath("$.otherResultContentType").value(DEFAULT_OTHER_RESULT_CONTENT_TYPE))
            .andExpect(jsonPath("$.otherResult").value(Base64Utils.encodeToString(DEFAULT_OTHER_RESULT)))
            .andExpect(jsonPath("$.comments").value(DEFAULT_COMMENTS.toString()))
            .andExpect(jsonPath("$.user").value(DEFAULT_USER.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingModelRunDef() throws Exception {
        // Get the modelRunDef
        restModelRunDefMockMvc.perform(get("/api/model-run-defs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateModelRunDef() throws Exception {
        // Initialize the database
        modelRunDefRepository.saveAndFlush(modelRunDef);

        int databaseSizeBeforeUpdate = modelRunDefRepository.findAll().size();

        // Update the modelRunDef
        ModelRunDef updatedModelRunDef = modelRunDefRepository.findById(modelRunDef.getId()).get();
        // Disconnect from session so that the updates on updatedModelRunDef are not directly saved in db
        em.detach(updatedModelRunDef);
        updatedModelRunDef
            .name(UPDATED_NAME)
            .language(UPDATED_LANGUAGE)
            .solver(UPDATED_SOLVER)
            .version(UPDATED_VERSION)
            .objFunction(UPDATED_OBJ_FUNCTION)
            .runDate(UPDATED_RUN_DATE)
            .startTime(UPDATED_START_TIME)
            .endTime(UPDATED_END_TIME)
            .status(UPDATED_STATUS)
            .progress(UPDATED_PROGRESS)
            .dataSetInput(UPDATED_DATA_SET_INPUT)
            .dataSetResult(UPDATED_DATA_SET_RESULT)
            .userResult(UPDATED_USER_RESULT)
            .userResultContentType(UPDATED_USER_RESULT_CONTENT_TYPE)
            .otherResult(UPDATED_OTHER_RESULT)
            .otherResultContentType(UPDATED_OTHER_RESULT_CONTENT_TYPE)
            .comments(UPDATED_COMMENTS)
            .user(UPDATED_USER);
        ModelRunDefDTO modelRunDefDTO = modelRunDefMapper.toDto(updatedModelRunDef);

        restModelRunDefMockMvc.perform(put("/api/model-run-defs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelRunDefDTO)))
            .andExpect(status().isOk());

        // Validate the ModelRunDef in the database
        List<ModelRunDef> modelRunDefList = modelRunDefRepository.findAll();
        assertThat(modelRunDefList).hasSize(databaseSizeBeforeUpdate);
        ModelRunDef testModelRunDef = modelRunDefList.get(modelRunDefList.size() - 1);
        assertThat(testModelRunDef.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testModelRunDef.getLanguage()).isEqualTo(UPDATED_LANGUAGE);
        assertThat(testModelRunDef.getSolver()).isEqualTo(UPDATED_SOLVER);
        assertThat(testModelRunDef.getVersion()).isEqualTo(UPDATED_VERSION);
        assertThat(testModelRunDef.getObjFunction()).isEqualTo(UPDATED_OBJ_FUNCTION);
        assertThat(testModelRunDef.getRunDate()).isEqualTo(UPDATED_RUN_DATE);
        assertThat(testModelRunDef.getStartTime()).isEqualTo(UPDATED_START_TIME);
        assertThat(testModelRunDef.getEndTime()).isEqualTo(UPDATED_END_TIME);
        assertThat(testModelRunDef.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testModelRunDef.getProgress()).isEqualTo(UPDATED_PROGRESS);
        assertThat(testModelRunDef.getDataSetInput()).isEqualTo(UPDATED_DATA_SET_INPUT);
        assertThat(testModelRunDef.getDataSetResult()).isEqualTo(UPDATED_DATA_SET_RESULT);
        assertThat(testModelRunDef.getUserResult()).isEqualTo(UPDATED_USER_RESULT);
        assertThat(testModelRunDef.getUserResultContentType()).isEqualTo(UPDATED_USER_RESULT_CONTENT_TYPE);
        assertThat(testModelRunDef.getOtherResult()).isEqualTo(UPDATED_OTHER_RESULT);
        assertThat(testModelRunDef.getOtherResultContentType()).isEqualTo(UPDATED_OTHER_RESULT_CONTENT_TYPE);
        assertThat(testModelRunDef.getComments()).isEqualTo(UPDATED_COMMENTS);
        assertThat(testModelRunDef.getUser()).isEqualTo(UPDATED_USER);
    }

    @Test
    @Transactional
    public void updateNonExistingModelRunDef() throws Exception {
        int databaseSizeBeforeUpdate = modelRunDefRepository.findAll().size();

        // Create the ModelRunDef
        ModelRunDefDTO modelRunDefDTO = modelRunDefMapper.toDto(modelRunDef);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restModelRunDefMockMvc.perform(put("/api/model-run-defs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelRunDefDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ModelRunDef in the database
        List<ModelRunDef> modelRunDefList = modelRunDefRepository.findAll();
        assertThat(modelRunDefList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteModelRunDef() throws Exception {
        // Initialize the database
        modelRunDefRepository.saveAndFlush(modelRunDef);

        int databaseSizeBeforeDelete = modelRunDefRepository.findAll().size();

        // Get the modelRunDef
        restModelRunDefMockMvc.perform(delete("/api/model-run-defs/{id}", modelRunDef.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ModelRunDef> modelRunDefList = modelRunDefRepository.findAll();
        assertThat(modelRunDefList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ModelRunDef.class);
        ModelRunDef modelRunDef1 = new ModelRunDef();
        modelRunDef1.setId(1L);
        ModelRunDef modelRunDef2 = new ModelRunDef();
        modelRunDef2.setId(modelRunDef1.getId());
        assertThat(modelRunDef1).isEqualTo(modelRunDef2);
        modelRunDef2.setId(2L);
        assertThat(modelRunDef1).isNotEqualTo(modelRunDef2);
        modelRunDef1.setId(null);
        assertThat(modelRunDef1).isNotEqualTo(modelRunDef2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ModelRunDefDTO.class);
        ModelRunDefDTO modelRunDefDTO1 = new ModelRunDefDTO();
        modelRunDefDTO1.setId(1L);
        ModelRunDefDTO modelRunDefDTO2 = new ModelRunDefDTO();
        assertThat(modelRunDefDTO1).isNotEqualTo(modelRunDefDTO2);
        modelRunDefDTO2.setId(modelRunDefDTO1.getId());
        assertThat(modelRunDefDTO1).isEqualTo(modelRunDefDTO2);
        modelRunDefDTO2.setId(2L);
        assertThat(modelRunDefDTO1).isNotEqualTo(modelRunDefDTO2);
        modelRunDefDTO1.setId(null);
        assertThat(modelRunDefDTO1).isNotEqualTo(modelRunDefDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(modelRunDefMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(modelRunDefMapper.fromId(null)).isNull();
    }
}
