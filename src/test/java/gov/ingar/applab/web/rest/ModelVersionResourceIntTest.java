package gov.ingar.applab.web.rest;

import gov.ingar.applab.AppLab2App;

import gov.ingar.applab.domain.ModelVersion;
import gov.ingar.applab.repository.ModelVersionRepository;
import gov.ingar.applab.service.ModelVersionService;
import gov.ingar.applab.service.dto.ModelVersionDTO;
import gov.ingar.applab.service.mapper.ModelVersionMapper;
import gov.ingar.applab.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static gov.ingar.applab.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ModelVersionResource REST controller.
 *
 * @see ModelVersionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppLab2App.class)
public class ModelVersionResourceIntTest {

    private static final Integer DEFAULT_VERSION = 1;
    private static final Integer UPDATED_VERSION = 2;

    private static final LocalDate DEFAULT_CREATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_SRC_PATH = "AAAAAAAAAA";
    private static final String UPDATED_SRC_PATH = "BBBBBBBBBB";

    private static final String DEFAULT_SRC_CONTENT = "AAAAAAAAAA";
    private static final String UPDATED_SRC_CONTENT = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private ModelVersionRepository modelVersionRepository;

    @Autowired
    private ModelVersionMapper modelVersionMapper;

    @Autowired
    private ModelVersionService modelVersionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restModelVersionMockMvc;

    private ModelVersion modelVersion;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ModelVersionResource modelVersionResource = new ModelVersionResource(modelVersionService);
        this.restModelVersionMockMvc = MockMvcBuilders.standaloneSetup(modelVersionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ModelVersion createEntity(EntityManager em) {
        ModelVersion modelVersion = new ModelVersion()
            .version(DEFAULT_VERSION)
            .creationDate(DEFAULT_CREATION_DATE)
            .srcPath(DEFAULT_SRC_PATH)
            .srcContent(DEFAULT_SRC_CONTENT)
            .description(DEFAULT_DESCRIPTION);
        return modelVersion;
    }

    @Before
    public void initTest() {
        modelVersion = createEntity(em);
    }

    @Test
    @Transactional
    public void createModelVersion() throws Exception {
        int databaseSizeBeforeCreate = modelVersionRepository.findAll().size();

        // Create the ModelVersion
        ModelVersionDTO modelVersionDTO = modelVersionMapper.toDto(modelVersion);
        restModelVersionMockMvc.perform(post("/api/model-versions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelVersionDTO)))
            .andExpect(status().isCreated());

        // Validate the ModelVersion in the database
        List<ModelVersion> modelVersionList = modelVersionRepository.findAll();
        assertThat(modelVersionList).hasSize(databaseSizeBeforeCreate + 1);
        ModelVersion testModelVersion = modelVersionList.get(modelVersionList.size() - 1);
        assertThat(testModelVersion.getVersion()).isEqualTo(DEFAULT_VERSION);
        assertThat(testModelVersion.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testModelVersion.getSrcPath()).isEqualTo(DEFAULT_SRC_PATH);
        assertThat(testModelVersion.getSrcContent()).isEqualTo(DEFAULT_SRC_CONTENT);
        assertThat(testModelVersion.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createModelVersionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = modelVersionRepository.findAll().size();

        // Create the ModelVersion with an existing ID
        modelVersion.setId(1L);
        ModelVersionDTO modelVersionDTO = modelVersionMapper.toDto(modelVersion);

        // An entity with an existing ID cannot be created, so this API call must fail
        restModelVersionMockMvc.perform(post("/api/model-versions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelVersionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ModelVersion in the database
        List<ModelVersion> modelVersionList = modelVersionRepository.findAll();
        assertThat(modelVersionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkVersionIsRequired() throws Exception {
        int databaseSizeBeforeTest = modelVersionRepository.findAll().size();
        // set the field null
        modelVersion.setVersion(null);

        // Create the ModelVersion, which fails.
        ModelVersionDTO modelVersionDTO = modelVersionMapper.toDto(modelVersion);

        restModelVersionMockMvc.perform(post("/api/model-versions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelVersionDTO)))
            .andExpect(status().isBadRequest());

        List<ModelVersion> modelVersionList = modelVersionRepository.findAll();
        assertThat(modelVersionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllModelVersions() throws Exception {
        // Initialize the database
        modelVersionRepository.saveAndFlush(modelVersion);

        // Get all the modelVersionList
        restModelVersionMockMvc.perform(get("/api/model-versions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(modelVersion.getId().intValue())))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION)))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].srcPath").value(hasItem(DEFAULT_SRC_PATH.toString())))
            .andExpect(jsonPath("$.[*].srcContent").value(hasItem(DEFAULT_SRC_CONTENT.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getModelVersion() throws Exception {
        // Initialize the database
        modelVersionRepository.saveAndFlush(modelVersion);

        // Get the modelVersion
        restModelVersionMockMvc.perform(get("/api/model-versions/{id}", modelVersion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(modelVersion.getId().intValue()))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION))
            .andExpect(jsonPath("$.creationDate").value(DEFAULT_CREATION_DATE.toString()))
            .andExpect(jsonPath("$.srcPath").value(DEFAULT_SRC_PATH.toString()))
            .andExpect(jsonPath("$.srcContent").value(DEFAULT_SRC_CONTENT.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingModelVersion() throws Exception {
        // Get the modelVersion
        restModelVersionMockMvc.perform(get("/api/model-versions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateModelVersion() throws Exception {
        // Initialize the database
        modelVersionRepository.saveAndFlush(modelVersion);

        int databaseSizeBeforeUpdate = modelVersionRepository.findAll().size();

        // Update the modelVersion
        ModelVersion updatedModelVersion = modelVersionRepository.findById(modelVersion.getId()).get();
        // Disconnect from session so that the updates on updatedModelVersion are not directly saved in db
        em.detach(updatedModelVersion);
        updatedModelVersion
            .version(UPDATED_VERSION)
            .creationDate(UPDATED_CREATION_DATE)
            .srcPath(UPDATED_SRC_PATH)
            .srcContent(UPDATED_SRC_CONTENT)
            .description(UPDATED_DESCRIPTION);
        ModelVersionDTO modelVersionDTO = modelVersionMapper.toDto(updatedModelVersion);

        restModelVersionMockMvc.perform(put("/api/model-versions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelVersionDTO)))
            .andExpect(status().isOk());

        // Validate the ModelVersion in the database
        List<ModelVersion> modelVersionList = modelVersionRepository.findAll();
        assertThat(modelVersionList).hasSize(databaseSizeBeforeUpdate);
        ModelVersion testModelVersion = modelVersionList.get(modelVersionList.size() - 1);
        assertThat(testModelVersion.getVersion()).isEqualTo(UPDATED_VERSION);
        assertThat(testModelVersion.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testModelVersion.getSrcPath()).isEqualTo(UPDATED_SRC_PATH);
        assertThat(testModelVersion.getSrcContent()).isEqualTo(UPDATED_SRC_CONTENT);
        assertThat(testModelVersion.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingModelVersion() throws Exception {
        int databaseSizeBeforeUpdate = modelVersionRepository.findAll().size();

        // Create the ModelVersion
        ModelVersionDTO modelVersionDTO = modelVersionMapper.toDto(modelVersion);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restModelVersionMockMvc.perform(put("/api/model-versions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelVersionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ModelVersion in the database
        List<ModelVersion> modelVersionList = modelVersionRepository.findAll();
        assertThat(modelVersionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteModelVersion() throws Exception {
        // Initialize the database
        modelVersionRepository.saveAndFlush(modelVersion);

        int databaseSizeBeforeDelete = modelVersionRepository.findAll().size();

        // Get the modelVersion
        restModelVersionMockMvc.perform(delete("/api/model-versions/{id}", modelVersion.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ModelVersion> modelVersionList = modelVersionRepository.findAll();
        assertThat(modelVersionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ModelVersion.class);
        ModelVersion modelVersion1 = new ModelVersion();
        modelVersion1.setId(1L);
        ModelVersion modelVersion2 = new ModelVersion();
        modelVersion2.setId(modelVersion1.getId());
        assertThat(modelVersion1).isEqualTo(modelVersion2);
        modelVersion2.setId(2L);
        assertThat(modelVersion1).isNotEqualTo(modelVersion2);
        modelVersion1.setId(null);
        assertThat(modelVersion1).isNotEqualTo(modelVersion2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ModelVersionDTO.class);
        ModelVersionDTO modelVersionDTO1 = new ModelVersionDTO();
        modelVersionDTO1.setId(1L);
        ModelVersionDTO modelVersionDTO2 = new ModelVersionDTO();
        assertThat(modelVersionDTO1).isNotEqualTo(modelVersionDTO2);
        modelVersionDTO2.setId(modelVersionDTO1.getId());
        assertThat(modelVersionDTO1).isEqualTo(modelVersionDTO2);
        modelVersionDTO2.setId(2L);
        assertThat(modelVersionDTO1).isNotEqualTo(modelVersionDTO2);
        modelVersionDTO1.setId(null);
        assertThat(modelVersionDTO1).isNotEqualTo(modelVersionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(modelVersionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(modelVersionMapper.fromId(null)).isNull();
    }
}
