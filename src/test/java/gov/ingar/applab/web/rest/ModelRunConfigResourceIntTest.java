package gov.ingar.applab.web.rest;

import gov.ingar.applab.AppLab2App;

import gov.ingar.applab.domain.ModelRunConfig;
import gov.ingar.applab.repository.ModelRunConfigRepository;
import gov.ingar.applab.service.ModelRunConfigService;
import gov.ingar.applab.service.dto.ModelRunConfigDTO;
import gov.ingar.applab.service.mapper.ModelRunConfigMapper;
import gov.ingar.applab.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static gov.ingar.applab.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ModelRunConfigResource REST controller.
 *
 * @see ModelRunConfigResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppLab2App.class)
public class ModelRunConfigResourceIntTest {

    private static final String DEFAULT_PARAM = "AAAAAAAAAA";
    private static final String UPDATED_PARAM = "BBBBBBBBBB";

    private static final String DEFAULT_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_VALUE = "BBBBBBBBBB";

    @Autowired
    private ModelRunConfigRepository modelRunConfigRepository;

    @Autowired
    private ModelRunConfigMapper modelRunConfigMapper;

    @Autowired
    private ModelRunConfigService modelRunConfigService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restModelRunConfigMockMvc;

    private ModelRunConfig modelRunConfig;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ModelRunConfigResource modelRunConfigResource = new ModelRunConfigResource(modelRunConfigService);
        this.restModelRunConfigMockMvc = MockMvcBuilders.standaloneSetup(modelRunConfigResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ModelRunConfig createEntity(EntityManager em) {
        ModelRunConfig modelRunConfig = new ModelRunConfig()
            .param(DEFAULT_PARAM)
            .value(DEFAULT_VALUE);
        return modelRunConfig;
    }

    @Before
    public void initTest() {
        modelRunConfig = createEntity(em);
    }

    @Test
    @Transactional
    public void createModelRunConfig() throws Exception {
        int databaseSizeBeforeCreate = modelRunConfigRepository.findAll().size();

        // Create the ModelRunConfig
        ModelRunConfigDTO modelRunConfigDTO = modelRunConfigMapper.toDto(modelRunConfig);
        restModelRunConfigMockMvc.perform(post("/api/model-run-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelRunConfigDTO)))
            .andExpect(status().isCreated());

        // Validate the ModelRunConfig in the database
        List<ModelRunConfig> modelRunConfigList = modelRunConfigRepository.findAll();
        assertThat(modelRunConfigList).hasSize(databaseSizeBeforeCreate + 1);
        ModelRunConfig testModelRunConfig = modelRunConfigList.get(modelRunConfigList.size() - 1);
        assertThat(testModelRunConfig.getParam()).isEqualTo(DEFAULT_PARAM);
        assertThat(testModelRunConfig.getValue()).isEqualTo(DEFAULT_VALUE);
    }

    @Test
    @Transactional
    public void createModelRunConfigWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = modelRunConfigRepository.findAll().size();

        // Create the ModelRunConfig with an existing ID
        modelRunConfig.setId(1L);
        ModelRunConfigDTO modelRunConfigDTO = modelRunConfigMapper.toDto(modelRunConfig);

        // An entity with an existing ID cannot be created, so this API call must fail
        restModelRunConfigMockMvc.perform(post("/api/model-run-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelRunConfigDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ModelRunConfig in the database
        List<ModelRunConfig> modelRunConfigList = modelRunConfigRepository.findAll();
        assertThat(modelRunConfigList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkParamIsRequired() throws Exception {
        int databaseSizeBeforeTest = modelRunConfigRepository.findAll().size();
        // set the field null
        modelRunConfig.setParam(null);

        // Create the ModelRunConfig, which fails.
        ModelRunConfigDTO modelRunConfigDTO = modelRunConfigMapper.toDto(modelRunConfig);

        restModelRunConfigMockMvc.perform(post("/api/model-run-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelRunConfigDTO)))
            .andExpect(status().isBadRequest());

        List<ModelRunConfig> modelRunConfigList = modelRunConfigRepository.findAll();
        assertThat(modelRunConfigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllModelRunConfigs() throws Exception {
        // Initialize the database
        modelRunConfigRepository.saveAndFlush(modelRunConfig);

        // Get all the modelRunConfigList
        restModelRunConfigMockMvc.perform(get("/api/model-run-configs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(modelRunConfig.getId().intValue())))
            .andExpect(jsonPath("$.[*].param").value(hasItem(DEFAULT_PARAM.toString())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.toString())));
    }

    @Test
    @Transactional
    public void getModelRunConfig() throws Exception {
        // Initialize the database
        modelRunConfigRepository.saveAndFlush(modelRunConfig);

        // Get the modelRunConfig
        restModelRunConfigMockMvc.perform(get("/api/model-run-configs/{id}", modelRunConfig.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(modelRunConfig.getId().intValue()))
            .andExpect(jsonPath("$.param").value(DEFAULT_PARAM.toString()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingModelRunConfig() throws Exception {
        // Get the modelRunConfig
        restModelRunConfigMockMvc.perform(get("/api/model-run-configs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateModelRunConfig() throws Exception {
        // Initialize the database
        modelRunConfigRepository.saveAndFlush(modelRunConfig);

        int databaseSizeBeforeUpdate = modelRunConfigRepository.findAll().size();

        // Update the modelRunConfig
        ModelRunConfig updatedModelRunConfig = modelRunConfigRepository.findById(modelRunConfig.getId()).get();
        // Disconnect from session so that the updates on updatedModelRunConfig are not directly saved in db
        em.detach(updatedModelRunConfig);
        updatedModelRunConfig
            .param(UPDATED_PARAM)
            .value(UPDATED_VALUE);
        ModelRunConfigDTO modelRunConfigDTO = modelRunConfigMapper.toDto(updatedModelRunConfig);

        restModelRunConfigMockMvc.perform(put("/api/model-run-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelRunConfigDTO)))
            .andExpect(status().isOk());

        // Validate the ModelRunConfig in the database
        List<ModelRunConfig> modelRunConfigList = modelRunConfigRepository.findAll();
        assertThat(modelRunConfigList).hasSize(databaseSizeBeforeUpdate);
        ModelRunConfig testModelRunConfig = modelRunConfigList.get(modelRunConfigList.size() - 1);
        assertThat(testModelRunConfig.getParam()).isEqualTo(UPDATED_PARAM);
        assertThat(testModelRunConfig.getValue()).isEqualTo(UPDATED_VALUE);
    }

    @Test
    @Transactional
    public void updateNonExistingModelRunConfig() throws Exception {
        int databaseSizeBeforeUpdate = modelRunConfigRepository.findAll().size();

        // Create the ModelRunConfig
        ModelRunConfigDTO modelRunConfigDTO = modelRunConfigMapper.toDto(modelRunConfig);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restModelRunConfigMockMvc.perform(put("/api/model-run-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelRunConfigDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ModelRunConfig in the database
        List<ModelRunConfig> modelRunConfigList = modelRunConfigRepository.findAll();
        assertThat(modelRunConfigList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteModelRunConfig() throws Exception {
        // Initialize the database
        modelRunConfigRepository.saveAndFlush(modelRunConfig);

        int databaseSizeBeforeDelete = modelRunConfigRepository.findAll().size();

        // Get the modelRunConfig
        restModelRunConfigMockMvc.perform(delete("/api/model-run-configs/{id}", modelRunConfig.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ModelRunConfig> modelRunConfigList = modelRunConfigRepository.findAll();
        assertThat(modelRunConfigList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ModelRunConfig.class);
        ModelRunConfig modelRunConfig1 = new ModelRunConfig();
        modelRunConfig1.setId(1L);
        ModelRunConfig modelRunConfig2 = new ModelRunConfig();
        modelRunConfig2.setId(modelRunConfig1.getId());
        assertThat(modelRunConfig1).isEqualTo(modelRunConfig2);
        modelRunConfig2.setId(2L);
        assertThat(modelRunConfig1).isNotEqualTo(modelRunConfig2);
        modelRunConfig1.setId(null);
        assertThat(modelRunConfig1).isNotEqualTo(modelRunConfig2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ModelRunConfigDTO.class);
        ModelRunConfigDTO modelRunConfigDTO1 = new ModelRunConfigDTO();
        modelRunConfigDTO1.setId(1L);
        ModelRunConfigDTO modelRunConfigDTO2 = new ModelRunConfigDTO();
        assertThat(modelRunConfigDTO1).isNotEqualTo(modelRunConfigDTO2);
        modelRunConfigDTO2.setId(modelRunConfigDTO1.getId());
        assertThat(modelRunConfigDTO1).isEqualTo(modelRunConfigDTO2);
        modelRunConfigDTO2.setId(2L);
        assertThat(modelRunConfigDTO1).isNotEqualTo(modelRunConfigDTO2);
        modelRunConfigDTO1.setId(null);
        assertThat(modelRunConfigDTO1).isNotEqualTo(modelRunConfigDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(modelRunConfigMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(modelRunConfigMapper.fromId(null)).isNull();
    }
}
