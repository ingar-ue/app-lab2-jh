package gov.ingar.applab.web.rest;

import gov.ingar.applab.AppLab2App;

import gov.ingar.applab.domain.ModelObjFunction;
import gov.ingar.applab.repository.ModelObjFunctionRepository;
import gov.ingar.applab.service.ModelObjFunctionService;
import gov.ingar.applab.service.dto.ModelObjFunctionDTO;
import gov.ingar.applab.service.mapper.ModelObjFunctionMapper;
import gov.ingar.applab.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static gov.ingar.applab.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ModelObjFunctionResource REST controller.
 *
 * @see ModelObjFunctionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppLab2App.class)
public class ModelObjFunctionResourceIntTest {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private ModelObjFunctionRepository modelObjFunctionRepository;

    @Autowired
    private ModelObjFunctionMapper modelObjFunctionMapper;

    @Autowired
    private ModelObjFunctionService modelObjFunctionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restModelObjFunctionMockMvc;

    private ModelObjFunction modelObjFunction;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ModelObjFunctionResource modelObjFunctionResource = new ModelObjFunctionResource(modelObjFunctionService);
        this.restModelObjFunctionMockMvc = MockMvcBuilders.standaloneSetup(modelObjFunctionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ModelObjFunction createEntity(EntityManager em) {
        ModelObjFunction modelObjFunction = new ModelObjFunction()
            .code(DEFAULT_CODE)
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION);
        return modelObjFunction;
    }

    @Before
    public void initTest() {
        modelObjFunction = createEntity(em);
    }

    @Test
    @Transactional
    public void createModelObjFunction() throws Exception {
        int databaseSizeBeforeCreate = modelObjFunctionRepository.findAll().size();

        // Create the ModelObjFunction
        ModelObjFunctionDTO modelObjFunctionDTO = modelObjFunctionMapper.toDto(modelObjFunction);
        restModelObjFunctionMockMvc.perform(post("/api/model-obj-functions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelObjFunctionDTO)))
            .andExpect(status().isCreated());

        // Validate the ModelObjFunction in the database
        List<ModelObjFunction> modelObjFunctionList = modelObjFunctionRepository.findAll();
        assertThat(modelObjFunctionList).hasSize(databaseSizeBeforeCreate + 1);
        ModelObjFunction testModelObjFunction = modelObjFunctionList.get(modelObjFunctionList.size() - 1);
        assertThat(testModelObjFunction.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testModelObjFunction.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testModelObjFunction.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createModelObjFunctionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = modelObjFunctionRepository.findAll().size();

        // Create the ModelObjFunction with an existing ID
        modelObjFunction.setId(1L);
        ModelObjFunctionDTO modelObjFunctionDTO = modelObjFunctionMapper.toDto(modelObjFunction);

        // An entity with an existing ID cannot be created, so this API call must fail
        restModelObjFunctionMockMvc.perform(post("/api/model-obj-functions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelObjFunctionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ModelObjFunction in the database
        List<ModelObjFunction> modelObjFunctionList = modelObjFunctionRepository.findAll();
        assertThat(modelObjFunctionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = modelObjFunctionRepository.findAll().size();
        // set the field null
        modelObjFunction.setCode(null);

        // Create the ModelObjFunction, which fails.
        ModelObjFunctionDTO modelObjFunctionDTO = modelObjFunctionMapper.toDto(modelObjFunction);

        restModelObjFunctionMockMvc.perform(post("/api/model-obj-functions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelObjFunctionDTO)))
            .andExpect(status().isBadRequest());

        List<ModelObjFunction> modelObjFunctionList = modelObjFunctionRepository.findAll();
        assertThat(modelObjFunctionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = modelObjFunctionRepository.findAll().size();
        // set the field null
        modelObjFunction.setName(null);

        // Create the ModelObjFunction, which fails.
        ModelObjFunctionDTO modelObjFunctionDTO = modelObjFunctionMapper.toDto(modelObjFunction);

        restModelObjFunctionMockMvc.perform(post("/api/model-obj-functions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelObjFunctionDTO)))
            .andExpect(status().isBadRequest());

        List<ModelObjFunction> modelObjFunctionList = modelObjFunctionRepository.findAll();
        assertThat(modelObjFunctionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllModelObjFunctions() throws Exception {
        // Initialize the database
        modelObjFunctionRepository.saveAndFlush(modelObjFunction);

        // Get all the modelObjFunctionList
        restModelObjFunctionMockMvc.perform(get("/api/model-obj-functions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(modelObjFunction.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getModelObjFunction() throws Exception {
        // Initialize the database
        modelObjFunctionRepository.saveAndFlush(modelObjFunction);

        // Get the modelObjFunction
        restModelObjFunctionMockMvc.perform(get("/api/model-obj-functions/{id}", modelObjFunction.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(modelObjFunction.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingModelObjFunction() throws Exception {
        // Get the modelObjFunction
        restModelObjFunctionMockMvc.perform(get("/api/model-obj-functions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateModelObjFunction() throws Exception {
        // Initialize the database
        modelObjFunctionRepository.saveAndFlush(modelObjFunction);

        int databaseSizeBeforeUpdate = modelObjFunctionRepository.findAll().size();

        // Update the modelObjFunction
        ModelObjFunction updatedModelObjFunction = modelObjFunctionRepository.findById(modelObjFunction.getId()).get();
        // Disconnect from session so that the updates on updatedModelObjFunction are not directly saved in db
        em.detach(updatedModelObjFunction);
        updatedModelObjFunction
            .code(UPDATED_CODE)
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION);
        ModelObjFunctionDTO modelObjFunctionDTO = modelObjFunctionMapper.toDto(updatedModelObjFunction);

        restModelObjFunctionMockMvc.perform(put("/api/model-obj-functions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelObjFunctionDTO)))
            .andExpect(status().isOk());

        // Validate the ModelObjFunction in the database
        List<ModelObjFunction> modelObjFunctionList = modelObjFunctionRepository.findAll();
        assertThat(modelObjFunctionList).hasSize(databaseSizeBeforeUpdate);
        ModelObjFunction testModelObjFunction = modelObjFunctionList.get(modelObjFunctionList.size() - 1);
        assertThat(testModelObjFunction.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testModelObjFunction.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testModelObjFunction.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingModelObjFunction() throws Exception {
        int databaseSizeBeforeUpdate = modelObjFunctionRepository.findAll().size();

        // Create the ModelObjFunction
        ModelObjFunctionDTO modelObjFunctionDTO = modelObjFunctionMapper.toDto(modelObjFunction);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restModelObjFunctionMockMvc.perform(put("/api/model-obj-functions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelObjFunctionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ModelObjFunction in the database
        List<ModelObjFunction> modelObjFunctionList = modelObjFunctionRepository.findAll();
        assertThat(modelObjFunctionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteModelObjFunction() throws Exception {
        // Initialize the database
        modelObjFunctionRepository.saveAndFlush(modelObjFunction);

        int databaseSizeBeforeDelete = modelObjFunctionRepository.findAll().size();

        // Get the modelObjFunction
        restModelObjFunctionMockMvc.perform(delete("/api/model-obj-functions/{id}", modelObjFunction.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ModelObjFunction> modelObjFunctionList = modelObjFunctionRepository.findAll();
        assertThat(modelObjFunctionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ModelObjFunction.class);
        ModelObjFunction modelObjFunction1 = new ModelObjFunction();
        modelObjFunction1.setId(1L);
        ModelObjFunction modelObjFunction2 = new ModelObjFunction();
        modelObjFunction2.setId(modelObjFunction1.getId());
        assertThat(modelObjFunction1).isEqualTo(modelObjFunction2);
        modelObjFunction2.setId(2L);
        assertThat(modelObjFunction1).isNotEqualTo(modelObjFunction2);
        modelObjFunction1.setId(null);
        assertThat(modelObjFunction1).isNotEqualTo(modelObjFunction2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ModelObjFunctionDTO.class);
        ModelObjFunctionDTO modelObjFunctionDTO1 = new ModelObjFunctionDTO();
        modelObjFunctionDTO1.setId(1L);
        ModelObjFunctionDTO modelObjFunctionDTO2 = new ModelObjFunctionDTO();
        assertThat(modelObjFunctionDTO1).isNotEqualTo(modelObjFunctionDTO2);
        modelObjFunctionDTO2.setId(modelObjFunctionDTO1.getId());
        assertThat(modelObjFunctionDTO1).isEqualTo(modelObjFunctionDTO2);
        modelObjFunctionDTO2.setId(2L);
        assertThat(modelObjFunctionDTO1).isNotEqualTo(modelObjFunctionDTO2);
        modelObjFunctionDTO1.setId(null);
        assertThat(modelObjFunctionDTO1).isNotEqualTo(modelObjFunctionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(modelObjFunctionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(modelObjFunctionMapper.fromId(null)).isNull();
    }
}
