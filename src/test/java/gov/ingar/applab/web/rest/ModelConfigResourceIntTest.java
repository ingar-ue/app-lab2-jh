package gov.ingar.applab.web.rest;

import gov.ingar.applab.AppLab2App;

import gov.ingar.applab.domain.ModelConfig;
import gov.ingar.applab.repository.ModelConfigRepository;
import gov.ingar.applab.service.ModelConfigService;
import gov.ingar.applab.service.dto.ModelConfigDTO;
import gov.ingar.applab.service.mapper.ModelConfigMapper;
import gov.ingar.applab.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static gov.ingar.applab.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ModelConfigResource REST controller.
 *
 * @see ModelConfigResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppLab2App.class)
public class ModelConfigResourceIntTest {

    private static final String DEFAULT_PARAM = "AAAAAAAAAA";
    private static final String UPDATED_PARAM = "BBBBBBBBBB";

    private static final String DEFAULT_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_VALUE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private ModelConfigRepository modelConfigRepository;

    @Autowired
    private ModelConfigMapper modelConfigMapper;

    @Autowired
    private ModelConfigService modelConfigService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restModelConfigMockMvc;

    private ModelConfig modelConfig;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ModelConfigResource modelConfigResource = new ModelConfigResource(modelConfigService);
        this.restModelConfigMockMvc = MockMvcBuilders.standaloneSetup(modelConfigResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ModelConfig createEntity(EntityManager em) {
        ModelConfig modelConfig = new ModelConfig()
            .param(DEFAULT_PARAM)
            .value(DEFAULT_VALUE)
            .description(DEFAULT_DESCRIPTION);
        return modelConfig;
    }

    @Before
    public void initTest() {
        modelConfig = createEntity(em);
    }

    @Test
    @Transactional
    public void createModelConfig() throws Exception {
        int databaseSizeBeforeCreate = modelConfigRepository.findAll().size();

        // Create the ModelConfig
        ModelConfigDTO modelConfigDTO = modelConfigMapper.toDto(modelConfig);
        restModelConfigMockMvc.perform(post("/api/model-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelConfigDTO)))
            .andExpect(status().isCreated());

        // Validate the ModelConfig in the database
        List<ModelConfig> modelConfigList = modelConfigRepository.findAll();
        assertThat(modelConfigList).hasSize(databaseSizeBeforeCreate + 1);
        ModelConfig testModelConfig = modelConfigList.get(modelConfigList.size() - 1);
        assertThat(testModelConfig.getParam()).isEqualTo(DEFAULT_PARAM);
        assertThat(testModelConfig.getValue()).isEqualTo(DEFAULT_VALUE);
        assertThat(testModelConfig.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createModelConfigWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = modelConfigRepository.findAll().size();

        // Create the ModelConfig with an existing ID
        modelConfig.setId(1L);
        ModelConfigDTO modelConfigDTO = modelConfigMapper.toDto(modelConfig);

        // An entity with an existing ID cannot be created, so this API call must fail
        restModelConfigMockMvc.perform(post("/api/model-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelConfigDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ModelConfig in the database
        List<ModelConfig> modelConfigList = modelConfigRepository.findAll();
        assertThat(modelConfigList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkParamIsRequired() throws Exception {
        int databaseSizeBeforeTest = modelConfigRepository.findAll().size();
        // set the field null
        modelConfig.setParam(null);

        // Create the ModelConfig, which fails.
        ModelConfigDTO modelConfigDTO = modelConfigMapper.toDto(modelConfig);

        restModelConfigMockMvc.perform(post("/api/model-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelConfigDTO)))
            .andExpect(status().isBadRequest());

        List<ModelConfig> modelConfigList = modelConfigRepository.findAll();
        assertThat(modelConfigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllModelConfigs() throws Exception {
        // Initialize the database
        modelConfigRepository.saveAndFlush(modelConfig);

        // Get all the modelConfigList
        restModelConfigMockMvc.perform(get("/api/model-configs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(modelConfig.getId().intValue())))
            .andExpect(jsonPath("$.[*].param").value(hasItem(DEFAULT_PARAM.toString())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getModelConfig() throws Exception {
        // Initialize the database
        modelConfigRepository.saveAndFlush(modelConfig);

        // Get the modelConfig
        restModelConfigMockMvc.perform(get("/api/model-configs/{id}", modelConfig.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(modelConfig.getId().intValue()))
            .andExpect(jsonPath("$.param").value(DEFAULT_PARAM.toString()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingModelConfig() throws Exception {
        // Get the modelConfig
        restModelConfigMockMvc.perform(get("/api/model-configs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateModelConfig() throws Exception {
        // Initialize the database
        modelConfigRepository.saveAndFlush(modelConfig);

        int databaseSizeBeforeUpdate = modelConfigRepository.findAll().size();

        // Update the modelConfig
        ModelConfig updatedModelConfig = modelConfigRepository.findById(modelConfig.getId()).get();
        // Disconnect from session so that the updates on updatedModelConfig are not directly saved in db
        em.detach(updatedModelConfig);
        updatedModelConfig
            .param(UPDATED_PARAM)
            .value(UPDATED_VALUE)
            .description(UPDATED_DESCRIPTION);
        ModelConfigDTO modelConfigDTO = modelConfigMapper.toDto(updatedModelConfig);

        restModelConfigMockMvc.perform(put("/api/model-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelConfigDTO)))
            .andExpect(status().isOk());

        // Validate the ModelConfig in the database
        List<ModelConfig> modelConfigList = modelConfigRepository.findAll();
        assertThat(modelConfigList).hasSize(databaseSizeBeforeUpdate);
        ModelConfig testModelConfig = modelConfigList.get(modelConfigList.size() - 1);
        assertThat(testModelConfig.getParam()).isEqualTo(UPDATED_PARAM);
        assertThat(testModelConfig.getValue()).isEqualTo(UPDATED_VALUE);
        assertThat(testModelConfig.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingModelConfig() throws Exception {
        int databaseSizeBeforeUpdate = modelConfigRepository.findAll().size();

        // Create the ModelConfig
        ModelConfigDTO modelConfigDTO = modelConfigMapper.toDto(modelConfig);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restModelConfigMockMvc.perform(put("/api/model-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelConfigDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ModelConfig in the database
        List<ModelConfig> modelConfigList = modelConfigRepository.findAll();
        assertThat(modelConfigList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteModelConfig() throws Exception {
        // Initialize the database
        modelConfigRepository.saveAndFlush(modelConfig);

        int databaseSizeBeforeDelete = modelConfigRepository.findAll().size();

        // Get the modelConfig
        restModelConfigMockMvc.perform(delete("/api/model-configs/{id}", modelConfig.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ModelConfig> modelConfigList = modelConfigRepository.findAll();
        assertThat(modelConfigList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ModelConfig.class);
        ModelConfig modelConfig1 = new ModelConfig();
        modelConfig1.setId(1L);
        ModelConfig modelConfig2 = new ModelConfig();
        modelConfig2.setId(modelConfig1.getId());
        assertThat(modelConfig1).isEqualTo(modelConfig2);
        modelConfig2.setId(2L);
        assertThat(modelConfig1).isNotEqualTo(modelConfig2);
        modelConfig1.setId(null);
        assertThat(modelConfig1).isNotEqualTo(modelConfig2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ModelConfigDTO.class);
        ModelConfigDTO modelConfigDTO1 = new ModelConfigDTO();
        modelConfigDTO1.setId(1L);
        ModelConfigDTO modelConfigDTO2 = new ModelConfigDTO();
        assertThat(modelConfigDTO1).isNotEqualTo(modelConfigDTO2);
        modelConfigDTO2.setId(modelConfigDTO1.getId());
        assertThat(modelConfigDTO1).isEqualTo(modelConfigDTO2);
        modelConfigDTO2.setId(2L);
        assertThat(modelConfigDTO1).isNotEqualTo(modelConfigDTO2);
        modelConfigDTO1.setId(null);
        assertThat(modelConfigDTO1).isNotEqualTo(modelConfigDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(modelConfigMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(modelConfigMapper.fromId(null)).isNull();
    }
}
