package gov.ingar.applab.web.rest;

import gov.ingar.applab.AppLab2App;

import gov.ingar.applab.domain.ModelDef;
import gov.ingar.applab.repository.ModelDefRepository;
import gov.ingar.applab.service.ModelConfigService;
import gov.ingar.applab.service.ModelDefService;
import gov.ingar.applab.service.ModelObjFunctionService;
import gov.ingar.applab.service.ModelVersionService;
import gov.ingar.applab.service.dto.ModelDefDTO;
import gov.ingar.applab.service.mapper.ModelDefMapper;
import gov.ingar.applab.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static gov.ingar.applab.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import gov.ingar.applab.domain.enumeration.Language;
import gov.ingar.applab.domain.enumeration.Solver;
/**
 * Test class for the ModelDefResource REST controller.
 *
 * @see ModelDefResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppLab2App.class)
public class ModelDefResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Language DEFAULT_LANGUAGE = Language.GAMS;
    private static final Language UPDATED_LANGUAGE = Language.PYOMO;

    private static final Solver DEFAULT_SOLVER = Solver.CPLEX;
    private static final Solver UPDATED_SOLVER = Solver.GUROBI;

    private static final Integer DEFAULT_CURRENT_VERSION = 1;
    private static final Integer UPDATED_CURRENT_VERSION = 2;

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private ModelDefRepository modelDefRepository;

    @Autowired
    private ModelDefMapper modelDefMapper;

    @Autowired
    private ModelDefService modelDefService;

    @Autowired
    private ModelConfigService modelConfigService;

    @Autowired
    private ModelVersionService modelVersionService;

    @Autowired
    private ModelObjFunctionService modelObjFunctionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restModelDefMockMvc;

    private ModelDef modelDef;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ModelDefResource modelDefResource = new ModelDefResource(modelDefService, modelConfigService, modelVersionService, modelObjFunctionService);
        this.restModelDefMockMvc = MockMvcBuilders.standaloneSetup(modelDefResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ModelDef createEntity(EntityManager em) {
        ModelDef modelDef = new ModelDef()
            .name(DEFAULT_NAME)
            .language(DEFAULT_LANGUAGE)
            .solver(DEFAULT_SOLVER)
            .currentVersion(DEFAULT_CURRENT_VERSION)
            .description(DEFAULT_DESCRIPTION);
        return modelDef;
    }

    @Before
    public void initTest() {
        modelDef = createEntity(em);
    }

    @Test
    @Transactional
    public void createModelDef() throws Exception {
        int databaseSizeBeforeCreate = modelDefRepository.findAll().size();

        // Create the ModelDef
        ModelDefDTO modelDefDTO = modelDefMapper.toDto(modelDef);
        restModelDefMockMvc.perform(post("/api/model-defs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelDefDTO)))
            .andExpect(status().isCreated());

        // Validate the ModelDef in the database
        List<ModelDef> modelDefList = modelDefRepository.findAll();
        assertThat(modelDefList).hasSize(databaseSizeBeforeCreate + 1);
        ModelDef testModelDef = modelDefList.get(modelDefList.size() - 1);
        assertThat(testModelDef.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testModelDef.getLanguage()).isEqualTo(DEFAULT_LANGUAGE);
        assertThat(testModelDef.getSolver()).isEqualTo(DEFAULT_SOLVER);
        assertThat(testModelDef.getCurrentVersion()).isEqualTo(DEFAULT_CURRENT_VERSION);
        assertThat(testModelDef.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createModelDefWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = modelDefRepository.findAll().size();

        // Create the ModelDef with an existing ID
        modelDef.setId(1L);
        ModelDefDTO modelDefDTO = modelDefMapper.toDto(modelDef);

        // An entity with an existing ID cannot be created, so this API call must fail
        restModelDefMockMvc.perform(post("/api/model-defs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelDefDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ModelDef in the database
        List<ModelDef> modelDefList = modelDefRepository.findAll();
        assertThat(modelDefList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = modelDefRepository.findAll().size();
        // set the field null
        modelDef.setName(null);

        // Create the ModelDef, which fails.
        ModelDefDTO modelDefDTO = modelDefMapper.toDto(modelDef);

        restModelDefMockMvc.perform(post("/api/model-defs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelDefDTO)))
            .andExpect(status().isBadRequest());

        List<ModelDef> modelDefList = modelDefRepository.findAll();
        assertThat(modelDefList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLanguageIsRequired() throws Exception {
        int databaseSizeBeforeTest = modelDefRepository.findAll().size();
        // set the field null
        modelDef.setLanguage(null);

        // Create the ModelDef, which fails.
        ModelDefDTO modelDefDTO = modelDefMapper.toDto(modelDef);

        restModelDefMockMvc.perform(post("/api/model-defs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelDefDTO)))
            .andExpect(status().isBadRequest());

        List<ModelDef> modelDefList = modelDefRepository.findAll();
        assertThat(modelDefList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSolverIsRequired() throws Exception {
        int databaseSizeBeforeTest = modelDefRepository.findAll().size();
        // set the field null
        modelDef.setSolver(null);

        // Create the ModelDef, which fails.
        ModelDefDTO modelDefDTO = modelDefMapper.toDto(modelDef);

        restModelDefMockMvc.perform(post("/api/model-defs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelDefDTO)))
            .andExpect(status().isBadRequest());

        List<ModelDef> modelDefList = modelDefRepository.findAll();
        assertThat(modelDefList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllModelDefs() throws Exception {
        // Initialize the database
        modelDefRepository.saveAndFlush(modelDef);

        // Get all the modelDefList
        restModelDefMockMvc.perform(get("/api/model-defs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(modelDef.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].language").value(hasItem(DEFAULT_LANGUAGE.toString())))
            .andExpect(jsonPath("$.[*].solver").value(hasItem(DEFAULT_SOLVER.toString())))
            .andExpect(jsonPath("$.[*].currentVersion").value(hasItem(DEFAULT_CURRENT_VERSION)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getModelDef() throws Exception {
        // Initialize the database
        modelDefRepository.saveAndFlush(modelDef);

        // Get the modelDef
        restModelDefMockMvc.perform(get("/api/model-defs/{id}", modelDef.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(modelDef.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.language").value(DEFAULT_LANGUAGE.toString()))
            .andExpect(jsonPath("$.solver").value(DEFAULT_SOLVER.toString()))
            .andExpect(jsonPath("$.currentVersion").value(DEFAULT_CURRENT_VERSION))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingModelDef() throws Exception {
        // Get the modelDef
        restModelDefMockMvc.perform(get("/api/model-defs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateModelDef() throws Exception {
        // Initialize the database
        modelDefRepository.saveAndFlush(modelDef);

        int databaseSizeBeforeUpdate = modelDefRepository.findAll().size();

        // Update the modelDef
        ModelDef updatedModelDef = modelDefRepository.findById(modelDef.getId()).get();
        // Disconnect from session so that the updates on updatedModelDef are not directly saved in db
        em.detach(updatedModelDef);
        updatedModelDef
            .name(UPDATED_NAME)
            .language(UPDATED_LANGUAGE)
            .solver(UPDATED_SOLVER)
            .currentVersion(UPDATED_CURRENT_VERSION)
            .description(UPDATED_DESCRIPTION);
        ModelDefDTO modelDefDTO = modelDefMapper.toDto(updatedModelDef);

        restModelDefMockMvc.perform(put("/api/model-defs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelDefDTO)))
            .andExpect(status().isOk());

        // Validate the ModelDef in the database
        List<ModelDef> modelDefList = modelDefRepository.findAll();
        assertThat(modelDefList).hasSize(databaseSizeBeforeUpdate);
        ModelDef testModelDef = modelDefList.get(modelDefList.size() - 1);
        assertThat(testModelDef.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testModelDef.getLanguage()).isEqualTo(UPDATED_LANGUAGE);
        assertThat(testModelDef.getSolver()).isEqualTo(UPDATED_SOLVER);
        assertThat(testModelDef.getCurrentVersion()).isEqualTo(UPDATED_CURRENT_VERSION);
        assertThat(testModelDef.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingModelDef() throws Exception {
        int databaseSizeBeforeUpdate = modelDefRepository.findAll().size();

        // Create the ModelDef
        ModelDefDTO modelDefDTO = modelDefMapper.toDto(modelDef);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restModelDefMockMvc.perform(put("/api/model-defs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(modelDefDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ModelDef in the database
        List<ModelDef> modelDefList = modelDefRepository.findAll();
        assertThat(modelDefList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteModelDef() throws Exception {
        // Initialize the database
        modelDefRepository.saveAndFlush(modelDef);

        int databaseSizeBeforeDelete = modelDefRepository.findAll().size();

        // Get the modelDef
        restModelDefMockMvc.perform(delete("/api/model-defs/{id}", modelDef.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ModelDef> modelDefList = modelDefRepository.findAll();
        assertThat(modelDefList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ModelDef.class);
        ModelDef modelDef1 = new ModelDef();
        modelDef1.setId(1L);
        ModelDef modelDef2 = new ModelDef();
        modelDef2.setId(modelDef1.getId());
        assertThat(modelDef1).isEqualTo(modelDef2);
        modelDef2.setId(2L);
        assertThat(modelDef1).isNotEqualTo(modelDef2);
        modelDef1.setId(null);
        assertThat(modelDef1).isNotEqualTo(modelDef2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ModelDefDTO.class);
        ModelDefDTO modelDefDTO1 = new ModelDefDTO();
        modelDefDTO1.setId(1L);
        ModelDefDTO modelDefDTO2 = new ModelDefDTO();
        assertThat(modelDefDTO1).isNotEqualTo(modelDefDTO2);
        modelDefDTO2.setId(modelDefDTO1.getId());
        assertThat(modelDefDTO1).isEqualTo(modelDefDTO2);
        modelDefDTO2.setId(2L);
        assertThat(modelDefDTO1).isNotEqualTo(modelDefDTO2);
        modelDefDTO1.setId(null);
        assertThat(modelDefDTO1).isNotEqualTo(modelDefDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(modelDefMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(modelDefMapper.fromId(null)).isNull();
    }
}
