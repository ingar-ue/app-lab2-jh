package gov.ingar.applab.web.rest;

import gov.ingar.applab.AppLab2App;

import gov.ingar.applab.domain.Distance;
import gov.ingar.applab.repository.DistanceRepository;
import gov.ingar.applab.service.DistanceService;
import gov.ingar.applab.service.dto.DistanceDTO;
import gov.ingar.applab.service.mapper.DistanceMapper;
import gov.ingar.applab.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static gov.ingar.applab.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DistanceResource REST controller.
 *
 * @see DistanceResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppLab2App.class)
public class DistanceResourceIntTest {

    private static final Double DEFAULT_DISTANCE = 1D;
    private static final Double UPDATED_DISTANCE = 2D;

    @Autowired
    private DistanceRepository distanceRepository;

    @Autowired
    private DistanceMapper distanceMapper;

    @Autowired
    private DistanceService distanceService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDistanceMockMvc;

    private Distance distance;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DistanceResource distanceResource = new DistanceResource(distanceService);
        this.restDistanceMockMvc = MockMvcBuilders.standaloneSetup(distanceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Distance createEntity(EntityManager em) {
        Distance distance = new Distance()
            .distance(DEFAULT_DISTANCE);
        return distance;
    }

    @Before
    public void initTest() {
        distance = createEntity(em);
    }

    @Test
    @Transactional
    public void createDistance() throws Exception {
        int databaseSizeBeforeCreate = distanceRepository.findAll().size();

        // Create the Distance
        DistanceDTO distanceDTO = distanceMapper.toDto(distance);
        restDistanceMockMvc.perform(post("/api/distances")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(distanceDTO)))
            .andExpect(status().isCreated());

        // Validate the Distance in the database
        List<Distance> distanceList = distanceRepository.findAll();
        assertThat(distanceList).hasSize(databaseSizeBeforeCreate + 1);
        Distance testDistance = distanceList.get(distanceList.size() - 1);
        assertThat(testDistance.getDistance()).isEqualTo(DEFAULT_DISTANCE);
    }

    @Test
    @Transactional
    public void createDistanceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = distanceRepository.findAll().size();

        // Create the Distance with an existing ID
        distance.setId(1L);
        DistanceDTO distanceDTO = distanceMapper.toDto(distance);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDistanceMockMvc.perform(post("/api/distances")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(distanceDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Distance in the database
        List<Distance> distanceList = distanceRepository.findAll();
        assertThat(distanceList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDistanceIsRequired() throws Exception {
        int databaseSizeBeforeTest = distanceRepository.findAll().size();
        // set the field null
        distance.setDistance(null);

        // Create the Distance, which fails.
        DistanceDTO distanceDTO = distanceMapper.toDto(distance);

        restDistanceMockMvc.perform(post("/api/distances")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(distanceDTO)))
            .andExpect(status().isBadRequest());

        List<Distance> distanceList = distanceRepository.findAll();
        assertThat(distanceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDistances() throws Exception {
        // Initialize the database
        distanceRepository.saveAndFlush(distance);

        // Get all the distanceList
        restDistanceMockMvc.perform(get("/api/distances?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(distance.getId().intValue())))
            .andExpect(jsonPath("$.[*].distance").value(hasItem(DEFAULT_DISTANCE.doubleValue())));
    }

    @Test
    @Transactional
    public void getDistance() throws Exception {
        // Initialize the database
        distanceRepository.saveAndFlush(distance);

        // Get the distance
        restDistanceMockMvc.perform(get("/api/distances/{id}", distance.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(distance.getId().intValue()))
            .andExpect(jsonPath("$.distance").value(DEFAULT_DISTANCE.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingDistance() throws Exception {
        // Get the distance
        restDistanceMockMvc.perform(get("/api/distances/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDistance() throws Exception {
        // Initialize the database
        distanceRepository.saveAndFlush(distance);

        int databaseSizeBeforeUpdate = distanceRepository.findAll().size();

        // Update the distance
        Distance updatedDistance = distanceRepository.findById(distance.getId()).get();
        // Disconnect from session so that the updates on updatedDistance are not directly saved in db
        em.detach(updatedDistance);
        updatedDistance
            .distance(UPDATED_DISTANCE);
        DistanceDTO distanceDTO = distanceMapper.toDto(updatedDistance);

        restDistanceMockMvc.perform(put("/api/distances")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(distanceDTO)))
            .andExpect(status().isOk());

        // Validate the Distance in the database
        List<Distance> distanceList = distanceRepository.findAll();
        assertThat(distanceList).hasSize(databaseSizeBeforeUpdate);
        Distance testDistance = distanceList.get(distanceList.size() - 1);
        assertThat(testDistance.getDistance()).isEqualTo(UPDATED_DISTANCE);
    }

    @Test
    @Transactional
    public void updateNonExistingDistance() throws Exception {
        int databaseSizeBeforeUpdate = distanceRepository.findAll().size();

        // Create the Distance
        DistanceDTO distanceDTO = distanceMapper.toDto(distance);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDistanceMockMvc.perform(put("/api/distances")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(distanceDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Distance in the database
        List<Distance> distanceList = distanceRepository.findAll();
        assertThat(distanceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDistance() throws Exception {
        // Initialize the database
        distanceRepository.saveAndFlush(distance);

        int databaseSizeBeforeDelete = distanceRepository.findAll().size();

        // Get the distance
        restDistanceMockMvc.perform(delete("/api/distances/{id}", distance.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Distance> distanceList = distanceRepository.findAll();
        assertThat(distanceList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Distance.class);
        Distance distance1 = new Distance();
        distance1.setId(1L);
        Distance distance2 = new Distance();
        distance2.setId(distance1.getId());
        assertThat(distance1).isEqualTo(distance2);
        distance2.setId(2L);
        assertThat(distance1).isNotEqualTo(distance2);
        distance1.setId(null);
        assertThat(distance1).isNotEqualTo(distance2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DistanceDTO.class);
        DistanceDTO distanceDTO1 = new DistanceDTO();
        distanceDTO1.setId(1L);
        DistanceDTO distanceDTO2 = new DistanceDTO();
        assertThat(distanceDTO1).isNotEqualTo(distanceDTO2);
        distanceDTO2.setId(distanceDTO1.getId());
        assertThat(distanceDTO1).isEqualTo(distanceDTO2);
        distanceDTO2.setId(2L);
        assertThat(distanceDTO1).isNotEqualTo(distanceDTO2);
        distanceDTO1.setId(null);
        assertThat(distanceDTO1).isNotEqualTo(distanceDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(distanceMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(distanceMapper.fromId(null)).isNull();
    }
}
