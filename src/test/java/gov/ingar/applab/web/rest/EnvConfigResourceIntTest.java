package gov.ingar.applab.web.rest;

import gov.ingar.applab.AppLab2App;

import gov.ingar.applab.domain.EnvConfig;
import gov.ingar.applab.repository.EnvConfigRepository;
import gov.ingar.applab.service.EnvConfigService;
import gov.ingar.applab.service.dto.EnvConfigDTO;
import gov.ingar.applab.service.mapper.EnvConfigMapper;
import gov.ingar.applab.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static gov.ingar.applab.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EnvConfigResource REST controller.
 *
 * @see EnvConfigResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppLab2App.class)
public class EnvConfigResourceIntTest {

    private static final String DEFAULT_PARAM = "AAAAAAAAAA";
    private static final String UPDATED_PARAM = "BBBBBBBBBB";

    private static final String DEFAULT_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_VALUE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private EnvConfigRepository envConfigRepository;

    @Autowired
    private EnvConfigMapper envConfigMapper;

    @Autowired
    private EnvConfigService envConfigService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restEnvConfigMockMvc;

    private EnvConfig envConfig;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EnvConfigResource envConfigResource = new EnvConfigResource(envConfigService);
        this.restEnvConfigMockMvc = MockMvcBuilders.standaloneSetup(envConfigResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EnvConfig createEntity(EntityManager em) {
        EnvConfig envConfig = new EnvConfig()
            .param(DEFAULT_PARAM)
            .value(DEFAULT_VALUE)
            .description(DEFAULT_DESCRIPTION);
        return envConfig;
    }

    @Before
    public void initTest() {
        envConfig = createEntity(em);
    }

    @Test
    @Transactional
    public void createEnvConfig() throws Exception {
        int databaseSizeBeforeCreate = envConfigRepository.findAll().size();

        // Create the EnvConfig
        EnvConfigDTO envConfigDTO = envConfigMapper.toDto(envConfig);
        restEnvConfigMockMvc.perform(post("/api/env-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(envConfigDTO)))
            .andExpect(status().isCreated());

        // Validate the EnvConfig in the database
        List<EnvConfig> envConfigList = envConfigRepository.findAll();
        assertThat(envConfigList).hasSize(databaseSizeBeforeCreate + 1);
        EnvConfig testEnvConfig = envConfigList.get(envConfigList.size() - 1);
        assertThat(testEnvConfig.getParam()).isEqualTo(DEFAULT_PARAM);
        assertThat(testEnvConfig.getValue()).isEqualTo(DEFAULT_VALUE);
        assertThat(testEnvConfig.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createEnvConfigWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = envConfigRepository.findAll().size();

        // Create the EnvConfig with an existing ID
        envConfig.setId(1L);
        EnvConfigDTO envConfigDTO = envConfigMapper.toDto(envConfig);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEnvConfigMockMvc.perform(post("/api/env-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(envConfigDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EnvConfig in the database
        List<EnvConfig> envConfigList = envConfigRepository.findAll();
        assertThat(envConfigList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkParamIsRequired() throws Exception {
        int databaseSizeBeforeTest = envConfigRepository.findAll().size();
        // set the field null
        envConfig.setParam(null);

        // Create the EnvConfig, which fails.
        EnvConfigDTO envConfigDTO = envConfigMapper.toDto(envConfig);

        restEnvConfigMockMvc.perform(post("/api/env-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(envConfigDTO)))
            .andExpect(status().isBadRequest());

        List<EnvConfig> envConfigList = envConfigRepository.findAll();
        assertThat(envConfigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkValueIsRequired() throws Exception {
        int databaseSizeBeforeTest = envConfigRepository.findAll().size();
        // set the field null
        envConfig.setValue(null);

        // Create the EnvConfig, which fails.
        EnvConfigDTO envConfigDTO = envConfigMapper.toDto(envConfig);

        restEnvConfigMockMvc.perform(post("/api/env-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(envConfigDTO)))
            .andExpect(status().isBadRequest());

        List<EnvConfig> envConfigList = envConfigRepository.findAll();
        assertThat(envConfigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEnvConfigs() throws Exception {
        // Initialize the database
        envConfigRepository.saveAndFlush(envConfig);

        // Get all the envConfigList
        restEnvConfigMockMvc.perform(get("/api/env-configs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(envConfig.getId().intValue())))
            .andExpect(jsonPath("$.[*].param").value(hasItem(DEFAULT_PARAM.toString())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getEnvConfig() throws Exception {
        // Initialize the database
        envConfigRepository.saveAndFlush(envConfig);

        // Get the envConfig
        restEnvConfigMockMvc.perform(get("/api/env-configs/{id}", envConfig.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(envConfig.getId().intValue()))
            .andExpect(jsonPath("$.param").value(DEFAULT_PARAM.toString()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingEnvConfig() throws Exception {
        // Get the envConfig
        restEnvConfigMockMvc.perform(get("/api/env-configs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEnvConfig() throws Exception {
        // Initialize the database
        envConfigRepository.saveAndFlush(envConfig);

        int databaseSizeBeforeUpdate = envConfigRepository.findAll().size();

        // Update the envConfig
        EnvConfig updatedEnvConfig = envConfigRepository.findById(envConfig.getId()).get();
        // Disconnect from session so that the updates on updatedEnvConfig are not directly saved in db
        em.detach(updatedEnvConfig);
        updatedEnvConfig
            .param(UPDATED_PARAM)
            .value(UPDATED_VALUE)
            .description(UPDATED_DESCRIPTION);
        EnvConfigDTO envConfigDTO = envConfigMapper.toDto(updatedEnvConfig);

        restEnvConfigMockMvc.perform(put("/api/env-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(envConfigDTO)))
            .andExpect(status().isOk());

        // Validate the EnvConfig in the database
        List<EnvConfig> envConfigList = envConfigRepository.findAll();
        assertThat(envConfigList).hasSize(databaseSizeBeforeUpdate);
        EnvConfig testEnvConfig = envConfigList.get(envConfigList.size() - 1);
        assertThat(testEnvConfig.getParam()).isEqualTo(UPDATED_PARAM);
        assertThat(testEnvConfig.getValue()).isEqualTo(UPDATED_VALUE);
        assertThat(testEnvConfig.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingEnvConfig() throws Exception {
        int databaseSizeBeforeUpdate = envConfigRepository.findAll().size();

        // Create the EnvConfig
        EnvConfigDTO envConfigDTO = envConfigMapper.toDto(envConfig);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEnvConfigMockMvc.perform(put("/api/env-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(envConfigDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EnvConfig in the database
        List<EnvConfig> envConfigList = envConfigRepository.findAll();
        assertThat(envConfigList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEnvConfig() throws Exception {
        // Initialize the database
        envConfigRepository.saveAndFlush(envConfig);

        int databaseSizeBeforeDelete = envConfigRepository.findAll().size();

        // Get the envConfig
        restEnvConfigMockMvc.perform(delete("/api/env-configs/{id}", envConfig.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<EnvConfig> envConfigList = envConfigRepository.findAll();
        assertThat(envConfigList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EnvConfig.class);
        EnvConfig envConfig1 = new EnvConfig();
        envConfig1.setId(1L);
        EnvConfig envConfig2 = new EnvConfig();
        envConfig2.setId(envConfig1.getId());
        assertThat(envConfig1).isEqualTo(envConfig2);
        envConfig2.setId(2L);
        assertThat(envConfig1).isNotEqualTo(envConfig2);
        envConfig1.setId(null);
        assertThat(envConfig1).isNotEqualTo(envConfig2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EnvConfigDTO.class);
        EnvConfigDTO envConfigDTO1 = new EnvConfigDTO();
        envConfigDTO1.setId(1L);
        EnvConfigDTO envConfigDTO2 = new EnvConfigDTO();
        assertThat(envConfigDTO1).isNotEqualTo(envConfigDTO2);
        envConfigDTO2.setId(envConfigDTO1.getId());
        assertThat(envConfigDTO1).isEqualTo(envConfigDTO2);
        envConfigDTO2.setId(2L);
        assertThat(envConfigDTO1).isNotEqualTo(envConfigDTO2);
        envConfigDTO1.setId(null);
        assertThat(envConfigDTO1).isNotEqualTo(envConfigDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(envConfigMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(envConfigMapper.fromId(null)).isNull();
    }
}
